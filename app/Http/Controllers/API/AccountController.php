<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Models\Follow;
use App\Models\Proficiency;
use App\Models\Experience;
use App\Models\Favorite;

use Hash;
/**
 *
 */
class AccountController extends Controller
{

  public function __construct()
  {
    # code...
  }

  public function accountDetail(Request $request){
    $user_id = $request->user_id;
    $api_key = $request->api_key;
    $user = User::where('user_id', $user_id)->where('api_key', $api_key)->first();
    return $user;
    if(!empty($user)){
      if($user->is_doctor == 0){
        $following = Follow::where('user_id', $user_id)->get();
        $data = [
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'birth_data' => $user->birth_data,
            'img_name' => $user->img_name,
            'following' => $following
        ];

        $status = [
            'status' => 'success',
            'statusCode' => '00',
            'description' => 'Data My Account of Member',
            'data' => $data
        ];
        return response()->json($status);
      }elseif($user->is_doctor == 1){
        $proficiency = Proficiency::where('user_id', $user_id)->first();
        $following = Follow::where('user_id', $user_id)->get();
        $follower = Follow::where('ref_user_id', $user_id)->get();

        $data = [
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'birth_data' => $user->birth_data,
            'img_name' => $user->img_name,
            'Proficiency' => $proficiency->name,
            'following' => $following,
            'follower' => $follower
        ];

        $status = [
            'status' => 'success',
            'statusCode' => '00',
            'description' => 'Data My Account of Doctor',
            'data' => $data
        ];
        return response()->json($status);
      }
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'User Not found.',
      ];
      return response()->json($status);
    }

  }

  // public function getForDoctor($user_id){
  //   $user = User::where('user_id', $user_id)->first();
  //   $proficiency = Proficiency::where('user_id', $user_id)->first();
  //   $following = Follow::where('user_id', $user_id)->get();
  //   $follower = Follow::where('ref_user_id', $user_id)->get();
  //
  //   $data = [
  //       'firstname' => $user->firstname,
  //       'lastname' => $user->lastname,
  //       'birth_data' => $user->birth_data,
  //       'img_name' => $user->img_name,
  //       'Proficiency' => $proficiency->name,
  //       'following' => $following,
  //       'follower' => $follower
  //   ];
  //
  //   $status = [
  //       'status' => 'success',
  //       'statusCode' => '00000',
  //       'description' => 'Data My Account of Doctor',
  //       'data' => $data
  //   ];
  //   return response()->json($status);
  // }
  // public function getForMember($user_id){
  //   $user = User::where('user_id', $user_id)->first();
  //   $following = Follow::where('user_id', $user_id)->get();
  //
  //   $data = [
  //       'firstname' => $user->firstname,
  //       'lastname' => $user->lastname,
  //       'birth_data' => $user->birth_data,
  //       'img_name' => $user->img_name,
  //       'following' => $following
  //   ];
  //
  //   $status = [
  //       'status' => 'success',
  //       'statusCode' => '00000',
  //       'description' => 'Data My Account of Member',
  //       'data' => $data
  //   ];
  //   return response()->json($status);
  // }

  public function getArticle($user_id){

  }
  public function editProfile(Request $request){
    $user_id = $request->user_id;
    $api_key = $request->api_key;
    $user = User::where('api_key', $api_key)->update($request->all());
    $status = [
        'status' => 'success',
        'statusCode' => '00',
        'description' => 'Update Success.',
        'data' => $user
    ];
    return response()->json($status);
  }
  public function changepassword(Request $request){
    $user_id = $request->user_id;
    $api_key = $request->api_key;
    $user = User::where('api_key', $api_key)->update($request->all());
    $status = [
        'status' => 'success',
        'statusCode' => '00',
        'description' => 'Change Password Success.'
    ];
    return response()->json($status);
  }
  public function getFollowing($user_id){
    $following = Follow::where('user_id', $user_id)->get();
    foreach ($following as $key => $follow) {
      # code...
      $following_1 = Experience::join('user', 'user.user_id', '=', 'experience.user_id')
                              ->join('department', 'experience.department_id', '=', 'department.department_id')
                              ->where('user.user_id', $follow->ref_user_id)
                              ->where('user.is_doctor', 1)
                              ->where('experience.is_primary', 1)
                              ->first();
      $following[$key]['ref_user_detail'] = $following_1;
    }

    if(!empty($following)){
      $status = [
          'status' => 'success',
          'statusCode' => '00000',
          'description' => '',
          'data' => $following
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '00001',
          'description' => 'Data is Not Found.'
      ];
      return response()->json($status);
    }
  }
  public function getFollower($user_id){
    $follower = Follow::where('ref_user_id', $user_id)->get();
    // $following = Follow::where('user_id', $user_id)->get();
    // return $follower;
    foreach ($follower as $key => $follow) {
      # code...
      $follower_1 = Experience::join('user', 'user.user_id', '=', 'experience.user_id')
                              ->join('department', 'experience.department_id', '=', 'department.department_id')
                              ->where('user.user_id', $follow->user_id)
                              ->where('user.is_doctor', 1)
                              ->where('experience.is_primary', 1)
                              ->first();
      $follower[$key]['ref_user_detail'] = $follower_1;
    }
    if(!empty($follower)){
      $status = [
          'status' => 'success',
          'statusCode' => '00000',
          'description' => '',
          'data' => $follower
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'Data is Not Found.'
      ];
      return response()->json($status);
    }
  }

  public function getFavorite($api_key, $type){
    // $api_key = $request->api_key;
    // $type = $request->type;

    $user = User::where('api_key', $api_key)->first();
    if(!empty($user)){

      $favorite = Favorite::join('article','article.user_id', '=', 'favorite.user_id')
                         ->where('user_id', $user->user_id)
                         ->where('type', $type)
                         ->get();

      if(!empty($favorite)){
        $status = [
            'status' => 'success',
            'statusCode' => '00',
            'description' => 'Data is found.'
        ];
        return response()->json($status);
      }else{
        $status = [
            'status' => 'error',
            'statusCode' => '01',
            'description' => 'Data is not found.'
        ];
        return response()->json($status);
      }
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'user not found.',
      ];
      return response()->json($status);
    }

  }




}
