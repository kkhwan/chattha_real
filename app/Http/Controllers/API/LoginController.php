<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Library\MainFunction;
use App\Models\User;


use Hash;
/**
 *
 */
class LoginController extends Controller
{

  public function __construct()
  {

  }
  public function signin(Request $request){
    $email = $request->email;
    $password = $request->password;
    $hashed_password = Hash::make($password);
    $user = User::where('email', $email)->where('password', $hashed_password)->first();
    if(empty($user)){
      $status = [
            'statusCode' => '01',
            'status' => 'error',
            'description' => 'Email and Password Invalid.'
      ];
      return response()->json($status);
    }else {

      $data = [
          'user_id' => $user->user_id,
          'firstname' => $user->firstname,
          'lastname' => $user->lastname,
          'img_name' => $user->img_name,
          'is_doctor' => $user->is_doctor,
      ];
      $status = [
            'statusCode' => '00',
            'status' => 'success',
            'description' => 'Signin success.',
            'data' => $data
      ];
    }
  }

  public function createAccount(Request $request){
    $objFn = new MainFunction();
    $email = $request->email;
    $password = $request->password;
    $api_key  = $objFn->gen_random(60,1);
    $user = User::where('email', $email)->first();
    if(!empty($user)){
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'Email is Duplicate.'
      ];
      return response()->json($status);
    }else{
      $hashed = Hash::make($password);
      $create_user = User::create(['email' => $email, 'password' => $password, 'api_key' => $api_key]);
      $status = [
          'status' => 'success',
          'statusCode' => '00',
          'description' => 'create account success.',
          'data' => ['user_id' => $create_user->user_id, 'api_key' => $create_user->api_key]
      ];
      return response()->json($status);
    }
  }

  public function forgetPassword(Request $request){
      $obj_fn = new MainFunction();
      $email = $request->email;
      $user = User::where('email', $email)->first();
      if(!empty($user)){
        $new_password = $obj_fn->gen_random(8,1);
        $user->password = $new_password;
        $user->save();

        Mail::send('emails.forget-password', ['user' => $user ], function ($m) use ($email) {
            $m->to('chayangkul@bangkoksolutions.com');
            $m->from('chayangkul@bangkoksolutions.com');
            // $m->to($email, $firstname . ' ' . $lastname);
            $m->subject('Forget Password');
        });
        $status = [
            'status' => 'success',
            'code' => '00000',
            'description' => 'Send Email Success.'
        ];
        return response()->json($status);
      }else{
        $status = [
            'status' => 'error',
            'code' => '00001',
            'description' => 'Email Not Found.'
        ];
        return response()->json($status);
      }

  }


}
