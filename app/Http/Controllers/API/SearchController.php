<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Models\Follow;
use App\Models\Proficiency;
use App\Models\DoctorProficiency;
use App\Models\Experience;
use App\Models\PainDiary;
use App\Models\PainPoint;
use App\Models\PainCondition;
use App\Models\Conditions;
use App\Models\PainCategory;
use App\Models\Location;
use App\Models\Department;

use Hash;
/**
 *
 */
class SearchController extends Controller
{

  public function __construct()
  {
    # code...
  }

  public function finder(Request $request){
      $check_type = $request->check_type;
      $name = $request->name;
      $proficiency_id = $request->proficiency_id;
      $province_id = $request->province_id;
      $hospital_location = $request->location_id;

      if($check_type == 0 || $check_type == 1){
          $data = Location::where('type', $check_type);
          if(!empty($name)){
            $data = $data->where('location_name', 'like', '%' . $name . '%');
          }
          if(!empty($province_id)){
            $data = $data->where('province_id', $province_id);
          }
          $data = $data->get();
          if(!empty($data)){
            $status = [
                'status' => 'success',
                'statusCode' => '00',
                'description' => 'Query Success.',
                'data' => $data
            ];
            return response()->json($status);
          }
      }
      if($check_type == 2){

            if(!empty($proficiency_id)){
                $data = User::join('experience', 'experience.user_id', '=', 'user.user_id')
                            ->join('department', 'department.department_id', '=', 'experience.department_id')
                            ->join('location', 'location.location_id', '=', 'department.location_id')
                            ->join('doctor_proficiency', 'doctor_proficiency.user_id', '=', 'user.user_id')
                            ->join('proficiency', 'proficiency.proficiency_id', '=', 'doctor_proficiency.proficiency_id');

                $data = $data->where('experience.is_primary', 1);
                $data = $data->where('user.is_doctor', 1);
                // $data = $data->where('location.province_id', 1);
                if(!empty($name)){
                  $data = $data->where('user.firstname', 'like', '%' . $name . '%');
                }
                if(!empty($hospital_location)){
                  $data = $data->where('location.location_id', $hospital_location);
                }
                if(!empty($province_id)){
                  $data = $data->where('location.province_id', $province_id);
                }
                if(!empty($proficiency_id)){
                  $data = $data->where('doctor_proficiency.proficiency_id', $proficiency_id);
                }
                $data = $data->get();
                $status = [
                    'status' => 'success',
                    'statusCode' => '00',
                    'description' => 'Query Success.',
                    'data' => $data
                ];
                return response()->json($status);
            }else{
                $data = User::join('experience', 'experience.user_id', '=', 'user.user_id')
                          ->join('department', 'department.department_id', '=', 'experience.department_id')
                          ->join('location', 'location.location_id', '=', 'department.location_id');
                $data = $data->where('experience.is_primary', 1);
                $data = $data->where('user.is_doctor', 1);
                if(!empty($name)){
                  $data = $data->where('user.firstname', 'like', '%' . $name . '%');
                }
                if(!empty($hospital_location)){
                  $data = $data->where('location.location_id', $hospital_location);
                }
                if(!empty($province_id)){
                  $data = $data->where('location.province_id', $province_id);
                }

                $datas = $data->get();
                  foreach ($datas as $key => $data) {
                    $doctor_proficiency = DoctorProficiency::join('proficiency', 'proficiency.proficiency_id', '=', 'doctor_proficiency.proficiency_id')
                                                          ->where('doctor_proficiency.user_id', $data->user_id)
                                                          ->first();

                    $data['proficiency_id'] = $doctor_proficiency->proficiency_id;
                    $data['proficiency_name'] = $doctor_proficiency->proficiency_name;
                  }
                  $status = [
                      'status' => 'success',
                      'statusCode' => '00',
                      'description' => 'Query Success.',
                      'data' => $datas
                  ];
                  return response()->json($status);
            }




            // if(empty($proficiency_id)){
            //   $datas = $data->get();
            //   foreach ($datas as $key => $data) {
            //     $doctor_proficiency = DoctorProficiency::join('proficiency', 'proficiency.proficiency_id', '=', 'doctor_proficiency.proficiency_id')
            //                                           ->where('doctor_proficiency.user_id', $data->user_id)
            //                                           ->get();
            //
            //     $data['proficiency'] = $doctor_proficiency;
            //   }
            //
            // }else{
            //   $doctor_proficiency = DoctorProficiency::join('proficiency', 'proficiency.proficiency_id', '=', 'doctor_proficiency.proficiency_id')
            //                                         ->where('doctor_proficiency.proficiency_id', $proficiency_id)
            //                                         ->get();
            //
            //   $array_data = [];
            //   foreach ($doctor_proficiency as $key => $value) {
            //     $data_proficiency = User::join('experience', 'experience.user_id', '=', 'user.user_id')
            //                 ->join('department', 'department.department_id', '=', 'experience.department_id')
            //                 ->join('location', 'location.location_id', '=', 'department.location_id');
            //
            //     $data_proficiency = $data_proficiency->where('experience.is_primary', 1);
            //     $data_proficiency = $data_proficiency->where('user.is_doctor', 1);
            //     $data_proficiency = $data_proficiency->where('user.user_id', $value->user_id);
            //     $data_proficiencys = $data_proficiency->first();
            //     array_push($array_data,$data_proficiencys);
            //   }
            //   return $array_data;
            //   foreach ($array_data as $key => $array) {
            //       $proficiency_s = Proficiency::where('proficiency_id', $proficiency_id)->first();
            //       $array['proficiency'] = $proficiency_s;
            //   }
            //
            //   $datas = $array_data;
            // }

      }
    }

      public function finderDetail(Request $request){
      $user_id = $request->user_id;
      $location_id = $request->location_id;
      $check_type = $request->check_type;
      $api_key = $request->api_key;
      if($check_type == 0 ){

          $data = Location::where('type', $check_type);
          if(!empty($api_key)){
            $data = $data->where('api_key', $api_key);
            $data = $data->get();
            if(!empty($data)){

              $status = [
                  'status' => 'success',
                  'statusCode' => '00',
                  'description' => 'Query Success.',
                  'data' => $data
              ];
              return response()->json($status);
            }
          }else{
            $status = [
                'status' => 'error',
                'statusCode' => '01',
                'description' => 'Data not found.'
            ];
            return response()->json($status);
          }


      }
      if($check_type == 1){
          $data = Location::where('location_id', $location_id);
          if(!empty($api_key)){
            $data = $data->where('api_key', $api_key)->first();
          }else{
            $status = [
                'status' => 'error',
                'statusCode' => '02',
                'description' => 'api_key not found.'
            ];
            return response()->json($status);
          }

          if(!empty($data)){
            $department = Department::where('location_id', $location_id)->get();
            foreach ($department as $key => $depart) {
              $experience = Experience::join('user', 'user.user_id', '=', 'experience.user_id')
                                     ->where('department_id', $depart->department_id)
                                     ->get();
                  foreach ($experience as $key => $exp) {
                    $doctor_proficiency = DoctorProficiency::join('proficiency', 'proficiency.proficiency_id', '=', 'doctor_proficiency.proficiency_id')
                                                          ->where('doctor_proficiency.user_id', $exp->user_id)->first();
                    $exp['proficiency'] = $doctor_proficiency;
                    // $proficiency = Proficiency::where('proficiency_id', $doctor_proficiency->proficiency_id)->first();
                  }
              $depart['experience'] = $experience;

              // $department
            }
            // return $department;
            $data['department'] = $department;
            return $data;
          }else{
            $status = [
                'status' => 'error',
                'statusCode' => '01',
                'description' => 'Data not found.',
            ];
            return response()->json($status);
          }

      }
      if($check_type == 2){
            $api_key = $request->api_key;
            $data = User::where('api_key', $api_key)->first();
            if(!empty($data)){
              $experience = Experience::where('user_id', $data->user_id)->get();
              $data['experience'] = $experience;
              $proficiency = DoctorProficiency::join('proficiency', 'proficiency.proficiency_id', '=', 'doctor_proficiency.proficiency_id')
                                                ->where('doctor_proficiency.user_id', $data->user_id)
                                                ->get();
              $data['proficiency'] = $proficiency;
              $location = Location::where('location_id', $location_id)->first();
              $data['location'] = $location;
              $status = [
                  'status' => 'success',
                  'statusCode' => '00',
                  'description' => 'Query Success.',
                  'data' => $data
              ];
              return response()->json($status);
            }else{
              $status = [
                  'status' => 'error',
                  'statusCode' => '01',
                  'description' => 'Data not found.',
              ];
              return response()->json($status);
            }


      }





  }




}
