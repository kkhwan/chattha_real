<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Models\Follow;
use App\Models\Proficiency;
use App\Models\Experience;
use App\Models\PainDiary;
use App\Models\PainPoint;
use App\Models\PainCondition;
use App\Models\Conditions;
use App\Models\PainCategory;

use Hash;
/**
 *
 */
class SymptomCheckerController extends Controller
{

  public function __construct()
  {
    # code...
  }

  public function getSymptomChecker(){
    $pain_point = PainPoint::get();

    if(!empty($pain_point)){
      $status = [
          'status' => 'success',
          'statusCode' => '00',
          'description' => 'Query Success.',
          'data' => $pain_point
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'data not found.'
      ];
      return response()->json($status);
    }
  }

  public function getTypeOfSymptom(){
    $pain_category = PainCategory::get();
    if(!empty($pain_category)){
      $status = [
          'status' => 'success',
          'statusCode' => '00',
          'description' => 'Query Success.',
          'data' => $pain_category
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'data not found.'
      ];
      return response()->json($status);
    }
  }

  public function getSymptomResult($category_id){
    $conditions = PainCondition::join('conditions', 'conditions.conditions_id', '=','pain_condition.conditions_id')
                              ->where('pain_condition.pain_category_id', $category_id)
                              ->get();
    return $conditions;
    if(!empty($conditions)){
      $status = [
          'status' => 'success',
          'statusCode' => '00',
          'description' => 'Query Success.',
          'data' => $conditions
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'data not found.'
      ];
      return response()->json($status);
    }
  }




}
