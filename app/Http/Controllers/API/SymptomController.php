<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Models\Follow;
use App\Models\Proficiency;
use App\Models\Experience;
use App\Models\PainDiary;

use Hash;
/**
 *
 */
class SymptomController extends Controller
{

  public function __construct()
  {
    # code...
  }

  public function getSymptomRecords($api_key){
    $user = User::where('api_key', $api_key)->first();
    if(!empty($user)){
      $symptom = PainDiary::where('user_id', $user->user_id)->get();
      $status = [
          'status' => 'success',
          'statusCode' => '00',
          'description' => 'Query Success.',
          'data' => $symptom
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'user not found.'
      ];
      return response()->json($status);
    }

  }
  public function addSymptom(Request $request){
    $api_key = $request->api_key;
    $user = User::where('api_key', $api_key)->first();
    $pain = $request->pain_level;
    $stiffness = $request->stiffness_level;
    $fatigue = $request->fatigue_level;
    $flag = false;
    if(!empty($pain)){
      $data = [
        'user_id' => $user->user_id,
        'symptom' => 1,
        'level' => $pain
      ];
      $pain_db = PainDiary::Create($data);
      $flag = true;
    }
    if(!empty($stiffness)){
      $data = [
        'user_id' => $user->user_id,
        'symptom' => 2,
        'level' => $stiffness
      ];
      $pain_db = PainDiary::Create($data);
      $flag = true;
    }
    if(!empty($fatigue)){
      $data = [
        'user_id' => $user->user_id,
        'symptom' => 3,
        'level' => $fatigue
      ];
      $pain_db = PainDiary::Create($data);
      $flag = true;
    }
    if($flag == true){
      $status = [
          'status' => 'success',
          'statusCode' => '00',
          'description' => 'insert symptom success'
      ];
      return response()->json($status);
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => ''
      ];
      return response()->json($status);
    }
  }

  public function exportSymptom(Request $request){
    $api_key = $request->api_key;
    $status = $request->status;
    $start_time = $request->start_time;
    $end_time = $request->end_time;
    $email_doctor = $request->email_to_doctor;

    $user = User::where('api_key', $api_key)->first();
    if(!empty($user)){
      if($status == 1){ // select All
        $symptom = PainDiary::where('user_id', $user->user_id)->get();
        // Sendmail
        //code
        $status = [
            'status' => 'success',
            'statusCode' => '00',
            'description' => 'query success.',
            'data' => $symptom
        ];
        return response()->json($status);
      }else if ($status == 2){ // select times
        $symptom = PainDiary::where('user_id', $user->user_id)->tosql();
        $symptom = $symptom->where('created_at', '=>' , $start_time );
        $symptom = $symptom->where('created_at', '<=' , $end_time );
        $symptoms = $symptom->get();
        // Sendmail
        //code
        $status = [
            'status' => 'success',
            'statusCode' => '00',
            'description' => 'query success.',
            'data' => $symptoms
        ];
        return response()->json($status);
      }
    }else{
      $status = [
          'status' => 'error',
          'statusCode' => '01',
          'description' => 'user not found.',
      ];
      return response()->json($status);
    }


  }




}
