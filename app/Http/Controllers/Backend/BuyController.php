<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\AdminRole;
use App\Models\Admin;
use App\Models\Page;
use App\Models\Buy;

use App\Models\Apertment;
use App\Models\Community;
use App\Models\Features;
use App\Models\BuildingOwner;
use App\Models\BuildingState;
use App\Models\LandDetail;
use App\Models\LandTitle;
use App\Models\LandType;
use App\Models\ListingType;
use App\Models\Location;
use App\Models\Living;
use App\Models\PropertyType;
use App\Models\Service;
use App\Models\Suitability;
use App\Models\Views;

use Input;
use Hash;
use DB;

class BuyController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Buy'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Buy'; // Page Title
        $this->a_search = ['property_no']; // Array Search
        $this->path = '_admin/buy'; // Url Path
        $this->view_path = 'backend.buy.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');

        $data = $obj_model;
        $data = $data->whereNull('deleted_at');
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy('buy_id','desc');
        $data = $data->paginate($per_page);
        // $data = $data->paginate($per_page);

        $property_type = PropertyType::where('type', 1)->orWhere('type', 3)->get();
        $listing_type = ListingType::all();
        $landDetail = LandDetail::all();
        $land_title = LandTitle::all();
        $land_type = LandType::all();
        $buildingOwner = BuildingOwner::all();
        $buildingState = BuildingState::all();
        $community = Community::where('type', 1)->orWhere('type', 3)->get();
        $living = Living::where('type', 1)->orWhere('type', 3)->get();
        $location = Location::all();
        $views = Views::where('type', 2)->orWhere('type', 3)->get();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission', 'community', 'buildingOwner', 'buildingState', 'landDetail', 'land_title', 'listing_type', 'living', 'property_type', 'land_type','location', 'views'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $property_type = PropertyType::where('type', 1)->orWhere('type', 3)->get();
        $listing_type = ListingType::all();
        $land_detail = LandDetail::all();
        $land_title = LandTitle::all();
        $land_type = LandType::all();
        $building_owner = BuildingOwner::all();
        $building_state = BuildingState::all();
        $community = Community::where('type', 1)->orWhere('type', 3)->get();
        $living = Living::where('type', 1)->orWhere('type', 3)->get();
        // $season = Season::all();
        $location = Location::all();
        $views = Views::where('type', 2)->orWhere('type', 3)->get();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn', 'community', 'building_owner', 'building_state', 'land_detail', 'land_title', 'listing_type', 'living', 'property_type', 'land_type','location','permission', 'views'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        // -------------- property_no -------------- //
        $p = "";
        $l = Location::where('location_id', $request->location_id)->first()->location_code;
        $count_num = Buy::count();
        $no = 0;
        if( $count_num != 0){
            $no = Buy::orderBy('buy_id','absc')->first()->buy_id;
        }
        switch ($request->property_id) {
            case '1':
                $p = "L9";
                break;
            case '2':
                $p = "V".$request->bedrooms;
                break;
            case '3':
                $p = "A".$request->bedrooms;
                break;
            case '6':
                $p = "B8";
                break;
        }
        $n = 1111 + $no;
        $property_no = "S".$l.$p.$n;
        // -------------- property_no -------------- //
        
        $land_detail_id = $request->land_detail_id;
        $living_id = $request->living_id;
        $community_id = $request->community_id;
        $view_id = $request->view_id;
        $input = $request->all(); // Get all post from form
        $input['property_no'] = $property_no;
        $input['is_available'] = 0;
        if(!empty($land_detail_id)){
            $input['land_detail_id'] = implode(",", $land_detail_id);
        }
        if(!empty($living_id)){
            $input['living_id'] = implode(",", $living_id);
        }
        if(!empty($community_id)){
            $input['community_id'] = implode(",", $community_id);
        }
        if(!empty($view_id)){
            $input['view_id'] = implode(",", $view_id);
        }
        $data = $obj_model->create($input);

        if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/property');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->buy_id);
            $data->img_name = $filename;
            $data->save();
        }

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);

        $property_type = PropertyType::where('type', 1)->orWhere('type', 3)->get();
        $listing_type = ListingType::all();
        $land_detail = LandDetail::all();
        $land_title = LandTitle::all();
        $land_type = LandType::all();
        $building_owner = BuildingOwner::all();
        $building_state = BuildingState::all();
        $community = Community::where('type', 1)->orWhere('type', 3)->get();
        $living = Living::where('type', 1)->orWhere('type', 3)->get();
        // $season = Season::where('rent_id', $id);
        $location = Location::all();
        $views = Views::where('type', 2)->orWhere('type', 3)->get();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn', 'community', 'building_owner', 'building_state', 'land_detail', 'land_title', 'listing_type', 'living', 'property_type', 'land_type','location', 'views'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        // -------------- property_no -------------- //
        $p = "";
        $l = Location::where('location_id', $request->location_id)->first()->location_code;
        $no = Buy::where('buy_id', $id)->first()->property_no;
        switch ($request->property_id) {
            case '1':
                $p = "L9";
                break;
            case '2':
                $p = "V".$request->bedrooms;
                break;
            case '3':
                $p = "A".$request->bedrooms;
                break;
            case '6':
                $p = "B8";
                break;
        }
        $n = substr($no,6);
        $property_no = "S".$l.$p.$n;
        // -------------- property_no -------------- //

        $land_detail_id = $request->land_detail_id;
        $living_id = $request->living_id;
        $community_id = $request->community_id;
        $view_id = $request->view_id;

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $input['property_no'] = $property_no;
        if(!empty($land_detail_id)){
            $input['land_detail_id'] = implode(",", $land_detail_id);
        }
        if(!empty($living_id)){
            $input['living_id'] = implode(",", $living_id);
        }
        if(!empty($community_id)){
            $input['community_id'] = implode(",", $community_id);
        }
        if(!empty($view_id)){
            $input['view_id'] = implode(",", $view_id);
        }
        $data = $obj_model->find($id)->update($input);

        $data = $obj_model->find($id);
        if (Input::hasFile('img_name')) { // test P.
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/property');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_name = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/property');
            if($old_name != ''){         // set path
                $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
                $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            }

            $data->img_name = '';
            $data->save();
        }

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'d');
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
