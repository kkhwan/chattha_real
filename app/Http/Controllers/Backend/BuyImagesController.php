<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Page;
use App\Models\BuyImages;
use App\Models\Buy;

use Input;
use DB;

class BuyImagesController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\BuyImages'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Buy Image'; // Page Title
        $this->a_search = ['header_name','title_name']; // Array Search
        $this->path = '_admin/buy_img'; // Url Path
        $this->view_path = 'backend.buy_img.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';
        $buy_id = Input::get('buy_id');

        $data = $obj_model;
        $data = $data->where('buy_id', $buy_id);
        
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);
        $property_no = Buy::where('buy_id', $buy_id)->first()->property_no;

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','data_user','property_no', 'permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $primaryKey = $obj_model->primaryKey;
        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {   
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');
        $primaryKey = $obj_model->primaryKey;
        $str_param = $request->str_param;
        $input = $request->all();
        $input['buy_id'] = $request->buy_id;
        $input['is_available'] = 0;
        $data = $obj_model->create($input);

        $buy_id = $data->buy_id;
        if($buy_id == '0'){
            $check_sorting = $data::select("sorting")
                ->where('buy_id', $buy_id)
                ->orderBy('sorting','desc')
                ->first();
            $data->sorting = $check_sorting->sorting+1;
            $data->save();
        }else{
            $check_sorting = $data::select("sorting")
                ->where('buy_id', $buy_id)
                ->orderBy('sorting','desc')
                ->first();
            $data->sorting = $check_sorting->sorting+1;
            $data->save();
        }
        
        if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/buy_images');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->img_id);
            $data->img_name = $filename;
            $data->save();

        }
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        $data = $obj_model->find($id);
        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $input['buy_id'] = $request->buy_id;
        $data = $obj_model->find($id)->update($input);
        $permission = $obj_fn->permission($this->page_id,'u');

        $data = $obj_model->find($id);
        if (Input::hasFile('img_name')) { // test P.
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/buy_images');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_name = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/buy_images');           // set path
            if($old_name != ''){         // set path
                $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
                $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            }
            $data->img_name = '';
            $data->save();
        }


        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'d');
        $obj_model->find($id)->forceDelete();

        return redirect()->to(session()->get('ref_url'));
    }
}
