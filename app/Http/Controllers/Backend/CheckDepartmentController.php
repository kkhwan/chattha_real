<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Department;

use Input;
use Hash;

class CheckDepartmentController extends Controller
{
    public function checkDepartment()
    {
        $location_id = Input::post('location_id')
        $department = Department::where('location_id', $location_id)->get();
        // $option = '<option value="">Select Department</option>';
        foreach ($department as $value) {
            echo '<option value="'.$value->department_id.'" >'.$value->department_name.'</option>';
        }         
    }

}
