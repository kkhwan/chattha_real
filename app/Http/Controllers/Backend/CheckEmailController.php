<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\User;

use Input;
use Hash;

class CheckEmailController extends Controller
{
    public function checkEmail(Request $request)
    {
        $data = User::where('email',$request->email)->first();
        if(empty($data))
        {
            return response()->json(['status'=>'OK', 'message' => 'Available']);
        }
        else
        {
            return response()->json(['status'=>'ERROR', 'message' => 'Not Available']);
        }
    }

}
