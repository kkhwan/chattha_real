<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\User;
use App\Models\Page;
use App\Models\Student;
use App\Models\ContactMsg;

use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Mail;
use Storage;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\ContactMsg'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Contact'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/contact'; // Url Path
        $this->view_path = 'backend.contact.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');
        $primaryKey = $obj_model->primaryKey;
        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $data = $obj_model;

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Reply';

        $data = $obj_model->find($id);

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param','from_name','from_email','']); // Get all post from form
        $input['admin_id'] = session()->get('s_admin_id');;
        $input['status'] = 1;
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;

        $from_name = $request->from_name;
        $from_email = $request->from_email;
        $from_msg = $request->from_msg;

        $data = $obj_model->find($id);
        $name = $data->name;
        $email = $data->email;
        $msg = $data->msg;

        // $send =  Mail::send('emails.contact-email', ['email' => $email, 'name' => $name, 'from_msg' => $from_msg, 'msg' => $msg], function ($m) use($email,$name,$from_msg,$msg){
            // $m->to($email, $name);
            // $m->subject('Contact');
        // });
        return redirect()->to($this->view_path);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        
    }
}
