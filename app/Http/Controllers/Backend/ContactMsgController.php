<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\User;
use App\Models\ContactMsg;

use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Mail;
use Storage;

class ContactMsgController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\ContactMsg'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Contact'; // Page Title
        $this->a_search = ['name']; // Array Search
        $this->path = '_admin/contact_msg'; // Url Path
        $this->view_path = 'backend.contact_msg.'; // View Path
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');

        $data = $obj_model;
        // $data = $data->where('parent_id',0);

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','data_user'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $id = Input::get('id');
        $use_msg = ContactMsg::select("msg","name","email","mobile")->where('contact_msg_id',$id)->get();

        $a = json_decode($use_msg);
        $a_use_msg = $a[0]->msg;
        $a_use_name = $a[0]->name;
        $a_use_email = $a[0]->email;
        $a_use_mobile = $a[0]->mobile;

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','data_user','a_use_msg','a_use_name','a_use_email','a_use_mobile'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        $input['type'] = 1;
        $data = $obj_model->create($input);

        $id = Input::get('id');
        // $obj_fn->db_add($data,$this->primaryKey,$request,$this->fieldList);

        $data->parent_id = $id;
        $data->status = 1;
        $data->save();

        $data2 = $obj_model::find($id);
        $data2->status = 1;
        $data2->save();

        Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        {
            $message->from($data->email , $data->name);
            $message->to( $data2->email , $data2->name)->subject('Welcome!');
        });

        return redirect()->to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Review';

        Session::put('referUrl',URL::previous());

        $data = $obj_model::find($id);
        $contact_massage = ContactMsg::where('contact_msg_id',$id)->get();
        $reply_massage = ContactMsg::where('parent_id',$id)->get();

        $c = json_decode($contact_massage);
        $c_name = $c[0]->name;
        $c_email = $c[0]->email;
        $c_mobile = $c[0]->mobile;
        $c_msg = $c[0]->msg;

        $r = json_decode($reply_massage);
        $r_name = $r[0]->name;
        $r_email = $r[0]->email;
        $r_mobile = $r[0]->mobile;
        $r_msg = $r[0]->msg;
        $r_time_send = $r[0]->created_at;

//return $c_name.'<br>'.$c_email.'<br>'.$c_msg.'<br>'.$r_name.'<br>'.$r_email.'<br>'.$r_msg.'<br>'.$r_time_send;

        return view($this->viewPath.'/review',compact('data','url_to','method','txt_manage','review_massage','c_name','c_email','c_msg','c_mobile','r_name','r_email','r_msg','r_mobile','r_time_send'));


    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Upadte';
        $data = $obj_model->find($id);
        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $input['type'] = 1;
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
//---------------------------------------
/*public function getReply(){

    $url_to = $this->path;
    $method = 'POST';
    $txt_manage = "Add";
    $id = Input::get('id');
    $use_msg = ContactMsg::select('msg')->where('contact_msg_id',$id)->first();
    return view($this->viewPath.'/update',compact('url_to','method','txt_manage','use_msg'));
}*/

}
