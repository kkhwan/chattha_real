<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ContactUs;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ContactUsController extends Controller
{
    public $model = 'App\Model\ContactUs';
    public $titlePage = 'Contact Us';
    public $tbName = 'contact_us';
    public $pkField = 'contact_us_id';
    public $fieldList = array('company_name_th','company_name_en','address_th','address_en','mobile','fax','email');
    public $a_search = array('company_name_th','company_name_en');
    public $path = '_admin/contact_us';
    public $page = 'contact_us';
    public $viewPath = 'backend/contact_us';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        return redirect()->to('_admin/contact_us/1/edit');
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        return redirect()->to('_admin/contact_us/1/edit');
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        return redirect()->to('_admin/contact_us/1/edit');
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);


        $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList); // add $id

        return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

         $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

