<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\User;
use App\Models\Article;
use App\Models\Category;

use Input;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {

    }
    // ------------------------------------ Show All List Page
    public function index()
    {
        // $doctor = User::where('is_doctor','1')->count();
        // $news = Article::where('type','1')->count();
        // $article = Article::where('type','!=','1')->count();
        return view('backend.index');
        // return view('backend.index',compact('hospital','pharmacy','doctor','proficiency','painpoint','painpointcat','news','article','articlecat'));
    }

    // ------------------------------------ View Add Page
    public function create()
    {

    }

    // ------------------------------------ Record Data
    public function store(Request $request)
    {

    }

    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }

    // ------------------------------------ View Update Page
    public function edit($id)
    {

    }

    // ------------------------------------ Record Update Data
    public function update(Request $request, $id)
    {

    }

    // ------------------------------------ Delete Data
    public function destroy($id)
    {

    }
}
