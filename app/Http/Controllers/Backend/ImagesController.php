<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\User;
use App\Models\Images;
use App\Models\Activity;

use Input;
use DB;

class ImagesController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Images'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Image'; // Page Title
        $this->a_search = ['header_name','title_name']; // Array Search
        $this->path = '_admin/images'; // Url Path
        $this->view_path = 'backend.images.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';
        $activity_id = Input::get('activity_id');

        $data = $obj_model;
        $data = $data->where('activity_id', $activity_id);
        
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);
        $activity_name = Activity::where('activity_id', $activity_id)->first()->header_name;

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','data_user','activity_name'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {   
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $str_param = $request->str_param;
        $input = $request->all();
        $input['activity_id'] = $request->activity_id;
        $input['type'] = 1;
        $data = $obj_model->create($input);

        $activity_id = $data->activity_id;
        if($activity_id == '0'){
            $check_sorting = $data::select("sorting")
                ->where('activity_id', $activity_id)
                ->orderBy('sorting','desc')
                ->first();
            $data->sorting = $check_sorting->sorting+1;
            $data->save();
        }else{
            $check_sorting = $data::select("sorting")
                ->where('activity_id', $activity_id)
                ->orderBy('sorting','desc')
                ->first();
            $data->sorting = $check_sorting->sorting+1;
            $data->save();
        }
        
        if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/activity');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->images_id);
            $data->img_name = $filename;
            $data->save();

        }
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        $data = $obj_model->find($id);
        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $input['activity_id'] = $request->activity_id;
        $input['type'] = 1;
        $data = $obj_model->find($id)->update($input);

        $data = $obj_model->find($id);
        if (Input::hasFile('img_name')) { // test P.
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/activity');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_name = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/activity');           // set path
            if($old_name != ''){         // set path
                $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
                $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            }
            $data->img_name = '';
            $data->save();
        }


        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
        $obj_model->find($id)->forceDelete();

        return redirect()->to(session()->get('ref_url'));
    }
}
