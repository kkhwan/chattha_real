<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Reply;
use App\Models\Blog;
use App\Models\Student;

use Input;
use DB;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Reply'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Reply'; // Page Title
        $this->path = '_admin/reply'; // Url Path
        $this->view_path = 'backend.reply.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';
        $blog_id = Input::get('blog_id');

        $data = $obj_model;
        $data = $data->where('blog_id', $blog_id);
        
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        $student = Student::get();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','student'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
        $obj_model->find($id)->forceDelete();

        return redirect()->to(session()->get('ref_url'));
    }
}
