<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\User;

use Input;
use DB;
use Mail;
use Hash;

class ResetPasswordController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\User'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Password'; // Page Title
        $this->a_search = ['firstname','lastname']; // Array Search
        $this->path = '_admin/reset_password'; // Url Path
        $this->view_path = 'backend.reset_password.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {

    }
    // ------------------------------------ View Add Page
    public function create()
    {
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';
        // if(empty(Input::get('user_id'))){
        //     return abort(503);
        // }
        $data = $obj_model->find($id);

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $password = $request->password_confirm;

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $input['password'] = Hash::make($password);
        $data = $obj_model->find($id)->update($input);
        $str_param = $request->str_param;
        $data = $obj_model->find($id);
        $email = $data->email;
        $firstname = $data->firstname;

        $send =  Mail::send('emails.doctor-register', ['email' => $email, 'password' => $password, 'firstname' => $firstname], function ($m) use($email,$password,$firstname){
            $m->to($email, $firstname);
            $m->subject('Reset Password');
        });
        return redirect()->to('_admin/doctor');
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        $obj_fn = $this->obj_fn;
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
