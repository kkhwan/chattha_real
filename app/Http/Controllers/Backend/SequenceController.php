<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\PainCategory;
use App\Models\experience;
use App\Models\Department;
use App\Models\StudentTake;
use App\Models\Student;
use App\Models\Images;
use App\Models\BuyImages;
use App\Models\RentImages;
use App\Models\Rent;
use App\Models\Buy;
use App\Models\Location;


use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SequenceController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }
    public function postImage(Request $request)
    {
        $images_id = $request->images_id;
        $activity_id = $request->activity_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;
        // return $old_sorting;

        $data = Images::where('sorting', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = Images::find($images_id);
            // $change_data2->where('activity_id', $activity_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Images::where('images_id',$data->images_id);
        $change_data->where('activity_id', $activity_id);
        $change_data->update(['sorting' => $old_sorting]);


        $change_data2 = Images::where('images_id',$images_id);
        $change_data2->where('activity_id', $activity_id);
        $change_data2->update(['sorting' => $new_sorting]);

        return redirect()->back();
    }
    public function postBuy(Request $request)
    {
        $img_id = $request->img_id;
        $buy_id = $request->buy_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;
        // return $old_sorting;

        $data = BuyImages::where('sorting', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = BuyImages::find($img_id);
            // $change_data2->where('buy_id', $buy_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = BuyImages::where('img_id',$data->img_id);
        $change_data->where('buy_id', $buy_id);
        $change_data->update(['sorting' => $old_sorting]);


        $change_data2 = BuyImages::where('img_id',$img_id);
        $change_data2->where('buy_id', $buy_id);
        $change_data2->update(['sorting' => $new_sorting]);

        return redirect()->back();
    }
    public function postRent(Request $request)
    {
        $img_id = $request->img_id;
        $rent_id = $request->rent_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;
        // return $old_sorting;

        $data = RentImages::where('sorting', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = RentImages::find($img_id);
            // $change_data2->where('rent_id', $rent_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = RentImages::where('img_id',$data->img_id);
        $change_data->where('rent_id', $rent_id);
        $change_data->update(['sorting' => $old_sorting]);


        $change_data2 = RentImages::where('img_id',$img_id);
        $change_data2->where('rent_id', $rent_id);
        $change_data2->update(['sorting' => $new_sorting]);

        return redirect()->back();
    }
    public function postSale(Request $request)
    {
        $rent_id = $request->rent_id;
        // return $old_sorting;

        $data = Rent::where('rent_id', $rent_id)->first();

        $l = Location::where('location_id', $data->location_id)->first()->location_code;
        $count_num = Buy::count();
        $no = 0;
        if( $count_num != 0){
            $no = Buy::orderBy('buy_id','absc')->first()->buy_id;
        }
        $p = "";
        switch ($data->property_id) {
            case '1':
                $p = "L9";
                break;
            case '2':
                $p = "V".$data->bedrooms;
                break;
            case '3':
                $p = "A".$data->bedrooms;
                break;
            case '6':
                $p = "B8";
                break;
        }
        $n = 1111 + $no;
        $property_no = "S".$l.$p.$n;

        $input_tables = [
                        "property_no"   => $property_no,
                        "title"         => $data->title,
                        "description"   => $data->description,
                        "property_id"   => $data->property_id,
                        "listing_id"    => 0,
                        "location_id"   => $data->location_id,
                        "price"         => 0,
                        "indoor_area"   => $data->indoor_area,
                        "land_size"     => $data->land_size,
                        "address"       => $data->address,
                        "web_link"      => $data->web_link,
                        "private_comment" => $data->private_comment,
                        "latitude"      => $data->latitude,
                        "longitude"     => $data->longitude,
                        "land_title_id" => 0,
                        "land_type_id"  => 0,
                        "frontage"      => "",
                        "width"         => "",
                        "land_detail_id"    => "",
                        "building_state_id" => 0,
                        "building_owner_id" => 0,
                        "year"          => 0,
                        "bedrooms"      => $data->bedrooms,
                        "bathrooms"     => $data->bathrooms,
                        "floors"        => $data->floors,
                        "rental_program"    => 0,
                        "living_id"     => $data->living_id,
                        "community_id"  => $data->community_id,
                        "img_name"      => "",
                        "view_id"       => $data->view_id,
                        "promote"       => $data->promote,
                        "discount_price"    => $data->discount_price,
                        "is_available"  => 0,
                        "apm_storeys"   => "",
                        "apm_floor"     => "",
                        "apm_access"    => "",
                        ];
        // return $input_tables;
        $tablesDB = Buy::create($input_tables);

        $tablesDB2 = Rent::where('rent_id', $rent_id)->update(['is_sale' => 1]);
        return redirect()->back();
    }
    public function postIn(Request $request)
    {
        $student_id = $request->student_id;
        $in_dir = $request->in_dir;

        // check in_dir and set in_dir = null //
        $check_id = StudentTake::where('student_id', $student_id)->first()->in_dir;
        $a_stdid = explode(",", $check_id);
        if ($check_id != null) {
            foreach ($a_stdid as $stdid) {
                $input = StudentTake::where('student_id', $stdid)->update(['in_dir' => null, 'check_in' => 0]);
            }
            $input = StudentTake::where('student_id', $student_id)->update(['in_dir' => null, 'check_in' => 0]);
        }

        // Add tack in in_dir
        foreach ($in_dir as $key => $value) {
            $a_stu = '';
            foreach ($in_dir as $key2 => $value2) {
                if($value2 != $value){
                    $a_stu .= $value2.",";
                }
            }
            if(!empty($a_stu)){
               $stu_id = $a_stu.$student_id; 
            }
            if (empty($a_stu)) {
               $stu_id = $student_id;
            }
            $input = StudentTake::where('student_id', $value)->update(['in_dir' => $stu_id, 'check_in' => 1]);
        }
        $e_stu = implode(",", $in_dir);
        $data = StudentTake::where('student_id', $student_id)->update(['in_dir' => $e_stu, 'check_in' => 1]);
        return redirect()->back();
    }
    public function postOut(Request $request)
    {
        $student_id = $request->student_id;
        $out_dir = $request->out_dir;

        // check out_dir and set out_dir = null //
        $check_id = StudentTake::where('student_id', $student_id)->first()->out_dir;
        $a_stdid = explode(",", $check_id);
        if ($check_id != null) {
            foreach ($a_stdid as $stdid) {
                $input = StudentTake::where('student_id', $stdid)->update(['out_dir' => null, 'check_out' => 0]);
            }
            $input = StudentTake::where('student_id', $student_id)->update(['out_dir' => null, 'check_out' => 0]);
        }

        // Add tack in out_dir
        foreach ($out_dir as $key => $value) {
            $a_stu = '';
            foreach ($out_dir as $key2 => $value2) {
                if($value2 != $value){
                    $a_stu .= $value2.",";
                }
            }
            if(!empty($a_stu)){
               $stu_id = $a_stu.$student_id; 
            }
            if (empty($a_stu)) {
               $stu_id = $student_id;
            }
            $input = StudentTake::where('student_id', $value)->update(['out_dir' => $stu_id, 'check_out' => 1]);
        }
        $e_stu = implode(",", $out_dir);
        $data = StudentTake::where('student_id', $student_id)->update(['out_dir' => $e_stu, 'check_out' => 1]);
        return redirect()->back();
    }
    public function postReset(Request $request)
    {
        $student_id = $request->student_id;
        $in_dir = $request->in_dir;
        $delete = $request->delete;

        // check in_dir and set in_dir = null //
        if($delete == "in"){
            $check_id = StudentTake::where('student_id', $student_id)->first()->in_dir;
            $a_stdid = explode(",", $check_id);
            if ($check_id != null) {
                foreach ($a_stdid as $stdid) {
                    $input = StudentTake::where('student_id', $stdid)->update(['in_dir' => null, 'check_in' => 0]);
                }
                $input = StudentTake::where('student_id', $student_id)->update(['in_dir' => null, 'check_in' => 0]);
            }
        }

        if($delete == "out"){
            $check_id = StudentTake::where('student_id', $student_id)->first()->out_dir;
            $a_stdid = explode(",", $check_id);
            if ($check_id != null) {
                foreach ($a_stdid as $stdid) {
                    $input = StudentTake::where('student_id', $stdid)->update(['out_dir' => null, 'check_out' => 0]);
                }
                $input = StudentTake::where('student_id', $student_id)->update(['out_dir' => null, 'check_out' => 0]);
            }
        }

        return redirect()->back();
    }

}
    //--------------------------------------------------
