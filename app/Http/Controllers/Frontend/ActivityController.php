<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Images;
use App\Models\Activity;
use App\Models\ContactMsg;
use App\Models\Booking;

use Input;
use DB;
use Mail;
class ActivityController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\Activity'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function
        
        $this->a_search = ['activity_name']; // Array Search
        $this->path = 'activity'; // Url Path
        $this->view_path = 'frontend.activity'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $primaryKey = $obj_model->primaryKey;
        $path = $this->path;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'asc';

        $data = $obj_model;
        $data = $data->where('is_available',1);

        $search = Input::get('search');
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }

        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate(12);
        $url_to = $this->path;

        return view($this->view_path,compact('count_data','data','path','obj_model','obj_fn','data_user','url_to'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
       
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        $input['activity_id'] = $request->activity_id;
        $input['type'] = 1;
        $data = Booking::create($input);
        return redirect()->to($this->path);
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {
    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        
        $data = $obj_model->find($id);

        $data_image = Images::where('activity_id', $id)->where('is_available',1)->get();

        return view($this->view_path.'detail',compact('url_to','method','txt_manage','obj_model','obj_fn','data_image'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
    public function postcontact(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        // return $input;
        $data = ContactMsg::create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });

        return redirect()->to($this->path);
        
    }
}
