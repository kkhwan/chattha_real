<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\ContactMsg;
use App\Models\Community;
use App\Models\Features;
use App\Models\BuildingOwner;
use App\Models\BuildingState;
use App\Models\LandDetail;
use App\Models\LandTitle;
use App\Models\LandType;
use App\Models\ListingType;
use App\Models\Living;
use App\Models\Location;
use App\Models\PropertyType;
use App\Models\Service;
use App\Models\Suitability;
use App\Models\Rent;
use App\Models\Views;
use App\Models\BuyImages;
use App\Models\Booking;
use App\Models\Buy;

use Input;
use DB;
use Mail;
class BuyController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\Buy'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->path = 'sale'; // Url Path
        $this->view_path = 'frontend.sale'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $data = $obj_model;
        $data = $data->where('is_available', 1);

        $property_type = Input::get('property_type');
        if(!empty($property_type)){
            $data = $data->where('property_id', $property_type);
        }

        $property_id = Input::get('property_id');
        if(!empty($property_id)){
            $data = $data->where('property_no', $property_id);
        }

        function multiexplode ($delimiters,$string) {
            $ready = str_replace($delimiters, $delimiters[0], $string);
            $launch = explode($delimiters[0], $ready);
            return  $launch;
        }

        $amount = Input::get('amount');
        if(!empty($amount)){
            $a_amount = multiexplode(array("-","","THB"),$amount);
            $v_amount1 = 0;
            $v_amount2 = 0;
            foreach ($a_amount as $key2 => $value2) {
                
                if($key2 == 0 ){
                    $v_amount1 = $value2;
                }
                if($key2 == 1 ){
                    $v_amount2 = $value2;
                }
            }
            // $a = $v_amount1." ".$v_amount2;
            // return $a;
            $data = $data->whereBetween('price',[$v_amount1,$v_amount2]);
            // $data = $data->where('price', '>=' ,$v_amount1);
            // $data = $data->where('price', '<=' ,$v_amount2);
        }

        $room = Input::get('room');
        if(!empty($room)){
            $a_room = multiexplode(array("-","","ROOM"),$room);    
            $v_room1 = 0;
            $v_room2 = 0;  
            foreach ($a_room as $key3 => $value3) {
                if($key3 == 0 ){
                    $v_room1 = $value3;
                }
                if($key3 == 1 ){
                    $v_room2 = $value3;
                }
            }
            // $a = $v_room1." ".$v_room2;
            // return $room;
            $data = $data->whereBetween('bedrooms',[$v_room1,$v_room2]);
            // $data = $data->where('bedrooms', '>=' ,$v_room1);
            // $data = $data->where('bedrooms', '<=' ,$v_room2);
        }

        $chbLocation = Input::get('chbLocation');
        if(!empty($chbLocation)){
            // $data = $data->where('location_id', $chbLocation);
            // return $chbLocation;
            // $a_location = implode(",", $chbLocation);
            // $inar_location = "[".$a_location."]";
            // return $a_location;
            // return $inar_location;
            $data = $data->where('location_id', $chbLocation);
            // $data->get();
        }

        $count_data = $data->count();
        $data = $data->orderBy('buy_id','desc');
        $data = $data->paginate(9);
        // $data = $data->get();
        // return $data;

        $property_type = PropertyType::where('type', 1)->orWhere('type', 3)->get();
        $listing_type = ListingType::all();
        $landDetail = LandDetail::all();
        $land_title = LandTitle::all();
        $land_type = LandType::all();
        $buildingOwner = BuildingOwner::all();
        $buildingState = BuildingState::all();
        $community = Community::where('type', 1)->orWhere('type', 3)->get();
        $living = Living::where('type', 1)->orWhere('type', 3)->get();
        $location = Location::all();
        $views = Views::all();
        $buy_images = BuyImages::all();

        return view($this->view_path,compact('page_title','count_data','data','path','obj_model','obj_fn', 'community', 'buildingOwner', 'buildingState', 'landDetail', 'land_title', 'listing_type', 'living', 'property_type', 'land_type','location','room','views','buy_images','chbLocation'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $data = $obj_model;

        $property_type = Input::get('property_type');
        if(!empty($property_type)){
            $data = $data->where('property_type', $property_type);
        }

        $property_id = Input::get('property_id');
        if(!empty($property_id)){
            $data = $data->where('property_id', $property_id);
        }

        function multiexplode ($delimiters,$string) {
            $ready = str_replace($delimiters, $delimiters[0], $string);
            $launch = explode($delimiters[0], $ready);
            return  $launch;
        }

        $amount = Input::get('amount');
        if(!empty($amount)){
            $a_amount = multiexplode(array("-","","THB"),$amount);
            $v_amount1 = "";
            $v_amount2 = "";
            foreach ($a_amount as $key2 => $value2) {
                
                if($key2 == 0 ){
                    $v_amount1 = $value2;
                }
                if($key2 == 1 ){
                    $v_amount2 = $value2;
                }
            }

            $data = $data->whereBetween('price', [$v_amount1, $v_amount2]);
        }
        $room = Input::get('room');
        if(!empty($room)){
            $a_room = multiexplode(array("-","","ROOM"),$room);      
            foreach ($a_room as $key3 => $value3) {
                $v_room1 = "";
                $v_room2 = "";
                if($key2 == 0 ){
                    $v_room1 = $value2;
                }
                if($key2 == 1 ){
                    $v_amount2 = $value2;
                }
                $data = $data->whereBetween('bedrooms', [$v_room1, $v_room2]);
            }
        }
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        // return $input;
        $input['buy_id'] = $request->buy_id;
        $input['type'] = 2;
        $data = Booking::create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
    public function postcontact(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        // return $input;
        $data = ContactMsg::create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });

        return redirect()->to($this->path);
        
    }
}
