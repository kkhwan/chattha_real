<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Student;

use Input;
use DB;
use Mail;
class ContactController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\ContactMsg'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->path = 'contact'; // Url Path
        $this->view_path = 'frontend.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        // return view('frontend.contact');
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $url_to = $this->path;
        $method = 'POST';

        return view($this->view_path.'contact',compact('url_to','method','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $email = $request->email;
        $msg = $request->msg;
        $name =$request->name;
        $input = $request->all(); // Get all post from form
        $data = $obj_model->create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact-email', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });
        // $send =  Mail::send('emails.contact-email', ['email' => $email, 'msg' => $msg, 'name' => $name], function ($m) use($email,$msg,$name){
        //     $email1 = 'lovetome0824@gmail.com';
        //     $name1 = 'khwanta Naknaen';
        //     $m->from($email, $name);
        //     $m->to($email1, $name1);
        //     $m->subject('Contact');
        // });

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
}
