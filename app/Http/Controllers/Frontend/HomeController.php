<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\ContactMsg;

use Input;
use DB;
use Mail;
class HomeController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\ContactMsg'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->path = '/'; // Url Path
        $this->view_path = 'frontend.index2'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $url_to = $this->path;
        
        return view($this->view_path,compact('url_to'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        // return $input;
        $data = ContactMsg::create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
}
