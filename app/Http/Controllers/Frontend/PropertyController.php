<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Student;
use App\Models\Images;

use Input;
use DB;
use Mail;
class PropertyController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\Activity'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->path = 'property'; // Url Path
        $this->view_path = 'frontend.property.'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        return view($this->view_path.'index');
        // $obj_fn = $this->obj_fn;
        // $obj_model = $this->obj_model;
        // $primaryKey = $obj_model->primaryKey;
        // $path = $this->path;
        // $per_page = config()->get('constants.PER_PAGE');

        // $order_by = Input::get('order_by');
        // if(empty($order_by)) $order_by = $obj_model->primaryKey;
        // $sort_by = Input::get('sort_by');
        // if(empty($sort_by)) $sort_by = 'desc';

        // $data = $obj_model;
        // $data = $data->where('is_available',1);

        // $count_data = $data->count();
        // $data = $data->orderBy($order_by,$sort_by);
        // $data = $data->paginate($per_page);

        // return view($this->view_path.'activties',compact('count_data','data','path','obj_model','obj_fn','data_user'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
       
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {
    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        
        $data = $obj_model->find($id);

        $data_image = Images::where('activity_id', $id)->where('is_available',1)->get();

        return view($this->view_path.'detail',compact('url_to','method','txt_manage','obj_model','obj_fn','data_image'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
}
