<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\AdminRole;
use App\Models\Admin;
use App\Models\Page;
use App\Models\Rent;

use App\Models\Community;
use App\Models\Features;
use App\Models\BuildingOwner;
use App\Models\BuildingState;
use App\Models\LandDetail;
use App\Models\LandTitle;
use App\Models\LandType;
use App\Models\ListingType;
use App\Models\Living;
use App\Models\Location;
use App\Models\PropertyType;
use App\Models\Service;
use App\Models\Suitability;
use App\Models\Views;
use App\Models\RentImages;
use App\Models\Booking;

use Input;
use DB;
use Mail;
class RentelController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\Rent'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->path = 'rentel'; // Url Path
        $this->view_path = 'frontend.rent'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $data = $obj_model;
        $data = $data->where('is_available', 1);

        $property_type = Input::get('property_type');
        if(!empty($property_type)){
            $data = $data->where('property_id', $property_type);
        }

        $property_id = Input::get('property_id');
        if(!empty($property_id)){
            $data = $data->where('property_no', $property_id);
        }

        function multiexplode ($delimiters,$string) {
            $ready = str_replace($delimiters, $delimiters[0], $string);
            $launch = explode($delimiters[0], $ready);
            return  $launch;
        }

        $amount = Input::get('amount');
        if(!empty($amount)){
            $a_amount = multiexplode(array("-","","THB"),$amount);
            $v_amount1 = 0;
            $v_amount2 = 0;
            foreach ($a_amount as $key2 => $value2) {
                
                if($key2 == 0 ){
                    $v_amount1 = $value2;
                }
                if($key2 == 1 ){
                    $v_amount2 = $value2;
                }
            }
            // $a = $v_amount1." ".$v_amount2;
            // return $a;
            $data = $data->whereBetween('high_daily',[$v_amount1,$v_amount2]);
            // $data = $data->where('high_daily', '>=' ,$v_amount1);
            // $data = $data->where('high_daily', '<=' ,$v_amount2);
        }

        $room = Input::get('room');
        if(!empty($room)){
            $a_room = multiexplode(array("-","","ROOM"),$room);    
            $v_room1 = 0;
            $v_room2 = 0;  
            foreach ($a_room as $key3 => $value3) {
                if($key3 == 0 ){
                    $v_room1 = $value3;
                }
                if($key3 == 1 ){
                    $v_room2 = $value3;
                }
            }
            // $a = $v_room1." ".$v_room2;
            $data = $data->whereBetween('bedrooms',[$v_room1,$v_room2]);
            // $data = $data->where('bedrooms', '>=' ,$v_room1);
            // $data = $data->where('bedrooms', '<=' ,$v_room2);
        }

        $chbLocation = Input::get('chbLocation');
        if(!empty($chbLocation)){
            $data = $data->where('location_id', $chbLocation);
            // $a_location = implode(",", $location1);
            // $data = $data->wherein('bedrooms', [$a_location]);
        }

        $count_data = $data->count();
        $data = $data->orderBy('rent_id','desc');
        $data = $data->paginate(9);
        // $data = $data->get();
        // return $data;

        // $location = Location::all();
        
        $community = Community::where('type', 2)->orWhere('type', 3)->get();
        $features = Features::where('type', 2)->orWhere('type', 3)->get();
        $listing_type = ListingType::where('type', 2)->orWhere('type', 3)->get();
        $living = Living::where('type', 2)->orWhere('type', 3)->get();
        $property_type = PropertyType::where('type', 2)->orWhere('type', 3)->get();
        $service = Service::where('type', 2)->orWhere('type', 3)->get();
        $suitability = Suitability::where('type', 2)->orWhere('type', 3)->get();
        $location = Location::all();
        $views = Views::all();
        $rent_images = RentImages::all();

        return view($this->view_path,compact('page_title','count_data','data','path','obj_model','obj_fn', 'community', 'features', 'listing_type', 'living', 'property_type', 'service', 'suitability','location','room', 'location', 'chbLocation','views','rent_images'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $data = $obj_model;

        $property_type = Input::get('property_type');
        if(!empty($property_type)){
            $data = $data->where('property_type', $property_type);
        }

        $property_id = Input::get('property_id');
        if(!empty($property_id)){
            $data = $data->where('property_id', $property_id);
        }

        function multiexplode ($delimiters,$string) {
            $ready = str_replace($delimiters, $delimiters[0], $string);
            $launch = explode($delimiters[0], $ready);
            return  $launch;
        }

        $amount = Input::get('amount');
        if(!empty($amount)){
            $a_amount = multiexplode(array("-","","THB"),$amount);
            $v_amount1 = "";
            $v_amount2 = "";
            foreach ($a_amount as $key2 => $value2) {
                
                if($key2 == 0 ){
                    $v_amount1 = $value2;
                }
                if($key2 == 1 ){
                    $v_amount2 = $value2;
                }
            }

            $data = $data->whereBetween('price', [$v_amount1, $v_amount2]);
        }

        $room = Input::get('room');
        if(!empty($room)){
            $a_room = multiexplode(array("-","","ROOM"),$room);      
            foreach ($a_room as $key3 => $value3) {
                $v_room1 = "";
                $v_room2 = "";
                if($key2 == 0 ){
                    $v_room1 = $value2;
                }
                if($key2 == 1 ){
                    $v_amount2 = $value2;
                }
                $data = $data->whereBetween('bedrooms', [$v_room1, $v_room2]);
            }
        }
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        // return $input;
        $input['rent_id'] = $request->rent_id;
        $input['type'] = 3;
        $data = Booking::create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {
    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        
        $data = $obj_model->find($id);

        $data_image = Images::where('activity_id', $id)->where('is_available',1)->get();

        return view($this->view_path.'detail',compact('url_to','method','txt_manage','obj_model','obj_fn','data_image'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
    public function postcontact(Request $request)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $input = $request->all(); // Get all post from form
        // return $input;
        $data = ContactMsg::create($input);

        // $id = Input::get('id');
        // $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        // $data->parent_id = $id;
        // $data->status = 1;
        // $data->save();

        // $data2 = $model::find($id);
        // $data2->status = 1;
        // $data2->save();

        // Mail::send('email.contact', ['data' => $data2, 'msg' => $data->msg],  function($message) use ($data2,$data)
        // {
        //     $message->from($data->email , $data->name);
        //     $message->to( $data2->email , $data2->name)->subject('Welcome!');
        // });

        return redirect()->to($this->path);
        
    }
}
