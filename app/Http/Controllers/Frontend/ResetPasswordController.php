<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Models\Student;
use App\Models\StudentDetail;

use Input;
use DB;
use Mail;
use Hash;

class ResetPasswordController extends Controller
{

    public function __construct()
    {
        $this->model = 'App\Models\Student'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->path = '_student/reset_password'; // Url Path
        $this->view_path = 'frontend.profile.'; // View Path
        $this->student = session()->get('s_student_id');
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        return redirect()->to($this->path.'/'.$this->student.'/edit?1');
    }
    // ------------------------------------ View Add Page
    public function create()
    {
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    { 
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        if($this->student != $id){
            return abort(503);
        }
        $data = $obj_model->find($id);

        return view($this->view_path.'reset_password',compact('data','url_to','method','obj_model','obj_fn','data_user'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    { 
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $password = $request->password_confirm;
        $input = $request->except(['_token','_method','str_param','password_confirm']); // Get all post from form
        $input['password'] = Hash::make($password);
        $data = $obj_model->where('student_id', $id)->update($input);

        // $data = $obj_model->find($id);
        return redirect()->to('_student');
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }
}
