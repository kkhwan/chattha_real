<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Student;

use Hash;
use Cookie;
class StudentLoginController extends Controller
{
    public function __construct()
    {

    }
    public function getIndex(){
        if(!session()->has('s_student_id'))
            return view('frontend.signin');
        else
            return redirect()->to('_student/');
    }
    public function postForm(Request $request){
        $data = Student::where('student_id',$request->student_id)->first();
        if(empty($data))
            return redirect()->back()->with(['error','ไม่มีชื่อผู้ใช้งานนี้ในระบบค่ะ']);
        if(!Hash::check($request->password,$data->password))
            return redirect()->back()->with(['error','รหัสผ่านไม่ถูกต้องค่ะ']);

        $remember_me = $request->remember;
        // session()->put('s_doctor_id',$data->id);
        session()->put('s_student_id',$data->student_id);
        session()->put('s_student_name',$data->stud_name_en.' '.$data->stud_sname_en);
        session()->put('s_student_major',$data->major_id);
        if($remember_me == 1){
            Cookie::queue('c_student_id',$data->student_id,525600);
        }else{
            Cookie::queue(Cookie::forget('c_student_id'));
        }
        return redirect()->to('_student/');
    }

    public function logout(){
        session()->flush();
        Cookie::queue(Cookie::forget('c_student_id'));

        return redirect()->to('_student/login');
    }
}
