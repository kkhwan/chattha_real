<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Student;

use Cookie;

class StudentLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $c_student_id = Cookie::get('c_student_id');
        $s_student_id = session()->get('s_student_id');
        if(empty($s_student_id) && empty($c_student_id)) {
            return redirect()->to('_student/login');
        }
        if(empty($s_student_id) && !empty($c_student_id) ){
            $data = Student::find($c_student_id);
            if(empty($data)){
                return redirect()->to('_student/login');
            } else {
                session()->put('s_student_id',$data->student_id);
                session()->put('s_student_name',$data->stud_name_en.' '.$data->stud_sname_en);
                session()->put('s_student_major',$data->major_id);
                // session()->put('s_student_name',$data->stud_name_en);
            }
        }
        return $next($request);
    }
}
