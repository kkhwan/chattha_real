<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('lang/{lang}',[
    'as'=>'lang',
    'uses'=>'Frontend\LocaleController@lang'
]);

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Clear Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('resend-email/{token}','Frontend\LoginController@resendMail');
Route::get('/token', function(){
  return csrf_token();
});
Route::controller('send-mail','Frontend\SendmailController');
Route::get('form-mail',function(){
    return view('emails.doctor-register');
});

//------------------ Home ------------------------//
Route::get('/', 'Frontend\HomeController@index');
Route::post('/contact_us', 'Frontend\HomeController@store');
//------------------ Home ------------------------//
Route::get('property', 'Frontend\PropertyController@index');
Route::post('yacht', 'Frontend\YachtController@postcontact');
//------------------ about_us ------------------------//
Route::get('about_us', 'Frontend\AboutUsController@index');
Route::post('about_us/contact', 'Frontend\AboutUsController@postcontact');
//------------------ vehicle ------------------------//
Route::get('vehicle', 'Frontend\VehicleController@index');
Route::post('/vehicle/store', 'Frontend\VehicleController@store');
Route::post('/vehicle/contact', 'Frontend\VehicleController@postcontact');
//------------------ phuket ------------------------//
Route::get('phuket', 'Frontend\PhuketController@index');
Route::post('/phuket/contact', 'Frontend\PhuketController@postcontact');
//------------------ service ------------------------//
Route::get('service', 'Frontend\ServiceController@index');
Route::post('service/contact', 'Frontend\ServiceController@postcontact');
//------------------ Activity ------------------------//
Route::get('activity', 'Frontend\ActivityController@index');
Route::post('/activity/store', 'Frontend\ActivityController@store');
Route::post('/activity/contact', 'Frontend\ActivityController@postcontact');
//------------------ contact Us ------------------------//
Route::get('/contact', 'Frontend\ContactController@create');
Route::post('/contact/store', 'Frontend\ContactController@store');
//------------------ sales ------------------------//
// Route::get('/sale', 'Frontend\SaleController@index');
// Route::post('/sale/store', 'Frontend\SaleController@store');
// Route::post('/sale/contact', 'Frontend\SaleController@postcontact');

Route::get('/sale', 'Frontend\BuyController@index');
Route::post('/sale/store', 'Frontend\BuyController@store');
Route::post('/sale/contact', 'Frontend\BuyController@postcontact');

//------------------ rentels ------------------------//
Route::get('/rentel', 'Frontend\RentelController@index');
Route::post('/rentel/store', 'Frontend\RentelController@store');
Route::post('/rentel/contact', 'Frontend\RentelController@postcontact');

//---------------------------------------------------

/* --------------------- Student ------------------- */
Route::group(['prefix' => '_student'], function () {
    Route::controller('login', 'Frontend\StudentLoginController');
    Route::get('logout', 'Frontend\StudentLoginController@logout');
    Route::post('api-active','Frontend\ApiController@changeActive');
});

Route::group(['middleware' => 'student', 'prefix' => '_student'],function (){
    Route::get('/', 'Frontend\ProfileController@index');
});

/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');

    Route::post('api-active','Backend\ApiController@changeActive');
    Route::post('api-site','Backend\ApiController@changeSite');
    Route::post('api-department','Backend\ApiController@changeDepartment');
    Route::post('api-product','Backend\ApiController@changeProduct');
});

Route::group(['middleware'=>'admin','prefix' => config()->get('constants.BO_NAME')], function () {
    Route::get('/', 'Backend\HomeController@index');
    Route::resource('user-management','Backend\AdminController');
    Route::resource('role','Backend\AdminRoleController');
    Route::resource('page','Backend\PageController');
    Route::resource('permission','Backend\PermissionController');
    Route::resource('default','Backend\DefaultController');

    Route::controller('/sequence','Backend\SequenceController');
    Route::resource('reset_password','Backend\ResetPasswordController');
    Route::post('check-email','Backend\CheckEmailController@checkemail');
    Route::post('check-department','Backend\CheckDepartmentController@checkdepartment');
    Route::post('check-username','Backend\CheckUsernameController@checkuser');
    Route::post('status-change','Backend\ChangeStatusController@chk_status');

    Route::resource('property_type','Backend\PropertyTypeController');
    Route::resource('living','Backend\LivingController');
    Route::resource('features', 'Backend\FeaturesController');
    Route::resource('community', 'Backend\CommunityController');
    Route::resource('services', 'Backend\ServicesController');
    Route::resource('suitability', 'Backend\SuitabilityController');
    Route::resource('listing_type', 'Backend\ListingTypeController');
    Route::resource('views', 'Backend\ViewsController');

    Route::resource('buy', 'Backend\BuyController');
    Route::resource('rent', 'Backend\RentController');
    Route::resource('buy_img', 'Backend\BuyImagesController');
    Route::resource('rent_img', 'Backend\RentImagesController');


    Route::resource('car', 'Backend\CarController');
    Route::resource('bike', 'Backend\BikeController');
    Route::resource('yacht', 'Backend\YachtController');
    Route::resource('hire', 'Backend\HireController');

    Route::resource('activity','Backend\ActivityController');
    Route::resource('images','Backend\ImagesController');

    Route::resource('phuket', 'Backend\PhuketController');
    Route::resource('contact', 'Backend\ContactController');
    Route::resource('booking', 'Backend\BookingController');
    Route::resource('our_service', 'Backend\OurServiceController');
    
});

/* --------------------- API ------------------- */
Route::group(['prefix' => 'api'], function () {
    Route::post('login/sign-in','API\LoginController@signin');
});
/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('/',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
