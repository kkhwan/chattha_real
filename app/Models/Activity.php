<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use SoftDeletes;
    public $table = 'activity';
    public $primaryKey = 'activity_id';
    public $fillable = ['activity_name','title_name','detail','img_name','date','is_available','is_book'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
