<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
    public $table = 'booking';
    public $primaryKey = 'booking_id';
    public $fillable = ['activity_id', 'buy_id', 'rent_id', 'vehicle_id', 'name', 'email', 'phone', 'location', 'date_from', 'date_to', 'guests', 'children', 'msg', 'type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
