<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuildingState extends Model
{
    use SoftDeletes;
    public $table = 'building_state';
    public $primaryKey = 'building_state_id';
    public $fillable = ['state_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
