<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buy extends Model
{
    use SoftDeletes;
    public $table = 'buy';
    public $primaryKey = 'buy_id';
    public $fillable = ['property_no', 'title', 'description', 'property_id', 'listing_id', 'location_id', 'price', 'indoor_area', 'land_size', 'address', 'web_link', 'private_comment', 'latitude', 'longitude', 'land_title_id', 'land_type_id', 'frontage', 'width', 'land_detail_id', 'building_state_id', 'building_owner_id', 'year', 'bedrooms', 'bathrooms', 'floors', 'rental_program', 'living_id', 'community_id', 'img_name', 'apm_storeys', 'apm_floor', 'apm_access', 'view_id', 'promote', 'discount_price', 'is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
