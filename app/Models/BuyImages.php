<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuyImages extends Model
{
    use SoftDeletes;
    public $table = 'buy_images';
    public $primaryKey = 'img_id';
    public $fillable = ['buy_id', 'img_name', 'is_available', 'sorting'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
