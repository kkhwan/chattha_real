<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Community extends Model
{
    use SoftDeletes;
    public $table = 'community';
    public $primaryKey = 'community_id';
    public $fillable = ['community_name','type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
