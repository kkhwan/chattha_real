<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ContactMsg extends Model
{
    use SoftDeletes;
    public $table = 'contact_msg';
    public $primaryKey = 'contact_msg_id';
    public $fillable = ['admin_id','name','email','msg','mobile','nationality','status'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}


?>