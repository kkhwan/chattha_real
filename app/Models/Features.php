<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Features extends Model
{
    use SoftDeletes;
    public $table = 'features';
    public $primaryKey = 'features_id';
    public $fillable = ['features_name','type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
