<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hire extends Model
{
    use SoftDeletes;
    public $table = 'hire';
    public $primaryKey = 'hire_id';
    public $fillable = ['name', 'vehicle_id', 'email', 'phone', 'location', 'date_from', 'date_to', 'msg'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
