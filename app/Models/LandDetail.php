<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LandDetail extends Model
{
    use SoftDeletes;
    public $table = 'land_detail';
    public $primaryKey = 'land_detail_id';
    public $fillable = ['detail_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
