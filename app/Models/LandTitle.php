<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LandTitle extends Model
{
    use SoftDeletes;
    public $table = 'land_title';
    public $primaryKey = 'land_title_id';
    public $fillable = ['title_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
