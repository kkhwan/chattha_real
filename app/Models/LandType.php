<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LandType extends Model
{
    use SoftDeletes;
    public $table = 'land_type';
    public $primaryKey = 'land_type_id';
    public $fillable = ['type_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
