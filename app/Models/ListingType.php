<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingType extends Model
{
    use SoftDeletes;
    public $table = 'listing_type';
    public $primaryKey = 'listing_id';
    public $fillable = ['listing_name','type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
