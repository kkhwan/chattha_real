<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Living extends Model
{
    use SoftDeletes;
    public $table = 'living';
    public $primaryKey = 'living_id';
    public $fillable = ['living_name','type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
