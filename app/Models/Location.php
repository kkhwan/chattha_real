<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    public $table = 'location';
    public $primaryKey = 'location_id';
    public $fillable = ['location_name', 'location_code'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
