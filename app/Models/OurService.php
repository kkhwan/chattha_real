<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurService extends Model
{
    use SoftDeletes;
    public $table = 'our_service';
    public $primaryKey = 'our_id';
    public $fillable = ['our_name', 'detail', 'img_name', 'is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
