<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phuket extends Model
{
    use SoftDeletes;
    public $table = 'phuket';
    public $primaryKey = 'phuket_id';
    public $fillable = ['phuket_name', 'detail', 'img_name', 'is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
