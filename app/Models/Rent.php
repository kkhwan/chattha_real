<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rent extends Model
{
    use SoftDeletes;
    public $table = 'rent';
    public $primaryKey = 'rent_id';
    public $fillable = ['property_no', 'title', 'description', 'property_id', 'location_id', 'low_from', 'low_to', 'high_from', 'high_to', 'peak_from', 'peak_to', 'low1_from', 'low1_to', 'high1_from', 'high1_to', 'low_month', 'low_weekly', 'low_daily', 'high_month', 'high_weekly', 'high_daily', 'peak_month', 'peak_weekly', 'peak_daily', 'low1_month', 'low1_weekly', 'low1_daily', 'high1_month', 'high1_weekly', 'high1_daily', 'remark', 'deposit', 'minimal_rent', 'period', 'bedrooms', 'bathrooms', 'floors', 'indoor_area', 'land_size', 'address', 'web_link', 'private_comment', 'latitude', 'longitude', 'living_id', 'features_id', 'community_id', 'service_id', 'suitability_id', 'img_name', 'owner_name', 'owner_tel', 'owner_email', 'owner_address', 'owner_remark', 'view_id', 'promote', 'discount_price', 'is_available', 'is_sale'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
