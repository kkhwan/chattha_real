<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentImages extends Model
{
    use SoftDeletes;
    public $table = 'rent_images';
    public $primaryKey = 'img_id';
    public $fillable = ['rent_id', 'img_name', 'is_available', 'sorting'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
