<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Reply extends Model
{
    use SoftDeletes;
    public $table = 'b_reply';
    public $primaryKey = 'reply_id';
    public $fillable = ['blog_id','student_id','commemt_msg','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
