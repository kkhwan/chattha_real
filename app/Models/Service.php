<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;
    public $table = 'service';
    public $primaryKey = 'service_id';
    public $fillable = ['service_name','type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
