<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    public $table = 'user';
    public $primaryKey = 'user_id';
    public $fillable = ['firstname','lastname','gender','birth_date','email','phone','password','overview','img_name','is_doctor','api_key','system'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
