<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;
    public $table = 'vehicle';
    public $primaryKey = 'vehicle_id';
    public $fillable = ['name', 'low_from', 'low_to', 'high_from', 'high_to', 'low_price', 'high_pice', 'engine', 'seat_capac', 'auto_manual', 'passenger', 'overnight', 'crew', 'cabin_no', 'cruising_speed', 'type','img_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
