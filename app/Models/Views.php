<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Views extends Model
{
    use SoftDeletes;
    public $table = 'views';
    public $primaryKey = 'view_id';
    public $fillable = ['view_name', 'type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
