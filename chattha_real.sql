-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2017 at 04:09 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chattha_real`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `activity_id` int(11) NOT NULL,
  `activity_name` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_available` tinyint(4) NOT NULL COMMENT '0=No, 1=Yes',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`activity_id`, `activity_name`, `detail`, `date`, `is_available`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Paty', 'birthday', '2017-06-25 17:00:00', 1, '2017-07-17 06:27:40', '2017-07-17 06:27:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_role_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_role_id`, `firstname`, `lastname`, `username`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Chattha Team', 'Admin', 'admin', '$2y$10$Rf/81pTQD8Bblyx5/XqMY.SMbqNY7sdzz7m8Me/tZIYLHNux9vEXO', '2016-06-11 16:13:26', '2017-07-14 10:01:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `admin_role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`admin_role_id`, `role_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bangkok Solutions Team', '2016-06-12 18:39:39', '0000-00-00 00:00:00', NULL),
(2, 'User', '2016-06-12 14:03:12', '2017-07-14 04:12:12', '2017-07-14 04:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `a_images`
--

CREATE TABLE `a_images` (
  `img_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `is_available` tinyint(4) NOT NULL COMMENT '0=not show, 1=show',
  `sorting` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `building_owner`
--

CREATE TABLE `building_owner` (
  `building_owner_id` int(11) NOT NULL,
  `owner_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `building_owner`
--

INSERT INTO `building_owner` (`building_owner_id`, `owner_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Leasehold', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Thai Freehold', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Foreigner Freehold', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Thai Company', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `building_state`
--

CREATE TABLE `building_state` (
  `building_state_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `building_state`
--

INSERT INTO `building_state` (`building_state_id`, `state_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ready to move in', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Under construction', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Off plan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buy`
--

CREATE TABLE `buy` (
  `buy_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `property_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `indoor_area` varchar(255) NOT NULL,
  `land_size` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `web_link` varchar(255) NOT NULL,
  `private_comment` text NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `land_title_id` int(11) NOT NULL,
  `land_type_id` int(11) NOT NULL,
  `frontage` varchar(100) NOT NULL COMMENT '(m)',
  `width` varchar(100) NOT NULL COMMENT '(m)',
  `land_detail_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...	',
  `building_state_id` int(11) NOT NULL,
  `building_owner_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `bedrooms` int(11) NOT NULL,
  `floors` int(11) NOT NULL,
  `rental_program` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `living_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `community_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `img_name` varchar(255) NOT NULL,
  `apm_storeys` varchar(255) NOT NULL,
  `apm_floor` varchar(255) NOT NULL,
  `apm_access` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buy`
--

INSERT INTO `buy` (`buy_id`, `title`, `description`, `property_id`, `location_id`, `listing_id`, `price`, `indoor_area`, `land_size`, `address`, `web_link`, `private_comment`, `latitude`, `longitude`, `land_title_id`, `land_type_id`, `frontage`, `width`, `land_detail_id`, `building_state_id`, `building_owner_id`, `year`, `bedrooms`, `floors`, `rental_program`, `living_id`, `community_id`, `img_name`, `apm_storeys`, `apm_floor`, `apm_access`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', '', 2, 0, 1, '', '', '', '', '', '', '', '', 1, 2, '', '', '1,2,3,4,5', 1, 2, 2019, 2, 0, 0, '2,3,4,5,6,7,8', '1,3,4,5,6,7', '', '', '', 0, '2017-07-19 03:21:21', '2017-07-19 03:21:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `community`
--

CREATE TABLE `community` (
  `community_id` int(11) NOT NULL,
  `community_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `community`
--

INSERT INTO `community` (`community_id`, `community_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Security/Gated Estate', 3, '2017-07-17 03:59:45', '2017-07-18 09:37:55', NULL),
(2, 'Tennis Court', 2, '2017-07-18 09:35:57', '2017-07-18 09:35:57', NULL),
(3, 'Restaurant', 3, '2017-07-18 09:36:07', '2017-07-18 09:38:02', NULL),
(4, 'Gym', 3, '2017-07-18 09:36:16', '2017-07-18 09:38:07', NULL),
(5, 'Clubhouse', 3, '2017-07-18 09:36:30', '2017-07-18 09:38:14', NULL),
(6, 'Community pool', 3, '2017-07-18 09:36:39', '2017-07-18 09:38:19', NULL),
(7, 'Management office', 1, '2017-07-18 09:37:37', '2017-07-18 09:38:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `features_id` int(11) NOT NULL,
  `features_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`features_id`, `features_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Internet', 2, '2017-07-17 04:48:07', '2017-07-18 10:38:10', NULL),
(2, 'Air Conditioning', 2, '2017-07-18 10:38:18', '2017-07-18 10:38:18', NULL),
(3, 'Phone line', 2, '2017-07-18 10:38:27', '2017-07-18 10:38:27', NULL),
(4, 'Satellite TV', 2, '2017-07-18 10:38:38', '2017-07-18 10:38:38', NULL),
(5, 'Refridgerator', 2, '2017-07-18 10:38:48', '2017-07-18 10:38:48', NULL),
(6, 'Washing machine', 2, '2017-07-18 10:38:57', '2017-07-18 10:38:57', NULL),
(7, 'Dishwasher', 2, '2017-07-18 10:39:07', '2017-07-18 10:39:14', NULL),
(8, 'Microwave', 2, '2017-07-18 10:39:22', '2017-07-18 10:39:22', NULL),
(9, 'Oven', 2, '2017-07-18 10:40:04', '2017-07-18 10:40:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_detail`
--

CREATE TABLE `land_detail` (
  `land_detail_id` int(11) NOT NULL,
  `detail_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `land_detail`
--

INSERT INTO `land_detail` (`land_detail_id`, `detail_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Phoneline', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Road', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Electricity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Water', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'Sewage', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_title`
--

CREATE TABLE `land_title` (
  `land_title_id` int(11) NOT NULL,
  `title_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `land_title`
--

INSERT INTO `land_title` (`land_title_id`, `title_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Chanote', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Nor Sor 3 Gor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_type`
--

CREATE TABLE `land_type` (
  `land_type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `land_type`
--

INSERT INTO `land_type` (`land_type_id`, `type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Valley, Flat', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Hill', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Beachfront', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `listing_type`
--

CREATE TABLE `listing_type` (
  `listing_id` int(11) NOT NULL,
  `listing_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat	',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `listing_type`
--

INSERT INTO `listing_type` (`listing_id`, `listing_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sale', 1, '2017-07-17 05:09:50', '2017-07-18 08:33:42', NULL),
(2, 'Lease', 1, '2017-07-18 08:33:26', '2017-07-18 08:33:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `living`
--

CREATE TABLE `living` (
  `living_id` int(11) NOT NULL,
  `living_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `living`
--

INSERT INTO `living` (`living_id`, `living_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Fully furnished', 2, '2017-07-17 04:50:37', '2017-07-18 09:42:59', NULL),
(2, 'Furnished', 1, '2017-07-18 09:43:10', '2017-07-18 09:43:10', NULL),
(3, 'Livingroom', 3, '2017-07-18 09:43:19', '2017-07-18 09:43:19', NULL),
(4, 'Kitchen', 3, '2017-07-18 09:43:35', '2017-07-18 09:43:35', NULL),
(5, 'Diningroom', 3, '2017-07-18 09:43:48', '2017-07-18 09:43:48', NULL),
(6, 'Room for a maid', 1, '2017-07-18 09:44:00', '2017-07-18 09:44:00', NULL),
(7, 'Office', 3, '2017-07-18 09:44:44', '2017-07-18 09:44:44', NULL),
(8, 'Balcony/Terrace', 3, '2017-07-18 09:45:09', '2017-07-18 09:45:09', NULL),
(9, 'Garden', 3, '2017-07-18 09:45:17', '2017-07-18 09:45:17', NULL),
(10, 'Car park', 3, '2017-07-18 09:45:26', '2017-07-18 09:45:26', NULL),
(11, 'Private pool', 3, '2017-07-18 09:45:35', '2017-07-18 09:45:35', NULL),
(12, 'Sea view', 3, '2017-07-18 09:45:43', '2017-07-18 09:45:43', NULL),
(13, 'Near beach', 3, '2017-07-18 09:45:53', '2017-07-18 09:46:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `location_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Phang Nga', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Mai Khao', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'East', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Talang', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'Cape Yamu', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'Cherng Talay', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'User', '2017-03-16 04:31:33', '0000-00-00 00:00:00', NULL),
(2, 'Role', '2017-03-16 04:31:33', '0000-00-00 00:00:00', NULL),
(3, 'Page', '2017-03-16 04:32:02', '0000-00-00 00:00:00', NULL),
(4, 'Permission', '2017-03-16 04:32:02', '0000-00-00 00:00:00', NULL),
(5, 'Community', '2017-07-17 02:33:52', '2017-07-17 02:33:52', NULL),
(6, 'Features', '2017-07-17 02:34:42', '2017-07-17 02:34:42', NULL),
(7, 'Listing Type', '2017-07-17 03:09:38', '2017-07-17 03:09:38', NULL),
(8, 'Living', '2017-07-17 02:43:42', '2017-07-17 02:43:42', NULL),
(9, 'Property Type', '2017-07-17 02:47:16', '2017-07-17 02:47:16', NULL),
(10, 'Services', '2017-07-17 02:59:15', '2017-07-17 02:59:15', NULL),
(11, 'Suitability', '2017-07-17 02:59:34', '2017-07-17 02:59:34', NULL),
(12, 'Buy', '2017-07-17 03:01:36', '2017-07-17 03:01:36', NULL),
(13, 'Rent', '2017-07-17 03:01:47', '2017-07-17 03:01:47', NULL),
(14, 'Vehicle', '2017-07-17 03:16:50', '2017-07-17 03:16:50', NULL),
(15, 'Hire', '2017-07-17 03:17:04', '2017-07-17 03:17:04', NULL),
(16, 'Activity', '2017-07-17 05:13:03', '2017-07-17 05:13:03', NULL),
(17, 'Contact', '2017-07-17 06:31:40', '2017-07-17 06:31:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `admin_role_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `c` int(1) NOT NULL COMMENT '0 = not allow / 1 = allow',
  `r` int(1) NOT NULL COMMENT '0 = not allow / 1 = allow',
  `u` int(1) NOT NULL COMMENT '0 = not allow / 1 = allow',
  `d` int(1) NOT NULL COMMENT '0 = not allow / 1 = allow',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `admin_role_id`, `page_id`, `c`, `r`, `u`, `d`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2017-03-16 04:46:09', '0000-00-00 00:00:00', NULL),
(2, 1, 2, 1, 1, 1, 1, '2017-03-16 04:46:09', '0000-00-00 00:00:00', NULL),
(3, 1, 3, 1, 1, 1, 1, '2017-03-16 04:53:39', '0000-00-00 00:00:00', NULL),
(4, 1, 4, 1, 1, 1, 1, '2017-07-14 04:13:37', '2017-07-14 04:13:37', NULL),
(5, 1, 5, 1, 1, 1, 1, '2017-07-17 03:17:27', '2017-07-17 03:17:27', NULL),
(6, 1, 6, 1, 1, 1, 1, '2017-07-17 03:17:27', '2017-07-17 03:17:27', NULL),
(7, 1, 7, 1, 1, 1, 1, '2017-07-17 03:17:28', '2017-07-17 03:17:28', NULL),
(8, 1, 8, 1, 1, 1, 1, '2017-07-17 03:17:28', '2017-07-17 03:17:28', NULL),
(9, 1, 9, 1, 1, 1, 1, '2017-07-17 03:17:29', '2017-07-17 03:17:29', NULL),
(10, 1, 10, 1, 1, 1, 1, '2017-07-17 03:17:30', '2017-07-17 03:17:30', NULL),
(11, 1, 11, 1, 1, 1, 1, '2017-07-17 03:17:30', '2017-07-17 03:17:30', NULL),
(12, 1, 12, 1, 1, 1, 1, '2017-07-17 03:17:54', '2017-07-17 03:17:54', NULL),
(13, 1, 13, 1, 1, 1, 1, '2017-07-17 03:17:32', '2017-07-17 03:17:32', NULL),
(14, 1, 14, 1, 1, 1, 1, '2017-07-17 03:17:55', '2017-07-17 03:17:55', NULL),
(15, 1, 15, 1, 1, 1, 1, '2017-07-17 03:17:57', '2017-07-17 03:17:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE `property_type` (
  `property_id` int(11) NOT NULL,
  `property_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property_type`
--

INSERT INTO `property_type` (`property_id`, `property_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Land', 1, '2017-07-17 05:09:30', '2017-07-18 08:30:27', NULL),
(2, 'Villa/House', 3, '2017-07-17 07:58:37', '2017-07-18 08:30:59', NULL),
(3, 'Condo/apartment', 3, '2017-07-17 07:58:50', '2017-07-18 08:31:42', NULL),
(4, 'Commercial', 1, '2017-07-18 08:32:14', '2017-07-18 08:32:14', NULL),
(5, 'Hotel', 1, '2017-07-18 08:32:39', '2017-07-18 08:32:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rent`
--

CREATE TABLE `rent` (
  `rent_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `property_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `low_month` varchar(100) NOT NULL,
  `low_weekly` varchar(100) NOT NULL,
  `low_daily` varchar(100) NOT NULL,
  `high_month` varchar(100) NOT NULL,
  `high_weekly` varchar(100) NOT NULL,
  `high_daily` varchar(100) NOT NULL,
  `peak_month` varchar(100) NOT NULL,
  `peak_weekly` varchar(100) NOT NULL,
  `peak_daily` varchar(100) NOT NULL,
  `deposit` varchar(255) NOT NULL,
  `minimal_rent` int(5) NOT NULL,
  `period` int(5) NOT NULL COMMENT '1=nights, 2=weeks, 3=months, 4=year',
  `bedrooms` int(5) NOT NULL,
  `bathrooms` int(5) NOT NULL,
  `floors` int(5) NOT NULL,
  `indoor_area` varchar(255) NOT NULL,
  `land_size` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `web_link` varchar(255) NOT NULL,
  `private_comment` text NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `living_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `features_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `community_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `service_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `suitability_id` varchar(100) NOT NULL COMMENT '1,2,3,4,5,...',
  `img_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rent`
--

INSERT INTO `rent` (`rent_id`, `title`, `description`, `property_id`, `location_id`, `low_month`, `low_weekly`, `low_daily`, `high_month`, `high_weekly`, `high_daily`, `peak_month`, `peak_weekly`, `peak_daily`, `deposit`, `minimal_rent`, `period`, `bedrooms`, `bathrooms`, `floors`, `indoor_area`, `land_size`, `address`, `web_link`, `private_comment`, `latitude`, `longitude`, `living_id`, `features_id`, `community_id`, `service_id`, `suitability_id`, `img_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', '111vghgjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh<br />\r\nhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh<br />\r\nhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh<strong>nnnnnnnnnnnnnn</strong>', 2, 0, '1', '2', '2', '3', '4', '4', '5', '5', '6', 'kkk', 2, 0, 2, 4, 2, '2', '2', '1werytyl', '-', '', '7.986477662667627', '98.31047058105469', '3,5,9', '2,6', '1,4,5,6', '3,4', '1,2', '', '2017-07-20 04:46:00', '2017-07-20 07:16:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `service_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Gardening', 2, '2017-07-17 04:55:25', '2017-07-19 10:54:13', NULL),
(2, 'Housekeeping', 2, '2017-07-19 10:54:24', '2017-07-19 10:54:24', NULL),
(3, 'Chef', 2, '2017-07-19 10:54:35', '2017-07-19 10:54:35', NULL),
(4, 'Laundry', 2, '2017-07-19 10:54:44', '2017-07-19 10:54:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suitability`
--

CREATE TABLE `suitability` (
  `suitability_id` int(11) NOT NULL,
  `suitability_name` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=buy, 2=reat, 3=buy/reat',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suitability`
--

INSERT INTO `suitability` (`suitability_id`, `suitability_name`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'No Pets', 2, '2017-07-17 04:57:22', '2017-07-19 11:09:05', NULL),
(2, 'No Smoking', 2, '2017-07-19 10:55:23', '2017-07-19 11:09:18', NULL),
(3, 'Wheelchair Accessible', 2, '2017-07-19 10:55:33', '2017-07-19 11:09:26', NULL),
(4, 'Wheelchair Accessible', 2, '2017-07-19 10:55:42', '2017-07-19 11:09:30', '2017-07-19 11:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_car`
--

CREATE TABLE `vehicle_car` (
  `vehicle_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_detail`
--

CREATE TABLE `vehicle_detail` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_name` varchar(255) NOT NULL,
  `img_name` varchar(150) NOT NULL,
  `price` varchar(11) NOT NULL,
  `engine` int(11) NOT NULL,
  `seat_capac` int(11) NOT NULL,
  `auto_manual` tinyint(4) NOT NULL COMMENT '0=auto, 1=manual, 2=auto/manual',
  `type` tinyint(4) NOT NULL COMMENT '1=car, 2=bike, 3=yacht',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `admin_role_id` (`admin_role_id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`admin_role_id`);

--
-- Indexes for table `a_images`
--
ALTER TABLE `a_images`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `building_owner`
--
ALTER TABLE `building_owner`
  ADD PRIMARY KEY (`building_owner_id`);

--
-- Indexes for table `building_state`
--
ALTER TABLE `building_state`
  ADD PRIMARY KEY (`building_state_id`);

--
-- Indexes for table `buy`
--
ALTER TABLE `buy`
  ADD PRIMARY KEY (`buy_id`);

--
-- Indexes for table `community`
--
ALTER TABLE `community`
  ADD PRIMARY KEY (`community_id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`features_id`);

--
-- Indexes for table `land_detail`
--
ALTER TABLE `land_detail`
  ADD PRIMARY KEY (`land_detail_id`);

--
-- Indexes for table `land_title`
--
ALTER TABLE `land_title`
  ADD PRIMARY KEY (`land_title_id`);

--
-- Indexes for table `land_type`
--
ALTER TABLE `land_type`
  ADD PRIMARY KEY (`land_type_id`);

--
-- Indexes for table `listing_type`
--
ALTER TABLE `listing_type`
  ADD PRIMARY KEY (`listing_id`);

--
-- Indexes for table `living`
--
ALTER TABLE `living`
  ADD PRIMARY KEY (`living_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `property_type`
--
ALTER TABLE `property_type`
  ADD PRIMARY KEY (`property_id`);

--
-- Indexes for table `rent`
--
ALTER TABLE `rent`
  ADD PRIMARY KEY (`rent_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `suitability`
--
ALTER TABLE `suitability`
  ADD PRIMARY KEY (`suitability_id`);

--
-- Indexes for table `vehicle_car`
--
ALTER TABLE `vehicle_car`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- Indexes for table `vehicle_detail`
--
ALTER TABLE `vehicle_detail`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `admin_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `a_images`
--
ALTER TABLE `a_images`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `building_owner`
--
ALTER TABLE `building_owner`
  MODIFY `building_owner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `building_state`
--
ALTER TABLE `building_state`
  MODIFY `building_state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `buy`
--
ALTER TABLE `buy`
  MODIFY `buy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `community`
--
ALTER TABLE `community`
  MODIFY `community_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `features_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `land_detail`
--
ALTER TABLE `land_detail`
  MODIFY `land_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `land_title`
--
ALTER TABLE `land_title`
  MODIFY `land_title_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `land_type`
--
ALTER TABLE `land_type`
  MODIFY `land_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `listing_type`
--
ALTER TABLE `listing_type`
  MODIFY `listing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `living`
--
ALTER TABLE `living`
  MODIFY `living_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `property_type`
--
ALTER TABLE `property_type`
  MODIFY `property_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `rent`
--
ALTER TABLE `rent`
  MODIFY `rent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suitability`
--
ALTER TABLE `suitability`
  MODIFY `suitability_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vehicle_car`
--
ALTER TABLE `vehicle_car`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vehicle_detail`
--
ALTER TABLE `vehicle_detail`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
