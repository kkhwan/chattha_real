<?php
// file : app/config/constants.php

return [
    'APP_NAME' => 'BKKSOL',
    'PER_PAGE' => '25',
    'BO_NAME' => '_admin',
    'COPYRIGHT' => 'Bangkok Solutions Co.,Ltd.',
    'NO_PIC' => 'http://smk.bkksol.com/images/no-pic.png',
    'PATH_IMG_PRODUCT' => 'uploads/menu/'
];
