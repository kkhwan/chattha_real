$(function(){
    // ------------------------------ For Validation
    var rules = {
        rules: {
            header_name: {
                required: true
            },
            title_name: {
                required: true
            },
            detail: {
                required: true
            }
        },
        messages: { // custom messages for radio buttons and checkboxes

        }
    }
    var validationObj = $.extend (rules, themeRules);
    form.validate(validationObj);
});
