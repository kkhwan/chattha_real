$(function() {
    $('.btn-delete').click(function () {
        data_id = $(this).data('id');
        bootbox.confirm("Do you want to delete this list?", function (result) {
            if (result == true) {
                $('.form-delete[parent-data-id=' + data_id + ']').submit();
            }
        });
    });
});

$('.btn-delete').click(function(){
    data_id = $(this).data('id');
    bootbox.confirm("Do you want to delete this list?", function(result) {
        if(result == true){
            $('.form-delete[parent-data-id='+data_id+']').submit();
        }
    });
});

jQuery(document).ready(function(){
    ChangeActive(jQuery('.change-active'));
    ChangeReportStatus(jQuery('.change-report'));
    
    $('')
});

function ChangeActive(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = $(this).data('change_field');
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'pk_field' : pk_field,
            'v_pk_field' : v_pk_field,
            'change_field' : change_field,
            'value' : value,
            '_token' : _token
        };

        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);

        $.ajax({
            type : "POST",
            url : 'api-active',
            data : FormData,
            dataType : 'json',
            success: function(data){
                $("#zip_id").append(data);
                if(data==1)
                {
                    //console.log(data);
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','1');
                    value = 1 ;
                    //$().empty().append();
                }else if(data==0)
                {
                    //console.log(data);
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','0');
                    value = 0 ;
                }




            }
        });


    });
}

function ChangeReportStatus(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = $(this).data('change_field');
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'pk_field' : pk_field,
            'v_pk_field' : v_pk_field,
            'change_field' : change_field,
            'value' : value,
            '_token' : _token
        };

        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);

        $.ajax({
            type : "POST",
            url : '/api-report',
            data : FormData,
            success: function(data){
                if(data=='N')
                {
                    //console.log(data);

                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','N');
                    value = 'N' ;
                    //$().empty().append();
                }else if(data=='Y')
                {
                    //console.log(data);

                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','Y');
                    value = 'Y' ;
                }


            }
        });


    });


}

$(function(){
    $(".remove_img").click(function(){
        var rootDIV = $(this).parent().parent().parent();
        var r = confirm('Are you sure you want to delete');

        if(r == true){
            rootDIV.find(".img_path").removeAttr('src');
            rootDIV.find('.upload').show();
            rootDIV.find(".remove_img").hide();
            rootDIV.find(".img_hidden").val('');
            rootDIV.find(".file_upload").remove();
            rootDIV.find(".img_del").val('y');
        }else{
            return false;
        }
    });

    $(".upload").on("click",function(e){
        e.preventDefault();

        var rootDIV = $(this).parent().parent().parent();
        var input_name = $(this).data('file');
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":input_name,
            "style":"display:none",
            change: function(e){
                var files = this.files;
                showThumbnail(rootDIV, files);

                rootDIV.find(".upload").hide();
                rootDIV.find(".remove_img").show();
                rootDIV.find(".img_hidden").val('');
                rootDIV.find(".img_del").val('n');
            }
        });

        $(this).before(objFile);
        console.log('a');
        rootDIV.find(".file_upload").show().click().hide();
    });

    function showThumbnail(rootDIV, files){
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/

            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }

            var image = rootDIV.find('.img_path');
            var thumbnail =rootDIV.find('.img_thumbnail');
            // image.file = file;
            thumbnail.append(image);

            var reader = new FileReader();
            reader.onload = (function(aImg){
                return function(e){
                    // aImg.src = e.target.result;
                    aImg.attr('src', e.target.result);
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop
    } // end showThumbnail
});

// Chk File Manager
function chk_file_manager(e){
    if(e==1){
        $("#elfinder_group").hide('slow');
        $("#file_manager").val(0);
    }else{
        $("#elfinder_group").show('slow');
        $("#file_manager").val(1);
    }
}
// End Chk File Manager

// File Manager
function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;
    return (match && match.length > 1) ? match[1] : '' ;
}
// End File Manager

function  ChangeLocationDepartment2(value,value2) {
    var location_id = value;
    var value2 = value2;
    var _token = $('input[name=_token]').val();
    var FormData = {
        'location_id' : location_id,
        'value2' : value2,
        '_token' : _token
    };

    $.ajax({
        type : "POST",
        url : admin_url + '/api-department',
        data : FormData,
        success: function(data){
            $('#department_id').html(data);
        }
    });
}