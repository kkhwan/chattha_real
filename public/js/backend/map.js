//$(function(){
var map;
var markers = [];
function saveData(map, event)
{
    var zoomLevel = map.getZoom();
    var pos = (event.latLng).toString();
    pos = pos.replace('(','');
    pos = pos.replace(')','');
    pos = pos.replace(' ','');
    var splitLatLong = pos.split(",");
    $('#latitude').val(splitLatLong[0]);
    $('#longitude').val(splitLatLong[1]);
}
// Add a marker to the map and push to the array.
function addMarker(location) {
    clearMarkers();
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable:true
    });

    google.maps.event.addListener(marker, 'dragend', function (event) {
        saveData(map,event);
    });

    markers.push(marker);
}
// Sets the map on all markers in the array.
function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}
function clearMarkers() {
    setAllMap(null);
}
// Shows any markers currently in the array.
function showMarkers() {
    setAllMap(map);
}
// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

//});
