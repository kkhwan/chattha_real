$(function(){
    $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        orientation: "left",
        autoclose: true
    });
    // ------------------------------ For Validation
    var rules = {
        rules: {
            title: {
                maxlength: 40,
                required: true
            },
            description: {
                minlength: 10,
                required: true
            },
            low_month: {
                required: true
            },
            low_weekly: {
                required: true
            },
            low_daily: {
                required: true
            },
            address: {
                required: true
            },
            latitude: {
                required: true
            },
            longitude: {
                required: true
            }
        },
        messages: { // custom messages for radio buttons and checkboxes

        }
    }
    var validationObj = $.extend (rules, themeRules);
    form.validate(validationObj);
});
