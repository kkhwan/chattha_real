$(function(){
    // ------------------------------ For Validation
    var rules = {
        rules: {
            password : {
                minlength : 6
            },
            password_confirm : {
                minlength : 6,
                equalTo : "#password"
            }
        },
        messages: { // custom messages for radio buttons and checkboxes

        }
    }
    var validationObj = $.extend (rules, themeRules);
    form.validate(validationObj);
});
