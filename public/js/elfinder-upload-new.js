$(function(){
    $(".remove_img").click(function(){
        var rootDIV = $(this).parent().parent().parent();
        var r = confirm('Are you sure you want to delete');

        if(r == true){
            rootDIV.find(".img_path").removeAttr('src');
            rootDIV.find('.upload').show();
            rootDIV.find(".remove_img").hide();
            rootDIV.find(".img_hidden").val('');
            rootDIV.find(".file_upload").remove();
            rootDIV.find(".img_del").val('y');
        }else{
            return false;
        }
    });

    $(".upload").on("click",function(e){
        e.preventDefault();

        var rootDIV = $(this).parent().parent().parent();
        var input_name = $(this).data('file');
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":input_name,
            "style":"display:none",
            change: function(e){
                var files = this.files;
                showThumbnail(rootDIV, files);

                rootDIV.find(".upload").hide();
                rootDIV.find(".remove_img").show();
                rootDIV.find(".img_hidden").val('');
                rootDIV.find(".img_del").val('n');
            }
        });

        $(this).before(objFile);
        console.log('a');
        rootDIV.find(".file_upload").show().click().hide();
    });

    function showThumbnail(rootDIV, files){
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/

            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }

            var image = rootDIV.find('.img_path');
            var thumbnail =rootDIV.find('.img_thumbnail');
            // image.file = file;
            thumbnail.append(image);

            var reader = new FileReader();
            reader.onload = (function(aImg){
                return function(e){
                    // aImg.src = e.target.result;
                    aImg.attr('src', e.target.result);
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop
    } // end showThumbnail
});

// Chk File Manager
function chk_file_manager(e){
    if(e==1){
        $("#elfinder_group").hide('slow');
        $("#file_manager").val(0);
    }else{
        $("#elfinder_group").show('slow');
        $("#file_manager").val(1);
    }
}
// End Chk File Manager

// File Manager
function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;
    return (match && match.length > 1) ? match[1] : '' ;
}
// End File Manager
