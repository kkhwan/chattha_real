<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
if(empty($latitude) && empty($longitude)){
    $myLatLong = '7.975597747315679, 98.33209991455078';
}else{
    $myLatLong = $latitude.', '.$longitude;
}
$i = 2;
$l = 27;
$c = 41;
$d = 82;
?>
@extends('backend.layout.main-layout')
@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
    <style type="text/css" media="screen">
        .loc label{
            line-height: 18px;
            padding: 0 3px 0 0;
            white-space: nowrap;
            text-decoration: none;
            box-shadow: 0 1px 4px rgba(0,0,0,0.5);
            white-space: nowrap;
            cursor: pointer;
            background: #fff;
            background-color: rgba(255,255,255,0.75);
            color: #888;
            left: -50%;
            margin-top: -23px;
            position: relative;
            display: block;
            font-weight: bold;
        }
        .form-horizontal .radio > span{
            margin-top: -2px;
        }
    </style>
@endsection

@section('page-title')
    {{ $txt_manage}} Property {{$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="title" value="{{ $title }}" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Description</label>
                            <div class="col-md-12">
                                <textarea class="form-control ckeditor" id="editor1" name="description" rows="10">{{ $description }}</textarea>
                                <div id="ckeditor_error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-md-6">
                                <select class="form-control select2me" name="property_id" required="">
                                    <option value="">Property Type</option>
                                    @foreach($property_type as $value)
                                        <option value="{{ $value->property_id }}" @if($value->property_id == $property_id) selected @endif >{{ $value->property_name }}</option>
                                    @endforeach
                                </select><br><br>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <select class="form-control select2me" name="listing_id" required="">
                                    <option value="">Listing Type</option>
                                    @foreach($listing_type as $value)
                                        <option value="{{ $value->listing_id }}" @if($value->listing_id == $listing_id) selected @endif >{{ $value->listing_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div @if($method == "PUT") style="height: 500px; background-image: url(../../../images/phuketmap-01.png); border: 1px solid #b6b6b6;" @elseif($method == "POST") style="height: 500px; background-image: url(../../images/phuketmap-01.png); border: 1px solid #b6b6b6;" @endif >
                                    <div class="newmapLocations">
                                        <div class="loc" style="height: 0px; position: absolute; left: 210px; top: 30px">
                                            <label><div><input title="Phang Nga" type="radio" name="location_id" value="1" @if($location_id == 1) checked @endif ><b>Phang Nga</b> </div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 340px; top: 200px">
                                            <label><div><input title="Ao Po" type="radio" name="location_id" value="3" @if($location_id == 3) checked @endif >Ao Po</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 240px; top: 210px">
                                            <label><div><input title="Thalang" type="radio" name="location_id" value="4" @if($location_id == 4) checked @endif >Thalang</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 230px; top: 240px">
                                            <label><div><input title="Cherng Talay" type="radio" name="location_id" value="6" @if($location_id == 6) checked @endif >Cherng Talay</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 345px; top: 265px">
                                            <label><div><input title="Cape Yamu" type="radio" name="location_id" value="7" @if($location_id == 7) checked @endif >Cape Yamu</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 250px; top: 283px">
                                            <label><div><input title="Koh Kaew" type="radio" name="location_id" value="8" @if($location_id == 8) checked @endif >Koh Kaew</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 210px; top: 310px">
                                            <label><div><input title="Kathu" type="radio" name="location_id" value="9" @if($location_id == 9) checked @endif >Kathu</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 300px; top: 370px">
                                            <label><div><input title="Phuket Town" type="radio" name="location_id" value="10" @if($location_id == 10) checked @endif >Phuket Town</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 350px; top: 400px">
                                            <label><div><input title="Koh Siray Beach" type="radio" name="location_id" value="11" @if($location_id == 11) checked @endif >Koh Siray</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 325px; top: 455px">
                                            <label><div><input title="Panwa Cape" type="radio" name="location_id" value="12" @if($location_id == 12) checked @endif >Panwa Cape</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 220px; top: 455px">
                                            <label><div><input title="Chalong Bay" type="radio" name="location_id" value="13" @if($location_id == 13) checked @endif >Chalong</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 210px; top: 485px">
                                            <label><div><input title="Rawai Beach" type="radio" name="location_id" value="14" @if($location_id == 14) checked @endif >Rawai</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 490px">
                                            <label><div><input title="Nai Harn Beach" type="radio" name="location_id" value="15" @if($location_id == 15) checked @endif >Nai Harn</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 135px; top: 455px">
                                            <label><div><input title="Kata Beach" type="radio" name="location_id" value="16" @if($location_id == 16) checked @endif >Kata</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 410px">
                                            <label><div><input title="Karon Beach" type="radio" name="location_id" value="17" @if($location_id == 17) checked @endif >Karon</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 350px">
                                            <label><div><input title="Patong Beach" type="radio" name="location_id" value="18" @if($location_id == 18) checked @endif >Patong</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 95px; top: 320px">
                                            <label><div><input title="Kamala Beach" type="radio" name="location_id" value="19" @if($location_id == 19) checked @endif >Kamala</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 115px; top: 288px">
                                            <label><div><input title="Surin Beach" type="radio" name="location_id" value="20" @if($location_id == 20) checked @endif >Surin</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 110px; top: 260px">
                                            <label><div><input title="Bangtao Beach" type="radio" name="location_id" value="21" @if($location_id == 21) checked @endif >Bangtao</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 233px">
                                            <label><div><input title="Layan Beach" type="radio" name="location_id" value="22" @if($location_id == 23) checked @endif >Layan</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 100px; top: 205px">
                                            <label><div><input title="Nai Thon Beach" type="radio" name="location_id" value="23" @if($location_id == 23) checked @endif >Nai Thon</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 120px; top: 160px">
                                            <label><div><input title="Nai Yang Beach" type="radio" name="location_id" value="24" @if($location_id == 24) checked @endif >Nai Yang</div></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="price" value="{{ $price }}" id="price" placeholder="Price [THB]">
                            </div>
                        </div> <br>
                        <div class="form-group">
                            <div class="col-sm-6 col-md-6">
                                <input type="text" class="form-control" name="indoor_area" value="{{ $indoor_area }}" id="indoor_area" placeholder="Indoor Area Size, m²">
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <input type="text" class="form-control" name="land_size" value="{{ $land_size }}" id="land_size" placeholder="Land Size, m²">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea class=" form-control ckeditor" name="address" rows="10" placeholder="Address" >{{ $address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="web_link" value="{{ $web_link }}" id="web_link" placeholder="Web Link">
                            </div>
                        </div> <br>

                        <div class="form-group" style="background:#f0f0f0; margin: 0 0 20px; padding: 9px 18px 18px;">
                            <label class="col-md-12"><b>Your Remarks </b><br>
                                The information in this area will be visible only to you. Nobody else will see it. <br><br>
                                You can keep your property related information you find useful and search for it later.<br><br>
                            </label>
                            <div class="col-md-12">
                                <textarea class=" form-control ckeditor" name="private_comment" rows="6" placeholder="Private Comment" >{{ $private_comment }}</textarea>
                            </div>
                        </div> <br>

                        <!-- ******************************* Map ***************************** -->
                        <div class="form-group">
                            <label class="col-md-12">Click to place your property on the map</label>
                            <div class="col-sm-12 col-md-12">
                                <div id="dvMap" style="width: 100%; height: 700px"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-md-6">
                                <input type="text" class="form-control" name="latitude" value="{{ $latitude }}" id="latitude" placeholder="Latitude">
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <input type="text" class="form-control" name="longitude" value="{{ $longitude }}" id="longitude" placeholder="Longitude">
                            </div>
                        </div><br>
                        <!-- ******************************* Map ***************************** -->

                        <div class="form-group">
                            <label class="col-md-2 control-label">Promote</label>
                            <div class="col-md-10">
                                <div class="md-radio-inline" data-error-container="#inline-radio">
                                    <div class="md-radio">
                                        <input type="radio" id="radio53" name="promote" value="2" class="md-radiobtn" @if($promote == 2) checked @endif>
                                        <label for="radio53">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Hot</label>
                                    </div>
                                    <div class="md-radio">
                                        <input type="radio" id="radio54" name="promote" value="3" class="md-radiobtn" @if($promote == 3) checked @endif>
                                        <label for="radio54">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Price Reduced</label>
                                        &nbsp;&nbsp; <input type="text" name="discount_price" value="{{ $discount_price }}" placeholder="Discount (%)">
                                    </div>
                                    <!-- <div class="md-radio">
                                        <input type="radio" id="radio55" name="promote" value="4" class="md-radiobtn" @if($promote == 4) checked @endif>
                                        <label for="radio55">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Investment</label>
                                    </div> -->
                                    <div class="md-radio">
                                        <input type="radio" id="radio56" name="promote" value="5" class="md-radiobtn" @if($promote == 5) checked @endif>
                                        <label for="radio56">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Bargain Price</label>
                                    </div>
                                </div>
                                <div id="inline-radio"></div>
                            </div>
                        </div>

                        <!-- ******************************* Land ***************************** -->
                        <div class="form-group">
                            <div class="col-sm-12 col-md-12">
                                <label>Land</label>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div>
                                    <select class="form-control select2me" name="land_title_id" required="">
                                        <option value="">Land Title</option>
                                        @foreach($land_title as $value)
                                            <option value="{{ $value->land_title_id }}" @if($value->land_title_id == $land_title_id) selected @endif >{{ $value->title_name }}</option>
                                        @endforeach
                                    </select>
                                </div><br>
                                <div>
                                    <select class="form-control select2me" name="land_type_id">
                                        <option value="">Land Type</option>
                                        @foreach($land_type as $value)
                                            <option value="{{ $value->land_type_id }}" @if($value->land_type_id == $land_type_id) selected @endif >{{ $value->type_name }}</option>
                                        @endforeach
                                    </select>
                                </div><br>
                                <!-- <input type="text" class="form-control" name="frontage" value="{{ $frontage }}" id="frontage" placeholder="Frontage (m)"><br> -->
                                <!-- <input type="text" class="form-control" name="width" value="{{ $width }}" id="width" placeholder="Width (m)"><br> -->
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox1" class="md-check selectedAll1" name="selectedAll1">
                                        <label for="checkbox1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($land_detail as $value)
                                        <?php $a_detail = explode(",", $land_detail_id); $check=""; ?>
                                        @foreach($a_detail as $detail)
                                            @if($detail == $value->land_detail_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$i}}" class="md-check checkbox1" name="land_detail_id[]" value="{{ $value->land_detail_id }}" {{ $check }} >
                                            <label for="checkbox{{$i}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->detail_name }} </label>
                                        </div>
                                        <?php $i++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div>
                            </div>
                        </div><br>
                        <!-- ******************************* Land ***************************** -->

                        <!-- ******************************* Building ***************************** -->
                        <div class="form-group">
                            <div class="col-sm-12 col-md-12">
                                <label>Building</label>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div>
                                    <select class="form-control select2me" name="building_state_id" required="">
                                        <option value="">Building State</option>
                                        @foreach($building_state as $value)
                                            <option value="{{ $value->building_state_id }}" @if($value->building_state_id == $building_state_id) selected @endif >{{ $value->state_name }}</option>
                                        @endforeach
                                    </select>
                                </div><br>
                                <div>
                                    <select class="form-control select2me" name="building_owner_id">
                                        <option value="">Building Owner</option>
                                        @foreach($building_owner as $value)
                                            <option value="{{ $value->building_owner_id }}" @if($value->building_owner_id == $building_owner_id) selected @endif >{{ $value->owner_name }}</option>
                                        @endforeach
                                    </select>
                                </div><br>
                                <div>
                                    <select class="form-control select2me" name="year" required="">
                                        <option value="">Completed year</option>
                                        @for($j=2020; $j>=2000; $j--)
                                            <option value="{{ $j }}" @if($j == $year) selected @endif >{{ $j }}</option>
                                        @endfor
                                    </select>
                                </div><br>
                                <div>
                                    <select class="form-control select2me" name="bedrooms">
                                        <option value="">Bedrooms</option>
                                        @for($y=1; $y<=8; $y++)
                                            <option value="{{ $y }}" @if($y == $bedrooms) selected @endif >{{ $y }}</option>
                                        @endfor
                                        <option value="9" @if(9 == $bedrooms) selected @endif >9 or more </option>
                                    </select>
                                </div><br>
                                <div>
                                    <select class="form-control select2me" name="bathrooms">
                                        <option value="">Bathrooms</option>
                                        @for($y=1; $y<=8; $y++)
                                            <option value="{{ $y }}" @if($y == $bathrooms) selected @endif >{{ $y }}</option>
                                        @endfor
                                        <option value="9" @if(9 == $bathrooms) selected @endif >9 or more </option>
                                    </select>
                                </div><br>
                                <input type="text" class="form-control" name="floors" value="{{ $floors }}" id="floors" placeholder="Floors"><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox11" class="md-check" name="rental_program" value="1" @if($rental_program == 1) checked @endif>
                                        <label for="checkbox11">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Rental program </label>
                                    </div>
                                </div><br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox26" class="md-check selectedAll2" name="selectedAll2">
                                        <label for="checkbox26">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($living as $value)
                                        <?php $a_living = explode(",", $living_id); $check=""; ?>
                                        @if(!empty($a_living))
                                            @foreach($a_living as $living)
                                                @if($living == $value->living_id)
                                                    <?php $check="checked"; ?>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$l}}" class="md-check checkbox2" name="living_id[]" value="{{ $value->living_id }}" {{ $check }} >
                                            <label for="checkbox{{$l}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->living_name }} </label>
                                        </div>
                                        <?php $l++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div><br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox40" class="md-check selectedAll3" name="selectedAll3">
                                        <label for="checkbox40">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($community as $value)
                                        <?php $a_community = explode(",", $community_id); $check=""; ?>
                                        @foreach($a_community as $value1)
                                            @if($value1 == $value->community_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$c}}" class="md-check checkbox3" name="community_id[]" value="{{ $value->community_id }}" {{ $check }} >
                                            <label for="checkbox{{$c}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->community_name }} </label>
                                        </div>
                                        <?php $c++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>

                                <br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox81" class="md-check selectedAll6" name="selectedAll6">
                                        <label for="checkbox81">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($views as $value)
                                        <?php $a_view = explode(",", $view_id); $check=""; ?>
                                        @foreach($a_view as $view_n)
                                            @if($view_n == $value->view_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$d}}" class="md-check checkbox6" name="view_id[]" value="{{ $value->view_id }}" {{ $check }} >
                                            <label for="checkbox{{$d}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->view_name }} </label>
                                        </div>
                                        <?php $d++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>
                            </div>
                        </div><br>
                        <!-- ******************************* Building ***************************** -->

                        <!-- ******************************* Apartment ***************************** -->
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Building Storey</label>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <input type="text" class="form-control" name="apm_storeys" value="{{ $apm_storeys }}" id="apm_storeys" placeholder="Total Building storey">
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <input type="text" class="form-control" name="apm_floor" value="{{ $apm_floor }}" id="apm_floor" placeholder="Floor">
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox60" class="md-check" name="apm_access" value="1" @if($apm_access == 1) checked @endif>
                                        <label for="checkbox60">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Elevator access </label>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <!-- ******************************* Apartment ***************************** -->

                        <!-- ******************************* image ***************************** -->
                        <?php
                            $field = 'img_name';
                            $label = 'Image';
                            $upload_path = 'property';
                        ?>
                        <div id="bg-img">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="img_thumbnail">
                                        @if($$field != '')
                                            <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                        @else
                                            <img src="" class="img_path" style="max-width:400px;">
                                        @endif

                                        <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">{{ $label }}</label>
                                <div class="col-md-4">
                                    @if($$field != '')
                                        <div class="btn btn-danger remove_img" >Remove</div>
                                        <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                    @elseif ($$field == '')
                                        <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                        <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                    @endif

                                    <input type="hidden" name="img_del" class="img_del" value="n">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-4">
                                    <span class="label label-danger">NOTE! &nbsp; </span>
                                    Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+ and Opera11.1+. In older browsers the filename is shown instead.
                                </div>
                            </div>
                        </div>
                        <!-- ******************************* image ***************************** -->
                        
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/validation.js') }}
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('API_KEY') }}"></script>
    {{ Html::script('js/backend/map.js') }}
    {{ Html::script('js/backend/buy.js') }}
    <script>
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ $myLatLong }});
            var myOptions = {
                zoom: 11,
                center: myLatlng,
            };
            map = new google.maps.Map(document.getElementById('dvMap'), myOptions);
            // This event listener will call addMarker() when the map is clicked.
            google.maps.event.addListener(map, 'click', function(event) {
                addMarker(event.latLng);
                saveData(map, event);
            });
            // Adds a marker at the center of the map.
            addMarker(myLatlng);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script>
        CKEDITOR.replace('editor1', {            
            toolbar:       //กลุ่มเครื่องมือ ตัดออก เพิ่มเข้าได้  
            [ ['Bold', 'Italic'], ['NumberedList', 'BulletedList'], ['Undo', 'Redo'],
            ['Cut', 'Copy', 'Paste'] ]
        });  
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll1').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll2').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox2').attr('checked', 'checked');
                    $('.checkbox2').parent().addClass('checked');
                }else{
                    $('.checkbox2').removeAttr('checked');
                    $('.checkbox2').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll3').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox3').attr('checked', 'checked');
                    $('.checkbox3').parent().addClass('checked');
                }else{
                    $('.checkbox3').removeAttr('checked');
                    $('.checkbox3').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll6').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox6').attr('checked', 'checked');
                    $('.checkbox6').parent().addClass('checked');
                }else{
                    $('.checkbox6').removeAttr('checked');
                    $('.checkbox6').parent().removeClass('checked');
                }
            });
        });
    </script>

@endsection
