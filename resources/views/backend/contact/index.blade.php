<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <input type="hidden" name="show" value="Y">
                    <div class="form-group">
                        <label class="control-label col-md-2">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by name </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
                <div class="text-right">
                    <div>
                        {{--<a href="{{URL::to('_admin/student_up')}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Update Student</button></a>--}}
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">{!! $obj_fn->sorting('ID','$primaryKey',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-2">{!! $obj_fn->sorting('Name','name',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-3">Detail</th>
                            <th class="text-center col-sm-3">{!! $obj_fn->sorting('Messag','msg',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <!-- <th class="text-center col-md-1"></th> -->
                            <th class="col-md-2 text-center font-blue-sharp">Reply Time</th>
                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                <th class="col-md-1 text-center font-blue-sharp">Reply</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{ $field->$primaryKey}}</td>
                                        <td>{{ $field->name }}</td>
                                        <td>
                                            <b>E-mail : </b>{{ $field->email }} <br>
                                            <b>Mobile : </b>{{ $field->mobile }} <br>
                                            <b>Nationality : </b>{{ $field->nationality }}
                                        </td>
                                        <td>{{ $field->msg }}</td>
                                        @if($field->status !='0')
                                            <td class="text-center">{{ $obj_fn->format_date_en ($field->updated_at,5) }}</td>
                                        @else
                                            <td><?php echo' '?></td>
                                        @endif
                                        @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                            @if($field->status == 0)
                                                <td class="text-center"><a href="{{ URL::to($path.'/'.$field->$primaryKey.'/edit?1'.$str_param) }}" class="btn btn-xs btn-circle blue"><i class="fa fa-share"></i></a></td>
                                            @else
                                                <td></td>
                                                <!-- <td class="text-center"><a href="{{URL::to($path.'/'.$field->$primaryKey.$str_param)}}" class="btn btn-xs btn-circle green" ><i class="fa fa-file-text-o"></i></a></td> -->

                                            @endif
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="7">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
@endsection
@section('more-script')
@endsection