<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$user_id =  Input::get('user_id');

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')
{{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
<a href= "{{ url()->to('_admin/broadcast_new') }}">&nbsp;{{ $page_title }}</a>
 > {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">From Name</label>
                            <div class="col-md-4">
                                 <input type='text' name="from_name" id="name" class="form-control" value="Admin TE Family and Alumni">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">From Email</label>
                            <div class="col-md-4">
                                <input type='text' name="from_email" id="email" class="form-control" value="Khwan.n08@gmail.com">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">To Name</label>
                            <div class="col-md-4">
                                <input type='text' name="name" id="to_name" class="form-control" readonly value="{{$name}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">To Email</label>
                            <div class="col-md-4">
                                <input type='text' name="email" id="to_email" class="form-control" readonly value={{$email}}>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile</label>
                            <div class="col-md-4">
                                <input type='text' name="mobile" id="mobile" class="form-control" readonly value='{{$mobile}}'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Message from user</label>
                            <div class="col-md-4">
                               <textarea name="msg" id="detail" class="form-control" readonly rows="12" cols="15">{{$msg}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Message</label>
                            <div class="col-md-4">
                               <textarea name="from_msg" id="detail" class="form-control" rows="12" cols="15"></textarea>
                            </div>
                        </div>
                    </div>
                <hr>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                        <input type='hidden' name="is_available" id="is_available" class="form-control" value="1">
                             <input type='hidden' name="type" id="type" class="form-control" value="1">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/validation.js') }}
    <!-- {{ Html::script('js/backend/boradcastnew.js') }} -->
@endsection