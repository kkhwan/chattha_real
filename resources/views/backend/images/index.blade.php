<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
if(!empty(Input::get('activity_id'))){
    $activity_id = "?activity_id=".Input::get('activity_id');
}else{
    $activity_id = "";
}
?>
@extends('backend.layout.main-layout')

@section('page-style')
{{ Html::style('css/change-status.css') }}
{{ Html::style('assets/global/plugins/select2/select2.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    <a href= "{{ url()->to('_admin/activity') }}">&nbsp;{{ $activity_name }}</a>
 > {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">Images</th>
                            <th class="text-center col-md-1">{!! $obj_fn->sorting('Available','is_available',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-2">Sorting</th>

                            <th class="text-center col-sm-1 col-md-1">
                                <a href="{{ url()->to($path.'/create'.$activity_id) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{ $field->$primaryKey }}</td>
                                        <td class="text-center">
                                            @if ($field->img_name != '' && file_exists('uploads/activity/100/' . $field->img_name))
                                                <img src="{{ URL::asset('uploads/activity/100/' . $field->img_name) }}">
                                            @endif
                                        </td>

                                        @if($field->is_available == 0 )
                                            <td><div class="text-center"><a id="is_available-{{$field->$primaryKey}}" class=" change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="is_available"  data-value="{{$field->is_available}}"></a></div></td>
                                        @else
                                            <td><div class="text-center"><a id="is_available-{{$field->$primaryKey}}" class=" change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="is_available"  data-value="{{$field->is_available}}"></a></div></td>
                                        @endif

                                        <td> {!! Form::open(array('url'=>'_admin/sequence/image' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}
                                            <input type="hidden" name="activity_id" value="{{$field->activity_id}}">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="new_sorting" value="{{$field->sorting}}">
                                                <div class="input-group-btn">
                                                    <input type="hidden" name="{{$primaryKey}}" value="{{$field->$primaryKey}}">
                                                    <input type="hidden" name="old_sorting" value="{{$field->sorting}}">;
                                                    <button class="btn" type="submit"><i class="glyphicon glyphicon-resize-horizontal"></i></button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </td>

                                        <td class="text-center">
                                            <a href="{{ url()->to($path.'/'.$field->$primaryKey.'/edit?1'.$str_param) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                            <form action="{{ url()->to($path.'/'.$field->$primaryKey) }}" class="form-delete" parent-data-id="{{ $field->$primaryKey }}" method="POST" >
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="button" class="btn btn-xs btn-circle red btn-delete" data-id="{{ $field->$primaryKey }}"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="7">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
{{ Html::style('js/change-status.js') }}
{{ Html::script('assets/global/plugins/select2/select2.min.js') }}
@endsection
@section('more-script')
@endsection