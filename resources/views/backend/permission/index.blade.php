<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('css/change-status.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }} >> {{ $admin_role->role_name }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <label class="control-label col-md-1">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by Page Name</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                            <input class="form-control" type="hidden" name="admin_role_id" value="{{ Input::get('admin_role_id') }}">
                        </div>
                    </div>
                </form>
                <hr>
            </div>

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th>{!! $obj_fn->sorting('Page Name','page_name',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-md-1 font-blue">Create</th>
                            <th class="text-center col-md-1 font-blue">Read</th>
                            <th class="text-center col-md-1 font-blue">Update</th>
                            <th class="text-center col-md-1 font-blue">Delete</th>
                            <th class="text-center col-md-1 font-blue"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{$field->$primaryKey}}</td>
                                        <td>{{ $field->page_name }}</td>
                                        @if($field->c == 0)
                                            <td><div class="text-center"><a id="c-{{$field->$primaryKey}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="c"  data-value="{{$field->c}}"></a></div></td>
                                        @else
                                            <td><div class="text-center"><a id="c-{{$field->$primaryKey}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="c"  data-value="{{$field->c}}"></a></div></td>
                                        @endif

                                        @if($field->r == 0)
                                            <td><div class="text-center"><a id="r-{{$field->$primaryKey}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="r"  data-value="{{$field->r}}"></a></div></td>
                                        @else
                                            <td><div class="text-center"><a id="r-{{$field->$primaryKey}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="r"  data-value="{{$field->r}}"></a></div></td>
                                        @endif

                                        @if($field->u == 0)
                                            <td><div class="text-center"><a id="u-{{$field->$primaryKey}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="u"  data-value="{{$field->u}}"></a></div></td>
                                        @else
                                            <td><div class="text-center"><a id="u-{{$field->$primaryKey}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="u"  data-value="{{$field->u}}"></a></div></td>
                                        @endif

                                        @if($field->d == 0)
                                            <td><div class="text-center"><a id="d-{{$field->$primaryKey}}" class="change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="d"  data-value="{{$field->d}}"></a></div></td>
                                        @else
                                            <td><div class="text-center"><a id="d-{{$field->$primaryKey}}" class="change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="d"  data-value="{{$field->d}}"></a></div></td>
                                        @endif

                                        <td class="text-center">
                                            <a class="change-crud-active btn btn-xs btn-circle green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}"
                                                data-change_field_c="c" data-change_field_r="r" data-change_field_u="u" data-change_field_d="d" data-value="0"> Select All</a>
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="7">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('js/change-status.js') }}
@endsection
@section('more-script')
@endsection
