<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
if(empty($latitude) && empty($longitude)){
    $myLatLong = '7.975597747315679, 98.33209991455078';
}else{
    $myLatLong = $latitude.', '.$longitude;
}
$i = 2;
$j = 17;
$m = 47;
$z = 165;
$d = 82;
$x = 115;
?>
@extends('backend.layout.main-layout')
@section('page-style')
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
@endsection
@section('more-style')
    <style type="text/css" media="screen">
        .loc label{
            line-height: 18px;
            padding: 0 3px 0 0;
            white-space: nowrap;
            text-decoration: none;
            box-shadow: 0 1px 4px rgba(0,0,0,0.5);
            white-space: nowrap;
            cursor: pointer;
            background: #fff;
            background-color: rgba(255,255,255,0.75);
            color: #888;
            left: -50%;
            margin-top: -23px;
            position: relative;
            display: block;
            font-weight: bold;
        }
        .form-horizontal .radio > span{
            margin-top: -2px;
        }
    </style>
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="title" value="{{ $title }}" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Description</label>
                            <div class="col-md-12">
                                <textarea class=" form-control ckeditor" id="editor1" name="description" rows="10">{{ $description }}</textarea>
                                <div id="ckeditor_error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <select class="form-control select2me" name="property_id" required="">
                                    <option value="">Property Type</option>
                                    @foreach($property_type as $value)
                                        <option value="{{ $value->property_id }}" @if($value->property_id == $property_id) selected @endif >{{ $value->property_name }}</option>
                                    @endforeach
                                </select><br>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div @if($method == "PUT") style="height: 500px; background-image: url(../../../images/phuketmap-01.png); border: 1px solid #b6b6b6; " @elseif($method == "POST") style="height: 500px; background-image: url(../../images/phuketmap-01.png); border: 1px solid #b6b6b6;" @endif >
                                    <div class="newmapLocations">
                                        <div class="loc" style="height: 0px; position: absolute; left: 210px; top: 30px">
                                            <label><div><input title="Phang Nga" type="radio" name="location_id" value="1" @if($location_id == 1) checked @endif ><b>Phang Nga</b> </div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 340px; top: 200px">
                                            <label><div><input title="Ao Po" type="radio" name="location_id" value="3" @if($location_id == 3) checked @endif >Ao Po</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 240px; top: 210px">
                                            <label><div><input title="Thalang" type="radio" name="location_id" value="4" @if($location_id == 4) checked @endif >Thalang</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 230px; top: 240px">
                                            <label><div><input title="Cherng Talay" type="radio" name="location_id" value="6" @if($location_id == 6) checked @endif >Cherng Talay</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 345px; top: 265px">
                                            <label><div><input title="Cape Yamu" type="radio" name="location_id" value="7" @if($location_id == 7) checked @endif >Cape Yamu</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 250px; top: 283px">
                                            <label><div><input title="Koh Kaew" type="radio" name="location_id" value="8" @if($location_id == 8) checked @endif >Koh Kaew</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 210px; top: 310px">
                                            <label><div><input title="Kathu" type="radio" name="location_id" value="9" @if($location_id == 9) checked @endif >Kathu</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 300px; top: 370px">
                                            <label><div><input title="Phuket Town" type="radio" name="location_id" value="10" @if($location_id == 10) checked @endif >Phuket Town</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 350px; top: 400px">
                                            <label><div><input title="Koh Siray Beach" type="radio" name="location_id" value="11" @if($location_id == 11) checked @endif >Koh Siray</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 325px; top: 455px">
                                            <label><div><input title="Panwa Cape" type="radio" name="location_id" value="12" @if($location_id == 12) checked @endif >Panwa Cape</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 220px; top: 455px">
                                            <label><div><input title="Chalong Bay" type="radio" name="location_id" value="13" @if($location_id == 13) checked @endif >Chalong</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 210px; top: 485px">
                                            <label><div><input title="Rawai Beach" type="radio" name="location_id" value="14" @if($location_id == 14) checked @endif >Rawai</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 490px">
                                            <label><div><input title="Nai Harn Beach" type="radio" name="location_id" value="15" @if($location_id == 15) checked @endif >Nai Harn</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 135px; top: 455px">
                                            <label><div><input title="Kata Beach" type="radio" name="location_id" value="16" @if($location_id == 16) checked @endif >Kata</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 410px">
                                            <label><div><input title="Karon Beach" type="radio" name="location_id" value="17" @if($location_id == 17) checked @endif >Karon</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 350px">
                                            <label><div><input title="Patong Beach" type="radio" name="location_id" value="18" @if($location_id == 18) checked @endif >Patong</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 95px; top: 320px">
                                            <label><div><input title="Kamala Beach" type="radio" name="location_id" value="19" @if($location_id == 19) checked @endif >Kamala</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 115px; top: 288px">
                                            <label><div><input title="Surin Beach" type="radio" name="location_id" value="20" @if($location_id == 20) checked @endif >Surin</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 110px; top: 260px">
                                            <label><div><input title="Bangtao Beach" type="radio" name="location_id" value="21" @if($location_id == 21) checked @endif >Bangtao</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 130px; top: 233px">
                                            <label><div><input title="Layan Beach" type="radio" name="location_id" value="22" @if($location_id == 23) checked @endif >Layan</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 100px; top: 205px">
                                            <label><div><input title="Nai Thon Beach" type="radio" name="location_id" value="23" @if($location_id == 23) checked @endif >Nai Thon</div></label>
                                        </div>
                                        <div class="loc" style="height: 0px; position: absolute; left: 120px; top: 160px">
                                            <label><div><input title="Nai Yang Beach" type="radio" name="location_id" value="24" @if($location_id == 24) checked @endif >Nai Yang</div></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <br>
                        <!-- ******************************* Saeson ***************************** -->
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-2">Low Season</label>
                            <div class="col-sm-6 col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="low_from" value="{{ $low_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="low_to" value="{{ $low_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="low_month" value="{{ $low_month }}" id="low_month" placeholder="Monthly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="low_weekly" value="{{ $low_weekly }}" id="low_weekly" placeholder="Weekly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="low_daily" value="{{ $low_daily }}" id="low_daily" placeholder="Daily Rent">
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-2">High Season</label>
                            <div class="col-sm-6 col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="high_from" value="{{ $high_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="high_to" value="{{ $high_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="high_month" value="{{ $high_month }}" id="high_month" placeholder="Monthly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="high_weekly" value="{{ $high_weekly }}" id="high_weekly" placeholder="Weekly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="high_daily" value="{{ $high_daily }}" id="high_daily" placeholder="Daily Rent">
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-2">Peak Season</label>
                            <div class="col-sm-6 col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="peak_from" value="{{ $peak_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="peak_to" value="{{ $peak_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="peak_month" value="{{ $peak_month }}" id="peak_month" placeholder="Monthly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="peak_weekly" value="{{ $peak_weekly }}" id="peak_weekly" placeholder="Weekly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="peak_daily" value="{{ $peak_daily }}" id="peak_daily" placeholder="Daily Rent">
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-2">Low Season</label>
                            <div class="col-sm-6 col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="low1_from" value="{{ $low1_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="low1_to" value="{{ $low1_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="low1_month" value="{{ $low1_month }}" id="low1_month" placeholder="Monthly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="low1_weekly" value="{{ $low1_weekly }}" id="low1_weekly" placeholder="Weekly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="low1_daily" value="{{ $low1_daily }}" id="low1_daily" placeholder="Daily Rent">
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-2">High Season</label>
                            <div class="col-sm-6 col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="high1_from" value="{{ $high1_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="high1_to" value="{{ $high1_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="high1_month" value="{{ $high1_month }}" id="high1_month" placeholder="Monthly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="high1_weekly" value="{{ $high1_weekly }}" id="high1_weekly" placeholder="Weekly Rent">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <input type="number" class="form-control" name="high1_daily" value="{{ $high1_daily }}" id="high1_daily" placeholder="Daily Rent">
                            </div>
                        </div><br>
                        <!-- ******************************* Saeson ***************************** -->

                        <!-- ******************************* Saeson ***************************** -->
                        <div class="form-group">
                            <label class="col-md-12">Please fill the prices for all seasons, as this will help us to find clients for your property.</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-4">
                                <input type="text" class="form-control" name="deposit" value="{{ $deposit }}" id="deposit" placeholder="Deposit">
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <select class="form-control select2me" name="minimal_rent" required="">4
                                    <option value="">Minimal Rent</option>
                                    @for($y=1; $y<=6; $y++)
                                        <option value="{{ $y }}" @if($y == $minimal_rent) selected @endif >{{ $y }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <select class="form-control select2me" name="period" required="">
                                    <option value="">Period</option>
                                    <option value="1" @if(1 == $period) selected @endif >Nights</option>
                                    <option value="2" @if(2 == $period) selected @endif >Weeks</option>
                                    <option value="3" @if(3 == $period) selected @endif >Months</option>
                                    <option value="4" @if(4 == $period) selected @endif >Year</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-4">
                                <select class="form-control select2me" name="bedrooms" required="">
                                    <option value="">Bedrooms</option>
                                    @for($y=1; $y<=8; $y++)
                                        <option value="{{ $y }}" @if($y == $bedrooms) selected @endif >{{ $y }}</option>
                                    @endfor
                                    <option value="9" @if(9 == $bedrooms) selected @endif >9 or more </option>
                                </select>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <select class="form-control select2me" name="bathrooms" required="">
                                    <option value="">Bathrooms</option>
                                    @for($y=1; $y<=8; $y++)
                                        <option value="{{ $y }}" @if($y == $bathrooms) selected @endif >{{ $y }}</option>
                                    @endfor
                                    <option value="9" @if(9 == $bathrooms) selected @endif >9 or more </option>
                                </select>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <select class="form-control select2me" name="floors">
                                    <option value="">Floors</option>
                                    @for($y=1; $y<=5; $y++)
                                        <option value="{{ $y }}" @if($y == $floors) selected @endif >{{ $y }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-4">
                                <input type="text" class="form-control" name="indoor_area" value="{{ $indoor_area }}" id="indoor_area" placeholder="Indoor Area Size, m2">
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <input type="text" class="form-control" name="land_size" value="{{ $land_size }}" id="land_size" placeholder="Land Size, m2">
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea class=" form-control ckeditor" name="address" rows="10" placeholder="Address" >{{ $address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="web_link" value="{{ $web_link }}" id="web_link" placeholder="Web Link">
                            </div>
                        </div> <br>
                        <div class="form-group" style="background:#f0f0f0; margin: 0 0 20px; padding: 9px 18px 18px;">
                            <label class="col-md-12"><b>Your Remarks </b><br>
                                The information in this area will be visible only to you. Nobody else will see it. <br><br>
                                You can keep your property related information you find useful and search for it later.<br><br>
                            </label>
                            <div class="col-md-12">
                                <textarea class=" form-control ckeditor" name="private_comment" rows="6" placeholder="Private Comment" >{{ $private_comment }}</textarea>
                            </div>
                        </div> <br>
                        <!-- ******************************* Saeson ***************************** -->


                        <!-- ******************************* Map ***************************** -->
                        <div class="form-group">
                            <label class="col-md-12">Click to place your property on the map</label>
                            <div class="col-sm-12 col-md-12">
                                <div id="dvMap" style="width: 100%; height: 700px"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-md-6">
                                <input type="text" class="form-control" name="latitude" value="{{ $latitude }}" id="latitude" placeholder="Latitude">
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <input type="text" class="form-control" name="longitude" value="{{ $longitude }}" id="longitude" placeholder="Longitude">
                            </div>
                        </div><br>
                        <!-- ******************************* Map ***************************** -->

                        <div class="form-group">
                            <label class="col-md-2 control-label">Promote</label>
                            <div class="col-md-10">
                                <div class="md-radio-inline" data-error-container="#inline-radio">
                                    <div class="md-radio">
                                        <input type="radio" id="radio53" name="promote" value="2" class="md-radiobtn" @if($promote == 2) checked @endif>
                                        <label for="radio53">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Hot</label>
                                    </div>
                                    <div class="md-radio">
                                        <input type="radio" id="radio54" name="promote" value="3" class="md-radiobtn" @if($promote == 3) checked @endif>
                                        <label for="radio54">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Price Reduced</label>
                                        &nbsp;&nbsp; <input type="text" name="discount_price" value="{{ $discount_price }}" placeholder="Discount (%)">
                                    </div>
                                    <!-- <div class="md-radio">
                                        <input type="radio" id="radio55" name="promote" value="4" class="md-radiobtn" @if($promote == 4) checked @endif>
                                        <label for="radio55">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Investment</label>
                                    </div> -->
                                    <div class="md-radio">
                                        <input type="radio" id="radio56" name="promote" value="5" class="md-radiobtn" @if($promote == 5) checked @endif>
                                        <label for="radio56">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Bargain Price</label>
                                    </div>
                                </div>
                                <div id="inline-radio"></div>
                            </div>
                        </div>

                        <!-- ******************************* chackbox ***************************** -->
                        <div class="form-group">
                            <div class="col-sm-4 col-md-4">
                                <h3>Living</h3><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox1" class="md-check selectedAll1" name="selectedAll1">
                                        <label for="checkbox1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($living as $value)
                                        <?php $a_living = explode(",", $living_id); $check=""; ?>
                                        @if(!empty($a_living))
                                            @foreach($a_living as $living)
                                                @if($living == $value->living_id)
                                                    <?php $check="checked"; ?>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$i}}" class="md-check checkbox1" name="living_id[]" value="{{ $value->living_id }}" {{ $check }} >
                                            <label for="checkbox{{$i}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->living_name }} </label>
                                        </div>
                                        <?php $i++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div><br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <h3>Features</h3><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox16" class="md-check selectedAll2" name="selectedAll2">
                                        <label for="checkbox16">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($features as $value)
                                        <?php $a_features = explode(",", $features_id); $check=""; ?>
                                        @foreach($a_features as $feature)
                                            @if($feature == $value->features_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$j}}" class="md-check checkbox2" name="features_id[]" value="{{ $value->features_id }}" {{ $check }} >
                                            <label for="checkbox{{$j}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->features_name }} </label>
                                        </div>
                                        <?php $j++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <h3>View</h3><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox81" class="md-check selectedAll6" name="selectedAll6">
                                        <label for="checkbox81">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($views as $value)
                                        <?php $a_view = explode(",", $view_id); $check=""; ?>
                                        @foreach($a_view as $view_n)
                                            @if($view_n == $value->view_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$d}}" class="md-check checkbox6" name="view_id[]" value="{{ $value->view_id }}" {{ $check }} >
                                            <label for="checkbox{{$d}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->view_name }} </label>
                                        </div>
                                        <?php $d++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-4">
                                <h3>Community</h3><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox114" class="md-check selectedAll3" name="selectedAll3">
                                        <label for="checkbox114">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($community as $value)
                                        <?php $a_community = explode(",", $community_id); $check=""; ?>
                                        @foreach($a_community as $communites)
                                            @if($communites == $value->community_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$x}}" class="md-check checkbox3" name="community_id[]" value="{{ $value->community_id }}" {{ $check }} >
                                            <label for="checkbox{{$x}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->community_name }} </label>
                                        </div>
                                        <?php $x++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <h3>Services</h3><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox46" class="md-check selectedAll4" name="selectedAll4">
                                        <label for="checkbox46">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($service as $value)
                                        <?php $a_services = explode(",", $service_id); $check=""; ?>
                                        @foreach($a_services as $value1)
                                            @if($value1 == $value->service_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$m}}" class="md-check checkbox4" name="service_id[]" value="{{ $value->service_id }}" {{ $check }} >
                                            <label for="checkbox{{$m}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->service_name }} </label>
                                        </div>
                                        <?php $m++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <h3>Suitability</h3><br>
                                <div class="md-checkbox-list" data-error-container="#checkboxs">
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checkbox164" class="md-check selectedAll5" name="selectedAll5">
                                        <label for="checkbox164">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Check all </label>
                                    </div>
                                    @foreach($suitability as $value)
                                        <?php $a_suitability = explode(",", $suitability_id); $check=""; ?>
                                        @foreach($a_suitability as $value1)
                                            @if($value1 == $value->suitability_id)
                                                <?php $check="checked"; ?>
                                            @endif
                                        @endforeach
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="checkbox{{$z}}" class="md-check checkbox5" name="suitability_id[]" value="{{ $value->suitability_id }}" {{ $check }} >
                                            <label for="checkbox{{$z}}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                                {{ $value->suitability_name }} </label>
                                        </div>
                                        <?php $z++; ?>
                                    @endforeach
                                </div>
                                <div id="checkboxs"></div> <br>
                            </div>
                        </div><br>
                        <!-- ******************************* chackbox ***************************** -->

                        <!-- ******************************* image ***************************** -->
                        <?php
                            $field = 'img_name';
                            $label = 'Image';
                            $upload_path = 'property';
                        ?>
                        <div id="bg-img">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="img_thumbnail">
                                        @if($$field != '')
                                            <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                        @else
                                            <img src="" class="img_path" style="max-width:400px;">
                                        @endif

                                        <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">{{ $label }}</label>
                                <div class="col-md-4">
                                    @if($$field != '')
                                        <div class="btn btn-danger remove_img" >Remove</div>
                                        <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                    @elseif ($$field == '')
                                        <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                        <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                    @endif

                                    <input type="hidden" name="img_del" class="img_del" value="n">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-6">
                                    <span class="label label-danger">NOTE! &nbsp; </span>
                                    Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+ and Opera11.1+. In older browsers the filename is shown instead.
                                </div>
                            </div>
                        </div>
                        <!-- ******************************* image ***************************** -->

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/validation.js') }}
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('API_KEY') }}"></script>
    {{ Html::script('js/backend/map.js') }}
    {{ Html::script('js/backend/rent.js') }}
    <script>
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ $myLatLong }});
            var myOptions = {
                zoom: 11,
                center: myLatlng,
            };
            map = new google.maps.Map(document.getElementById('dvMap'), myOptions);
            // This event listener will call addMarker() when the map is clicked.
            google.maps.event.addListener(map, 'click', function(event) {
                addMarker(event.latLng);
                saveData(map, event);
            });
            // Adds a marker at the center of the map.
            addMarker(myLatlng);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script>
        CKEDITOR.replace('editor1', {            
            toolbar:       //กลุ่มเครื่องมือ ตัดออก เพิ่มเข้าได้  
            [ ['Bold', 'Italic'], ['NumberedList', 'BulletedList'], ['Undo', 'Redo'],
            ['Cut', 'Copy', 'Paste'] ]
        });  
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll1').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll2').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox2').attr('checked', 'checked');
                    $('.checkbox2').parent().addClass('checked');
                }else{
                    $('.checkbox2').removeAttr('checked');
                    $('.checkbox2').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll3').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox3').attr('checked', 'checked');
                    $('.checkbox3').parent().addClass('checked');
                }else{
                    $('.checkbox3').removeAttr('checked');
                    $('.checkbox3').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll4').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox4').attr('checked', 'checked');
                    $('.checkbox4').parent().addClass('checked');
                }else{
                    $('.checkbox4').removeAttr('checked');
                    $('.checkbox4').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll5').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox5').attr('checked', 'checked');
                    $('.checkbox5').parent().addClass('checked');
                }else{
                    $('.checkbox5').removeAttr('checked');
                    $('.checkbox5').parent().removeClass('checked');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.selectedAll6').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox6').attr('checked', 'checked');
                    $('.checkbox6').parent().addClass('checked');
                }else{
                    $('.checkbox6').removeAttr('checked');
                    $('.checkbox6').parent().removeClass('checked');
                }
            });
        });
    </script>
@endsection
