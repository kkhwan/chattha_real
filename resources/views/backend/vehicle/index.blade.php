<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <label class="control-label col-md-1">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by Name</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-2">Image</th>
                            <th class="text-center col-sm-3">{!! $obj_fn->sorting('Name','name',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">Detail</th>
                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                <th class="text-center col-sm-2"> </th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{ $key+1 }}</td>
                                        <td>
                                            @if ($field->img_name != '' && file_exists('uploads/vehicle/100/' . $field->img_name))
                                                <img src="{{ URL::asset('uploads/vehicle/100/' . $field->img_name) }}" class="img-responsive">
                                            @endif
                                        </td>
                                        <td>{{ $field->name }}</td>
                                        <td>
                                            @if( $field->type == 3)
                                                <b>Low Season : </b>{{ number_format($field->low_price) }} ฿ (per day) <br>
                                                <b>High Season : </b>{{ number_format($field->high_pice) }} ฿ (per day) <br>
                                                <b>Passenger Capacity : </b>{{ $field->passenger }} (ft.)<br>
                                                <b>Overnight Capacity : </b>{{ $field->overnight }} <br>
                                                <b>Crew : </b>{{ $field->crew }} <br>
                                                <b>Cabin Number : </b>{{ $field->cabin_no }} <br>
                                                <b>Cruising Speed : </b>{{ $field->cruising_speed }}  (Kts.) <br>
                                            @else
                                                <b>Low Season : </b>{{ number_format($field->low_price) }} ฿ (per day) <br>
                                                <b>High Season : </b>{{ number_format($field->high_pice) }} ฿ (per day) <br>
                                                <b>Engine : </b>{{ $field->engine }} <br>
                                                <b>Seat Capac : </b>{{ $field->seat_capac }} <br>
                                                <b>Auto / Manual : </b>
                                                    @if($field->auto_manual == 1) 
                                                        A/T
                                                    @elseif($field->auto_manual == 2)
                                                        Manual
                                                    @else
                                                        Auto / Manual
                                                    @endif<br>
                                            @endif
                                        </td>
                                        @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                            <td class="text-center">
                                                @if($permission['u'] == '1')
                                                    <a href="{{ url()->to($path.'/'.$field->$primaryKey.'/edit?1'.$str_param) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                @endif
                                                @if($permission['d'] == '1')
                                                    <form action="{{ url()->to($path.'/'.$field->$primaryKey) }}" class="form-delete" parent-data-id="{{ $field->$primaryKey }}" method="POST" >
                                                        <input type="hidden" name="_method" value="delete">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="button" class="btn btn-xs btn-circle red btn-delete" data-id="{{ $field->$primaryKey }}"><i class="fa fa-trash-o"></i></button>
                                                    </form>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="3">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
