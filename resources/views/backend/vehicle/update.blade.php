<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name" value="{{ $name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Low Season</label>
                            <div class="col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="low_from" value="{{ $low_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="low_to" value="{{ $low_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <input type="nummber" class="form-control" name="low_price" value="{{ $low_price }}" placeholder="Low Daily">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">High Season</label>
                            <div class="col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="10 oct 2012" data-date-format="dd M yyyy">
                                    <input type="text" class="form-control" name="high_from" value="{{ $high_from }}" placeholder="Date From">
                                                <span class="input-group-addon">
                                                to </span>
                                    <input type="text" class="form-control" name="high_to" value="{{ $high_to }}" placeholder="Date to">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <input type="nummber" class="form-control" name="high_pice" value="{{ $high_pice }}" placeholder="High Daily">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Engine</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="engine" value="{{ $engine }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Seat Capac</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="seat_capac" value="{{ $seat_capac }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Auto / Manual</label>
                            <div class="col-md-4">
                                <select class="form-control select2me" name="auto_manual" required="">
                                    <option value="1">Auto</option>
                                    <option value="2">Manual</option>
                                    <option value="3">Auto / Manual</option>
                                </select>
                            </div>
                        </div>
                        <?php
                            $field = 'img_name';
                            $label = 'Image';
                            $upload_path = 'vehicle';
                        ?>
                        <div id="bg-img">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="img_thumbnail">
                                        @if($$field != '')
                                        <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                        @else
                                        <img src="" class="img_path" style="max-width:400px;">
                                        @endif

                                        <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">{{ $label }}</label>
                                <div class="col-md-4">
                                    @if($$field != '')
                                    <div class="btn btn-danger remove_img" >Remove</div>
                                    <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                    @elseif ($$field == '')
                                    <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                    <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                    @endif

                                    <input type="hidden" name="img_del" class="img_del" value="n">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-4">
                                    <span style="color:#F00">* หมายเหตุ</span><br>
                                    <span style="">
                                        - รูปควรมีขนาด 800 x 460 pixel. <br>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/page/validation.js') }}
    {{ Html::script('js/backend/vehicle.js') }}
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
@endsection
