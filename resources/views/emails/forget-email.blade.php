<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Responsive Window -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Custom fonts -->
    {!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
    {!! Html::style('css/frontend/mail.css') !!}
</head>
<body>
@include('emails.include.header')
<div class="mail-body">
  Forget Email.
     {{$user}}
</div>
@include('emails.include.footer')
</body>
</html>
