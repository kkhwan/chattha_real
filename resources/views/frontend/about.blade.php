@extends('frontend.layout.main-layout')

@section('title', 'About Us')
@section('about_us', 'class="active"')

@section('contact-us')
    action="{{ URL::to('about_us/contact')}}"
@endsection

@section('css')
  {!! Html::style('css/frontend/about.css') !!}
@endsection

@section('content')
	<!-- <div class="container">
		<div class="row">
			<div class="col-md-9">
				<img src="{{ URL::asset('images/staff.JPG') }}" class="img-responsive" alt="staff">
			</div>
			<div class="col-md-3">
				<p> &nbsp; We are experienced real estate agents in Phuket with knowledge and understanding of this rapidly growing and changing market. Buy-Sell-Rent, Emphasizing on investment and property management, we pride ourselves on transparency, quality products and customer service.</p>
				<p> &nbsp; Whatever your real estate requirements, let us help find the right solution for you.</p>
			</div>
		</div>
	</div> -->

	<div class="content-agileits" id="about">
		<div class="welcome-w3">
			<div class="container">
				<div class="welcome-grids">
					<div class="col-md-8 col-sm-8 wel-grid1">
						<div class="port-7 effect-2">
							<div class="image-box">
								<img src="{{ URL::asset('images/staff.JPG') }}" alt="Image-2">
							</div>
							<div class="text-desc">
								<h4>Come with us.</h4>
								<!-- <p>Lorem ipsum dolor sit amet adipiscing.</p> -->
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 wel-grid">
						<h3 class="w3_tittle">About Us </h3>
						<!-- <h4>sit amet consectetur elit </h4> -->
						<p>We are experienced real estate agents in Phuket with knowledge and understanding of this rapidly growing and changing market. Buy-Sell-Rent, Emphasizing on investment and property management, we pride ourselves on transparency, quality products and customer service.</p>
						<p>Whatever your real estate requirements, let us help find the right solution for you.</p>
						<div class="right-sign-grid">
							<!-- <img src="images/sign.png" alt=" "> -->
							<!-- <h5>Ms.Chanapa Jiso</h5> -->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('script')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/fwslider.js') !!}
@endsection
