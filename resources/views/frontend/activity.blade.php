<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('frontend.layout.main-layout')

@section('title', 'Tours package')
@section('activity', 'class="active"')
@section('contact-us')
    action="{{ URL::to('activity/contact')}}"
@endsection

@section('css')
    {!! Html::style('css/frontend/activties.css') !!}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
@endsection

@section('content')
    <br>
    <div class="container">
        <div class="row">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <div class="col-md-offset-7 col-sm-offset-7 col-xs-offset-1 col-md-4 col-sm-4 col-xs-7">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}" placeholder="Search by Name">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-3">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($count_data > 0)
        <!-- <div class="trending-ads">
            <div class="container">
                <div class="agileits_work_grids">
                    <ul id="flexiselDemo1">
                        @foreach ($data as $key => $value)
                            <li>
                                <div class="l_g_r">
                                    <div class="dapibus">
                                        <h3>{{ substr($value->activity_name,0,20) }}</h3>
                                        @if ($value->img_name != '' && file_exists('uploads/activity/100/' . $value->img_name))
                                            <img src="{{ URL::asset('uploads/activity/' . $value->img_name) }}" class="img-responsive">
                                        @endif
                                        <br>
                                        <a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div> -->
        <div class="container">
            <div class="row">
                @foreach ($data as $key => $value)
                    <div class="col-md-3 col-sm-3">
                        <div class="l_g_r">
                            <div class="dapibus">
                                <h3 align="center">{{ substr($value->activity_name,0,18) }}</h3>
                                @if ($value->img_name != '' && file_exists('uploads/activity/100/' . $value->img_name))
                                    <img src="{{ URL::asset('uploads/activity/' . $value->img_name) }}" class="img-responsive">
                                @endif
                                <br>
                                <a data-toggle="modal" data-target="#myModal{{$key}}" class="link" align="center">Read More</a>
                            </div>
                        </div><br>
                        <div class="clearfix"> </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <br>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>

        <!-- /blog-pop-->
        @foreach ($data as $key2 => $value2)
            <div class="modal fade bd-example-modal-lg" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="tittle">{{ $value2->activity_name }}</h4><br>
                                    <div align="center">
                                        @if ($value2->img_name != '' && file_exists('uploads/activity/' . $value2->img_name))
                                            <img src="{{ URL::asset('uploads/activity/' . $value2->img_name) }}"  class="img-responsive">
                                        @endif
                                    </div>
                                    <!-- <h5 style="color: #868f98;">{{ $value2->title_name }}</h5>
                                    <div style="word-break: break-all; text-align: left;">
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value2->detail }}</p>
                                    </div> -->
                                </div>
                            </div><br>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <form action="{{ url()->to($path.'/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="name" value="" placeholder="name" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                                    <input type="email" class="form-control" name="email" value="" placeholder="Email" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                                    <input type="text" class="form-control" name="phone" value="" placeholder="Phone number (Please use international format)" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                                    <div class="input-group date date-time-picker">
                                                        <input type="text" size="16" readonly class="form-control" name="date_from">
                                                        <span class="input-group-btn">
                                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                                    <textarea name="msg" id="editor1" class="form-control " rows="5" placeholder="Request"></textarea>
                                                </div>
                                            </div> 
                                            <div class="form-actions">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" align="center">
                                                    <input type='hidden' name="activity_id" id="activity_id" class="form-control" value="{{ $value2->activity_id }}">
                                                    <button type="submit" class="btn" style="background: #c7040c; color: #fff;"><h3><b>Send</b></h3></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><br>
                    </div>
                </div>
            </div>
            <!-- <div class="modal ab fade" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog about" role="document">
                    <div class="modal-content about">
                        <div class="modal-header">
                            <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body about">
                            <div class="about">

                                <div class="about-inner">
                                    <h4 class="tittle">{{ $value2->activity_name }}</h4><br>
                                    <div align="center">
                                        @if ($value2->img_name != '' && file_exists('uploads/activity/' . $value2->img_name))
                                            <img src="{{ URL::asset('uploads/activity/' . $value2->img_name) }}"  class="img-responsive">
                                        @endif
                                    </div>
                                    
                                    <h5 style="color: #868f98;">{{ $value2->title_name }}</h5>
                                    <div style="word-break: break-all; text-align: left;">
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value2->detail }}</p>
                                    </div>
                                </div>
                                @if ($value2->is_book == 1)
                                <div style="margin: 5% 15% 0% 15%;">
                                    <form action="{{ url()->to($path.'/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="about-inner">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="name" value="" placeholder="name" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" name="email" value="" placeholder="Email" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="phone" value="" placeholder="Tel" required>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group date date-time-picker">
                                                        <input type="text" size="16" readonly class="form-control" name="date_from">
                                                        <span class="input-group-btn">
                                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="msg" id="editor1" class="form-control " rows="5" placeholder="Comment"></textarea>
                                                </div> 
                                                <div class="form-actions">
                                                    <input type='hidden' name="activity_id" id="activity_id" class="form-control" value="{{ $value2->activity_id }}">
                                                    <button type="submit" class="btn green">Book</button>
                                                    <button type="reset" class="btn default">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        @endforeach
    @endif
@endsection
@section('page-plugin')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}    
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
@endsection
@section('script')
    {{ Html::script('js/backend/validation.js') }}
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/jquery.flexisel.js') !!}
    <script>
        $(".date-time-picker").datetimepicker({
            autoclose: true,
            isRTL: Metronic.isRTL(),
            format: "dd MM yyyy - hh:ii",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
        });
    </script>
    <script type="text/javascript">
        $(window).load(function() {
            $("#flexiselDemo1").flexisel({
                visibleItems: 4,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 3000,            
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: { 
                    portrait: { 
                        changePoint:480,
                        visibleItems: 1
                    }, 
                    landscape: { 
                        changePoint:640,
                        visibleItems:2
                    },
                    tablet: { 
                        changePoint:768,
                        visibleItems: 3
                    }
                }
            });
            
        });
    </script>
@endsection
