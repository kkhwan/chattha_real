<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('frontend.layout.main-layout')

@section('title', 'CHATTHA -  Activities')
@section('activity', 'class="active"')

@section('css')
  {!! Html::style('css/frontend/phuket.css') !!}
  <!-- {!! Html::style('css/frontend/activties.css') !!} -->
@endsection

@section('content')
    @if($count_data > 0)
        <div class="trending-ads">
            <div class="container">
                <div class="agileits_work_grids">
                    <ul id="flexiselDemo1">
                        @foreach ($data as $key => $value)
                            <li>
                                <div class="l_g_r">
                                    <div class="dapibus">
                                        <h2>{{ $value->activity_name }}</h2>
                                        @if ($value->img_name != '' && file_exists('uploads/phuket/100/' . $value->img_name))
                                            <img src="{{ URL::asset('uploads/phuket/' . $value->img_name) }}" class="img-responsive">
                                        @endif
                                        <h4 style="color: #bbb;">{{ $value->title_name }}</h4>
                                        <p>{{ substr($value->detail,0,50) }}...</p>
                                        <a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
        <!-- /blog-pop-->
        @foreach ($data as $key2 => $value2)
            <div class="modal ab fade" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog about" role="document">
                    <div class="modal-content about">
                        <div class="modal-header">
                            <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body about">
                            <div class="about">

                                <div class="about-inner">
                                    <div align="center">
                                        @if ($value2->img_name != '' && file_exists('uploads/activity/' . $value2->img_name))
                                            <img src="{{ URL::asset('uploads/activity/' . $value2->img_name) }}"  class="img-responsive">
                                        @endif
                                    </div>
                                    
                                    <h4 class="tittle">{{ $value2->activity_name }}</h4>
                                    <p style="color: #868f98;">{{ $value2->title_name }}</p>
                                    <div style="word-break: break-all; text-align: left;">
                                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value2->detail }}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection
@section('script')


@endsection
