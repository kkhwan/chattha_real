@extends('frontend.layout.main-layout')

@section('title', ' - Active')
@section('active', 'class="active"')

@section('css')
  {!! Html::style('css/frontend/activties.css') !!}
@endsection

@section('content')
        <div id="works" class=" clearfix grid">
            @foreach($data_image as $key => $field)
                <figure class="effect-oscar  wowload fadeInUp">
                    @if ($field->img_name != '' && file_exists('uploads/activity/' . $field->img_name))
                        <img src="{{ URL::asset('uploads/activity/' . $field->img_name) }}">
                    @endif
                </figure>
            @endforeach
        </div>
@endsection
@section('script')


@endsection
