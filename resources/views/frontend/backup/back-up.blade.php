@extends('frontend.layout.main-layout')

@section('title', 'Vehicle')
@section('vehicle', 'class="active"')

@section('css')
  {!! Html::style('css/frontend/vehicle.css') !!}
  {!! Html::style('css/frontend/flexslider.css') !!}
  {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
  <!-- {!! Html::style('css/frontend/yacht.css') !!} -->
@endsection

@section('content')
    <div class="featured_section_w3l">
        <div class="container">
            <div class="inner_tabs">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">cars</a></li>
                        <li role="presentation"><a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Motorbike</a></li>
                        <li role="presentation"><a href="#tree" role="tab" id="tree-tab" data-toggle="tab" aria-controls="tree">Yacht</a></li>
                        
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
                           <div class="section__content clearfix">
                                <!-- /card1 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Tel" required=""/>
                                                        <input type="text" name="location" class="location" placeholder="Location" required=""/>
                                                        <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                            <input type="text" class="form-control" name="date_from" placeholder="mm/dd/yyyy">
                                                            <span class="input-group-addon">to</span>
                                                            <input type="text" class="form-control" name="date_to" placeholder="mm/dd/yyyy">
                                                        </div>
                                                        <input type="text" name="phone" class="" placeholder="Comment"/>
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card1 -->
                                <!-- /card2 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card2 -->
                                <!-- /card3 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card3 -->
                                <!-- /card4 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card4 -->
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
                            <div class="section__content clearfix">
                                <!-- /card1 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card1 -->
                                <!-- /card2 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card2 -->
                                <!-- /card3 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card3 -->
                                <!-- /card4 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card4 -->
                            </div>  
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tree" aria-labelledby="tree-tab">
                            <div class="section__content clearfix">
                                <!-- /card1 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card1 -->
                                <!-- /card2 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card2 -->
                                <!-- /card3 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card3 -->
                                <!-- /card4 -->
                                <div class="card effect__hover">
                                    <div class="card__front">
                                        <span class="card__text">
                                            <div class="img-grid">
                                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                                <div class="car_description">
                                                    <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                    <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                    <p>Estimated Price</p>
                                                    <div class="date">Mar 2017</div>
                                                    <p>Expected Launch</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="card__back">
                                        <span class="card__text">
                                            <div class="login-inner2">
                                                <h4>Be the first to know</h4>
                                                <div class="login-top sign-top">
                                                    <form>
                                                        <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                        <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                        <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                        <div class="section_drop">
                                                            <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                                <option value="city">Amsterdam</option>
                                                                <option value="city">Bahrain</option>
                                                                <option value="city">Cannes</option>
                                                                <option value="city">Dublin</option>
                                                                <option value="city">Edinburgh</option>
                                                                <option value="city">Florence</option>
                                                                <option value="city">Georgia</option>
                                                                <option value="city">Hungary</option>
                                                                <option value="city">Hong Kong</option>
                                                                <option value="city">Johannesburg</option>
                                                                <option value="city">Kiev</option>
                                                                <option value="city">London</option>
                                                                <option value="city">Others...</option>
                                                            </select>
                                                        </div>   
                                                        <input type="submit" value="Done">   
                                                    </form>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- //card4 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!--//featured_section-->
<!-- //-------------------------------------------------------------------------------------------------------------------------------// -->
    <!-- <div class="trending-ads">
        <div class="container-fluid"> -->
            <!-- slider -->
            <!-- <div class="agile-trend-ads">
                <h2>YACHT HIRE</h2>
                <ul id="flexiselDemo3">
                    <li> -->
                        <!-- /card1 -->
                        <!-- <div class="col-md-3 ">
                            <div class="card effect__hover">
                                <div class="card__front">
                                    <span class="card__text">
                                        <div class="img-grid">
                                            <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                            <div class="car_description">
                                                <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                <p>Estimated Price</p>
                                                <div class="date">Mar 2017</div>
                                                <p>Expected Launch</p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="card__back">
                                    <span class="card__text">
                                         <div class="login-inner2">
                                            <h4>Be the first to know</h4>
                                            <div class="login-top sign-top">
                                                <form>
                                                    <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                    <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                    <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                    <div class="section_drop">
                                                        <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                            <option value="null"> Select City</option>
                                                            <option value="city">Amsterdam</option>
                                                            <option value="city">Bahrain</option>
                                                            <option value="city">Cannes</option>
                                                            <option value="city">Dublin</option>
                                                            <option value="city">Edinburgh</option>
                                                            <option value="city">Florence</option>
                                                            <option value="city">Georgia</option>
                                                            <option value="city">Hungary</option>
                                                            <option value="city">Hong Kong</option>
                                                            <option value="city">Johannesburg</option>
                                                            <option value="city">Kiev</option>
                                                            <option value="city">London</option>
                                                            <option value="city">Others...</option>
                                                        </select>
                                                    </div>   
                                                    <input type="submit" value="Done">
                                                </form>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <!-- //card1 -->
                        <!-- /card2 -->
                        <!-- <div class="col-md-3 ">
                            <div class="card effect__hover">
                                <div class="card__front">
                                    <span class="card__text">
                                        <div class="img-grid">
                                            <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                            <div class="car_description">
                                                <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                <p>Estimated Price</p>
                                                <div class="date">Mar 2017</div>
                                                <p>Expected Launch</p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="card__back">
                                    <span class="card__text">
                                         <div class="login-inner2">
                                            <h4>Be the first to know</h4>
                                            <div class="login-top sign-top">
                                                <form>
                                                    <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                    <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                    <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                    <div class="section_drop">
                                                        <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                            <option value="city">Amsterdam</option>
                                                            <option value="city">Bahrain</option>
                                                            <option value="city">Cannes</option>
                                                            <option value="city">Dublin</option>
                                                            <option value="city">Edinburgh</option>
                                                            <option value="city">Florence</option>
                                                            <option value="city">Georgia</option>
                                                            <option value="city">Hungary</option>
                                                            <option value="city">Hong Kong</option>
                                                            <option value="city">Johannesburg</option>
                                                            <option value="city">Kiev</option>
                                                            <option value="city">London</option>
                                                            <option value="city">Others...</option>
                                                        </select>
                                                    </div>   
                                                    <input type="submit" value="Done">
                                                </form>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <!-- //card2 -->
                        <!-- /card3 -->
                        <!-- <div class="col-md-3 ">
                            <div class="card effect__hover">
                                <div class="card__front">
                                    <span class="card__text">
                                        <div class="img-grid">
                                            <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                            <div class="car_description">
                                                <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                <p>Estimated Price</p>
                                                <div class="date">Mar 2017</div>
                                                <p>Expected Launch</p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="card__back">
                                    <span class="card__text">
                                         <div class="login-inner2">
                                            <h4>Be the first to know</h4>
                                            <div class="login-top sign-top">
                                                <form>
                                                    <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                    <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                    <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                    <div class="section_drop">
                                                        <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                            <option value="city">Amsterdam</option>
                                                            <option value="city">Bahrain</option>
                                                            <option value="city">Cannes</option>
                                                            <option value="city">Dublin</option>
                                                            <option value="city">Edinburgh</option>
                                                            <option value="city">Florence</option>
                                                            <option value="city">Georgia</option>
                                                            <option value="city">Hungary</option>
                                                            <option value="city">Hong Kong</option>
                                                            <option value="city">Johannesburg</option>
                                                            <option value="city">Kiev</option>
                                                            <option value="city">London</option>
                                                            <option value="city">Others...</option>
                                                        </select>
                                                    </div>   
                                                    <input type="submit" value="Done">
                                                </form>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <!-- //card3 -->
                        <!-- /card4 -->
                        <!-- <div class="col-md-3">
                            <div class="card effect__hover">
                                <div class="card__front">
                                    <span class="card__text">
                                        <div class="img-grid">
                                            <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz">
                                            <div class="car_description">
                                                <h4><a href="single.html">Mercedes-Benz C250 CDI</a></h4>
                                                <div class="price"><span class="fa fa-rupee"></span><span class="font25">$ 8000 - $ 12000</span></div>
                                                <p>Estimated Price</p>
                                                <div class="date">Mar 2017</div>
                                                <p>Expected Launch</p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="card__back">
                                    <span class="card__text">
                                         <div class="login-inner2">
                                            <h4>Be the first to know</h4>
                                            <div class="login-top sign-top">
                                                <form>
                                                    <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                    <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                    <input type="text" name="phone" class="phone" placeholder="Phone" required=""/>
                                                    <div class="section_drop">
                                                        <select id="country1" onchange="change_country(this.value)" class="frm-field required">
                                                                <option value="null"> Select City</option>
                                                            <option value="city">Amsterdam</option>
                                                            <option value="city">Bahrain</option>
                                                            <option value="city">Cannes</option>
                                                            <option value="city">Dublin</option>
                                                            <option value="city">Edinburgh</option>
                                                            <option value="city">Florence</option>
                                                            <option value="city">Georgia</option>
                                                            <option value="city">Hungary</option>
                                                            <option value="city">Hong Kong</option>
                                                            <option value="city">Johannesburg</option>
                                                            <option value="city">Kiev</option>
                                                            <option value="city">London</option>
                                                            <option value="city">Others...</option>
                                                        </select>
                                                    </div>   
                                                    <input type="submit" value="Done">
                                                </form>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <!-- //card4 -->
                    <!-- </li> 
                </ul>
            </div>

        </div>
    </div> -->

<!-- //-------------------------------------------------------------------------------------------------------------------------------// -->
    <!-- <div class="container" >
        <h1 align="center">Car or Bike Rental</h1>
        <p>
            Providing a comprehensive range of services with long-term & short-term rental.
            Select the vehicle your interested in to rent and fill in the form. if you have any question please let us know.
        </p>
        <hr>
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Toyota Yaris 2016</h3>
                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="" width="100%">
                <p align="left">
                    Price : 1,250 ฿* (per day)<br>
                    Engine : 1.2<br>
                    Seat Capac : 4<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Nissan Almera</h3>
                <img src="images/hire/nissan-almera.jpg" alt="" width="100%">
                <p align="left">
                    Price : 1,250* ฿ (per day)<br>
                    Engine : 1.2<br>
                    Seat Capac : 5<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Toyota Vios</h3>
                <img src="images/hire/toyota-vios.jpg" alt="" width="100%">
                <p align="left">
                    Price : 1,440 ฿* (per day)<br>
                    Engine : 1.5<br>
                    Seat Capac : 5<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Honda Jazz</h3>
                <img src="images/hire/2016-Honda-Jazz-White-Desktop-Wallpaper.jpg" alt="" width="100%">
                <p align="left">
                    Price : 1,440 ฿* (per day)<br>
                    Engine : 1.5<br>
                    Seat Capac : 5<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">HONDA CR-V</h3>
                <img src="images/hire/honda-crv.jpg" alt="" width="100%">
                <p align="left">
                    Price : Start 2,570 ฿* (per day)<br>
                    Engine : 2.0<br>
                    Seat Capac : 5<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Toyota Fortuner</h3>
                <img src="images/hire/toyota-fortuner.jpg" alt="" width="100%">
                <p align="left">
                    Price : 2,580 ฿ (per day)<br>
                    Engine : 2.4<br>
                    Seat Capac : 7<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Mitsubishi Pajero</h3>
                <img src="images/hire/mitsubishi-pajero.jpg" alt="" width="100%">
                <p align="left">
                    Price : 2,580 ฿ (per day)<br>
                    Engine : 2.4<br>
                    Seat Capac : 7<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6" align="center">
                <h3 class="">Toyota Hilux Revo</h3>
                <img src="images/hire/toyota-hilux-revo.jpg" alt="" width="100%">
                <p align="left">
                    Price : 1,820 ฿ (per day)<br>
                    Engine : 2.4<br>
                    Seat Capac : 7\4<br>
                    Auto/Manual : A/T
                </p>
                <button type="button" class="btn btn-danger">Book</button>
            </div>
        </div>
        <div class="clearfix"> </div>
        <hr>
    </div> -->
    <br><br><br><br>

@endsection
@section('page-plugin')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}    
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
@endsection
@section('script')
    {!! Html::script('js/frontend/fwslider.js') !!}
    {{ Html::script('js/backend/validation.js') }}
    {{ Html::script('js/backend/default.js') }}
    <!-- {!! Html::script('js/frontend/jquery.flexisel.js') !!} -->
    <!-- <script type="text/javascript">
         $(window).load(function() {
            $("#flexiselDemo3").flexisel({
                visibleItems:1,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 5000,            
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: { 
                    portrait: { 
                        changePoint:480,
                        visibleItems:1
                    }, 
                    landscape: { 
                        changePoint:640,
                        visibleItems:1
                    },
                    tablet: { 
                        changePoint:768,
                        visibleItems:1
                    }
                }
            });
             
        });
    </script> -->
@endsection
