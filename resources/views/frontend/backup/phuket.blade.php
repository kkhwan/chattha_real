<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('frontend.layout.main-layout')

@section('title', 'Phuket')
@section('phuket', 'class="active"')

@section('css')
  {!! Html::style('css/frontend/phuket.css') !!}
@endsection	

@section('content')
	<div class="trending-ads">
	    <div class="container">
	    	<div class="agileits_work_grids">
				<ul id="flexiselDemo1">
					<!-- <li>
						<div class="card effect__hover">
	                        <div class="card__front">
	                            <span class="card__text">
	                                <div class="img-grid">
	                                    <img src="images/hire/Toyota-Yaris-2016.jpg" alt="Catchy Carz" class="img-responsive">
	                                    <div class="car_description">
	                                        <h4><a href="single.html">Toyota Yaris 2016</a></h4>
	                                        <p align="left"><biv class="date">Price : 1,250 ฿</biv> (per day)</p>
	                                        <p align="left"><biv class="date">Engine : </biv>1.2</p>
	                                        <p align="left"><biv class="date">Seat Capac : </biv>4</p>
	                                        <p align="left"><biv class="date">Auto/Manual : </biv>A/T</p>
	                                    </div>
	                                </div>
	                            </span>
	                        </div>
	                        <div class="card__back">
	                            <span class="card__text">
	                                <div class="login-inner2">
	                                    <h4>Select your Vehicle</h4>
	                                    <div class="login-top sign-top">
	                                        <form>
	                                            <input type="text" name="name" class="name active" placeholder="Name" required=""/>
	                                            <input type="text" name="email" class="email" placeholder="Email" required=""/>
	                                            <input type="text" name="phone" class="phone" placeholder="Tel" required=""/>
	                                            <input type="text" name="location" class="location" placeholder="Location" required=""/>
	                                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
	                                                <input type="text" class="form-control" name="date_from" placeholder="mm/dd/yyyy">
	                                                <span class="input-group-addon">to</span>
	                                                <input type="text" class="form-control" name="date_to" placeholder="mm/dd/yyyy">
	                                            </div>
	                                            <input type="text" name="phone" class="" placeholder="Comment"/>
	                                            <input type="submit" value="Done">   
	                                        </form>
	                                    </div>
	                                </div>
	                            </span>
	                        </div>
	                    </div>
					</li> -->
					@foreach ($data as $key => $value)
						<li>
							<div class="l_g_r">
								<div class="dapibus">
									<h2>{{ $value->phuket_name }}</h2>
									@if ($value->img_name != '' && file_exists('uploads/phuket/' . $value->img_name))
                                        <img src="{{ URL::asset('uploads/phuket/' . $value->img_name) }}" class="img-responsive">
                                    @endif
									<p>{{ substr($value->detail,0,50) }}...</p>
									<a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>
								</div>
							</div>
						</li>
					@endforeach
					
					<!-- <li>
						<div class="l_g_r">
							<div class="dapibus">
								<h2>Phuket</h2>
								<p class="adm">Posted by <a href="#">Admin</a>  |  7 days ago</p>
								<a data-toggle="modal" data-target="#myModal1"><img src="images/hire/Toyota-Yaris-2016.jpg" class="img-responsive" alt=""></a>
								<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate. </p>
								<a data-toggle="modal" data-target="#myModal1" class="link">Read More</a>
							</div>
						</div>
					</li>
					
					<li>
						<div class="agileits_work_grid view view-sixth">
							<img src="images/hire/Toyota-Yaris-2016.jpg" alt=" " class="img-responsive" />
							<div class="mask">
								<a href="#" data-toggle="modal" data-target="#myModal1" class="info">Dazzling Birds</a>
							</div>
						</div>
					</li> -->

				</ul>
			</div>
	    </div>
	</div>
    <!-- /blog-pop-->
    @foreach ($data as $key2 => $value2)
    	<div class="modal ab fade" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog about" role="document">
	            <div class="modal-content about">
	                <div class="modal-header">
	                    <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                </div>
	                <div class="modal-body about">
	                    <div class="about">

	                        <div class="about-inner">
	                        	<div align="center">
	                        		@if ($value2->img_name != '' && file_exists('uploads/phuket/100/' . $value2->img_name))
	                                    <img src="{{ URL::asset('uploads/phuket/' . $value2->img_name) }}"  class="img-responsive">
	                                @endif
	                        	</div>
	                            
	                            <h4 class="tittle">{{ $value2->phuket_name }}</h4>
								<p style="color: #868f98;">Posted by Chattha on {{ $obj_fn->format_date_en($value2->updated_at,7) }}</p>
								<div style="word-break: break-all; text-align: left;">
									<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value2->detail }}</p>
								</div>
	                        </div>

	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    @endforeach
    <!-- //blog-pop-->
@endsection
@section('script')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/jquery.flexisel.js') !!}
    <script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo1").flexisel({
				visibleItems: 4,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems:2
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			
		});
	</script>
@endsection
