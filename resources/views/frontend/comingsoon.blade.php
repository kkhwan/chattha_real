@extends('frontend.layout.main-layout')

@section('title', 'Coming Soon')
@section('', 'class="active"')

@section('css')
  {!! Html::style('css/frontend/coming_soon.css') !!}
@endsection

@section('content')
	coming soon
@endsection
@section('script')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/fwslider.js') !!}
@endsection
