@extends('frontend.layout.main-layout')

@section('title', 'Contact')
@section('contact', 'class="active"')
@section('contact-us')
    action="{{ URL::to('contact/store')}}"
@endsection

@section('css')
  	{!! Html::style('css/frontend/home.css') !!}
  	{!! Html::style('css/frontend/contact.css') !!}
@endsection

@section('content')
  	<div id="emailme" class="contact">
  		<div class="container">
  			<div class="col-md-8 contact-left ">
  				<div class="horizontal-tab">
  					<div class="tab-content">
  						<div class="tab-pane active in" id="tab1">
  							<div class="contact-form" style="border:1px solid #fff; background-color: #fbfbfb; padding:20px 15px; padding-bottom: 0px;">
                  <ul>
                    <li><br><i class="fa fa-phone fa-lg" aria-hidden="true" style="border:1px #fff;"></i>+66 (0)76 636 244</li>
                    <li><i class="fa fa-mobile fa-lg" aria-hidden="true" style="border:1px #fff;"></i> +66 (0)90 179 6635</li>
                    <li><i class="fa fa-envelope-o fa-lg" aria-hidden="true" style="border:1px #fff;"></i>info@chattharealestate.com</li>
                    <li><i class="fa fa-facebook-square fa-lg" aria-hidden="true" style="border:1px #fff;"></i>Chattha Real Estate & Property Management Phuket</li>
                    <li><i class="fa fa-home fa-lg" aria-hidden="true" style="border:1px #fff;"></i><b>Head Office : </b>16/18 Moo 2, Phra Phuket Kaew Road, Kathu, Phuket 83120 Thailand</li>
                    <li><i class="fa fa-home fa-lg" aria-hidden="true" style="border:1px #fff;"></i><b>Chalong Branch: </b>62/66 Moo 10, Chao Fha Road, Chalong, Meaung, Phuket 83000 Thailand</li> <hr>
                    <li">We are experienced real estate agents in Phuket with knowledge and understanding of this rapidly growing and changing market. Emphasizing on investment and property management, we pride ourselves on transparency, quality products and customer service.<br><br></li>
                    <li>Whatever your real estate requirements, let us help find the right solution for you.</li> <hr>
                    <li style="text-align: center;"><b>Office Hours:</b></li>
                    <li style="text-align: center;">Monday – Saturday: 9.00am – 18.00pm</li>
                    <li style="text-align: center;">Sunday: CLOSED (By appointment ONLY)</li>
                  </ul>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="col-md-4 contact-right ">
          <div class="horizontal-tab">
            <div class="tab-content">
              <div class="tab-pane active in" id="tab1">
                <div class="contact-form">
                  <form action="{{ url()->to($url_to.'/store') }}" method="POST" enctype="multipart/form-data">
                    <!-- <input type="hidden" name="_method" value="{{ $method }}"> -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="text" name="name" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
                    <input type="email" name="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="text" name="mobile" value="Mobile" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Mobile';}" required="">
                    <input type="text" name="nationality" value="Nationality" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nationality';}" required="">
                    <textarea type="text" name="msg" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
                    <input type="submit" value="Submit" >
                  </form>
                </div>
              </div>
            </div>
          </div>
  			</div>
  		</div>
  	</div>
@endsection
