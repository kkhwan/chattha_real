
<div class="header-top">
	<div class="container">
		<div class="logo">
			<a href="{{ URL::to('/') }}"><img src="{{URL::asset('images/chattha-real15.jpg')}}" alt="logo" /></a>
		</div>
		<div class="header-address">
			<ul>
				<!-- <li></li>
				<li>
					<a href="" ><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a><br>
				</li>
				<li>
					<a href="https://www.facebook.com/chattharealestatephuket/" ><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a><br>
				</li>
				<li>
					<a href="" ><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a><br>
				</li>
				<li>
					<a href="https://pin.it/vxvYwKR" ><i class="fa fa-pinterest-p  fa-2x" aria-hidden="true"></i></a><br>
				</li> -->
				<li>
					<span><b> Contact us: </b></span><br>
					<div style="color: #a0a0a0;"> info@chattharealestate.com <br>
					+66 (0) 76 636 244 <br> +66 (0) 90 179 6635 </div>
				</li>
				<li>
					<span><b> Opening Hours: </b></span> <br>
					<div style="color: #a0a0a0;"> 9am - 6pm <br>
					Monday - Saturday <br> Sunday ( By Appointment )  </div>
				</li>
			</ul>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!-- //header-top -->
</div>
<div class="main_section_agile">
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<nav class="link-effect-2" id="link-effect-2">
					<ul class="nav navbar-nav">
						<li @yield('home')><a href="{{ URL::to('/') }}" class="effect-3">Home</a></li>
						<!-- <li @yield('service')><a href="{{ URL::to('service') }}" class="effect-3">Services</a></li> -->
						<!-- <li @yield('property')><a href="{{ URL::to('property') }}" class="effect-3">Property</a></li> -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle effect-3" data-toggle="dropdown">Property<b class="caret"></b></a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li @yield('sale')><a href="{{ URL::to('sale') }}">Buy</a></li>
								<li @yield('rentel')><a href="{{ URL::to('rentel') }}">Rent</a></li>
							</ul>
						</li>
						<!-- <li class="dropdown">
							<a href="#" class="dropdown-toggle effect-3" data-toggle="dropdown">Hire<b class="caret"></b></a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li @yield('vehicle')><a href="{{ URL::to('vehicle') }}">Vehicle</a></li>
								<li @yield('yacht')><a href="{{ URL::to('yacht') }}">Yacht</a></li>
							</ul>
						</li> -->
						<li @yield('vehicle')><a href="{{ URL::to('vehicle') }}" class="effect-3">Vehicle Hire</a></li>
						<li @yield('activity')><a href="{{ URL::to('activity') }}" class="effect-3">Activity</a></li>
						<li @yield('phuket')><a href="{{ URL::to('phuket') }}" class="effect-3">Phuket</a></li>
						<!-- <li @yield('phuket')><a href="{{ URL::to('phuket') }}" class="effect-3">Phuket</a></li> -->
						<li @yield('phuket')><a href="https://www.coconutgrovephuket.com/" target="_blank" class="effect-3">Coconut grove</a></li>
						<li @yield('service')><a href="{{ URL::to('our_service') }}" class="effect-3">Service</a></li>
						<li @yield('about_us')><a href="{{ URL::to('about_us') }}" class="effect-3">About Us</a></li>
						<li @yield('contact')><a href="{{ URL::to('contact') }}" class="effect-3">Contact Us</a></li>
						
					</ul>
				</nav>

			</div>
		</div>
	</nav>	
	<div class="clearfix"> </div> 
</div>


