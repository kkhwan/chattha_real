@extends('frontend.layout.main-layout')

@section('title', 'Home')
@section('home', 'class="active"')
@section('contact-us')
    action="{{ URL::to('/contact_us')}}"
@endsection

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
    <!-- start slider -->
    <div class="banner">
        <div id="fwslider">
            <div class="slider_container">
                <div class="slide">
                    <!-- Slide image -->
                    <img src="{{URL::asset('images/banner/banner1.jpg')}}" class="img-responsive" alt="" />
                    <!-- /Slide image -->
                    <!-- Texts container -->
                    <div class="slide_content">
                        <div class="slide_content_wrap">
                            <!-- Text title -->
                            <!-- <h1 class="title">Run Over<br>Everything</h1> -->
                            <!-- /Text title -->
                            <!-- <div class="button"><a href="http://admissions.te.psu.ac.th/">See Details</a></div> -->
                        </div>
                    </div>
                    <!-- /Texts container -->
                </div>
                <div class="slide">
                    <img src="{{URL::asset('images/banner/banner3.jpg')}}" class="img-responsive" alt="" />
                </div>
                <div class="slide">
                    <img src="{{URL::asset('images/banner/banner4.jpg')}}" class="img-responsive" alt="" />
                </div>
                <div class="slide">
                    <img src="{{URL::asset('images/banner/banner2.jpg')}}" class="img-responsive" alt="" />
                </div>
                <!-- <div class="slide">
                    <img src="{{URL::asset('images/banner/banner5.jpg')}}" class="img-responsive" alt="" />
                </div>
                <div class="slide">
                    <img src="{{URL::asset('images/banner/banner6.jpg')}}" class="img-responsive" alt="" />
                </div> -->
                <div class="clearfix"> </div>
            </div>
            <div class="timers"></div>
            <div class="slidePrev"><span></span></div>
            <div class="slideNext"><span></span></div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!--/slider -->

    <div class="container-fluid" >
        <div class="services " >
            <div class="services-w3ls-row">
                <span class="vc_sep_holder vc_sep_holder_l">
                    <span style="border-color:#c7040c;" class="vc_sep_line"></span>
                </span>
                <h2 class="agileits-title w3title1" style="color:#c7040c; margin-top: 0px;">INVESTED IN YOU</h2>
                <span class="vc_sep_holder vc_sep_holder_l">
                    <span style="border-color:#c7040c;" class="vc_sep_line"></span>
                </span><br>
                <div class="col-md-2 col-sm-2 col-xs-6 services-grid agileits-w3layouts">
                    <span class="glyphicon glyphicon-home effect-1" aria-hidden="true"></span>
                    <h5>BUY</h5>
                    <!-- <p>Our selected sale properties.</p> -->
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 services-grid agileits-w3layouts">
                    <span class="glyphicon glyphicon-tags effect-1" aria-hidden="true"></span>
                    <h5>SELL</h5>
                    <!-- <p>List your property for sale and rent with us.</p> -->
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 services-grid agileits-w3layouts">
                    <span class="glyphicon glyphicon-heart-empty effect-1" aria-hidden="true"></span>
                    <h5>RENT</h5>
                    <!-- <p>Holiday property rental is available.</p> -->
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 services-grid">
                    <span class="glyphicon glyphicon-usd effect-1" aria-hidden="true"></span>
                    <h5>GUARANTEED INCOME</h5>
                    <!-- <p>We offer Rental Guaranteed Income Program.</p> -->
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 services-grid">
                    <span class="glyphicon glyphicon-wrench effect-1" aria-hidden="true"></span>
                    <h5>PROPERTY MANAGEMENT</h5>
                    <!-- <p>A one stop property management solution.</p> -->
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 services-grid">
                    <span class="glyphicon glyphicon-user effect-1" aria-hidden="true"></span>
                    <h5>LEGAL SERVICES</h5>
                    <!-- <p>property, company registration, visas, work permits, accounting & tax.</p> -->
                </div>
                <div class="clearfix"> </div>
            </div>  
        </div>
    </div><br>
@endsection
@section('script')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/fwslider.js') !!}
@endsection
