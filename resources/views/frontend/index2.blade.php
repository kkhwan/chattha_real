<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CHATTHA REAL ESTATE</title>
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') !!}
    {{ Html::style('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}
    {{ Html::style('assets/global/plugins/uniform/css/uniform.default.min.css') }}
    {{ Html::style('css/comming_soon/style.css') }}
    <!-- {{ Html::style('css/comming_soon/icons.css') }} -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body style="background: url(../images/comming_soon/bg-image.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <br><br>
                <h1>New Site Coming Soon</h1>
                <h3 style="color: #fff">Chattha Real Estate Phuket</h3><br>
                
            </div>    
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-6" align="center">
                <p style="color: #fff">Chattha Real Estate incorporated with Zest Real Estate offers complete portfolio of real estate services including luxury market, tours, bike-car- boat rental in Phuket and legal services including property legal, visa, work permit. <br>
                    With our unique contemporary style and total passion for connecting people with property, we aspire to provide the ultimate real estate experience for today’s modern clients. Chattha Real Estate also advises individuals and corporate on growth strategies and real estate investment opportunities in current and new markets. <br><br>
                    <!-- We are experienced real estate agents in Phuket with knowledge and understanding of this rapidly growing and changing market. Emphasizing on investment and property management, we pride ourselves on transparency, quality products and customer service. <br>

                    Whatever your real estate requirements, let us help find the right solution for you. <br>
                    <b>Office Hours:</b> <br>
                    Monday – Saturday: 9.00am – 18.00pm <br> Sunday: CLOSED (By appointment ONLY) <br> -->

                    Visit us:  <a href="https://www.facebook.com/chattharealestatephuket/" target="_blank" "facebook" >https://www.facebook.com/chattharealestatephuket/</a>
                </p>
            </div>
        </div>
    </div>
    {!! Html::script('assets/global/scripts/jquery.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    {!! Html::script('js/comming_soon/jquery.downCount.js') !!}
    {!! Html::script('js/comming_soon/jquery.downCount.js') !!}
</body>
</html>