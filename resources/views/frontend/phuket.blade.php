<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('frontend.layout.main-layout')

@section('title', 'Phuket')
@section('phuket', 'class="active"')
@section('contact-us')
    action="{{ URL::to('phuket/contact')}}"
@endsection
@section('css')
  {!! Html::style('css/frontend/phuket.css') !!}
@endsection	

@section('content')
	<br>
    <div class="container">
        <div class="row">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <div class="col-md-offset-7 col-sm-offset-7 col-xs-offset-1 col-md-4 col-sm-4 col-xs-7">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}" placeholder="Search by Name">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-3">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	<!-- <div class="trending-ads">
	    <div class="container">
	    	<div class="agileits_work_grids">
				<ul id="flexiselDemo1">
					@foreach ($data as $key => $value)
						<li>
							<div class="l_g_r">
								<div class="dapibus">
									<h2>{{ $value->phuket_name }}</h2>
									@if ($value->img_name != '' && file_exists('uploads/phuket/100/' . $value->img_name))
                                        <img src="{{ URL::asset('uploads/phuket/' . $value->img_name) }}" class="img-responsive">
                                    @endif
									<p>{{ substr($value->detail,0,50) }}...</p>
									<a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>
								</div>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
	    </div>
	</div> -->
	<div class="container">
	    <div class="row">
			@foreach ($data as $key => $value)
				<div class="col-md-3 col-sm-3">
					<div class="l_g_r">
						<div class="dapibus">
							<h2 align="center">{{ $value->phuket_name }}</h2>
							@if ($value->img_name != '' && file_exists('uploads/phuket/100/' . $value->img_name))
                                <img src="{{ URL::asset('uploads/phuket/' . $value->img_name) }}" class="img-responsive">
                            @endif
                            <!-- <h4 style="color: #bbb;">{{ $value->phuket_name }}</h4> -->
							<p>{{ substr($value->detail,0,50) }}...</p>
							<a data-toggle="modal" data-target="#myModal{{$key}}" class="link" align="center">Read More</a>
						</div>
        				<div class="clearfix"> </div>
					</div><br>
				</div>
			@endforeach
	    </div>
	    <div class="row">
			<br>
			{!! $data->appends(Input::except('page'))->render() !!}
		</div>
	</div>
    <!-- /blog-pop-->
    @foreach ($data as $key2 => $value2)
    	<div class="modal fade bd-example-modal-lg" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div align="center">
	                        		@if ($value2->img_name != '' && file_exists('uploads/phuket/' . $value2->img_name))
	                                    <img src="{{ URL::asset('uploads/phuket/' . $value2->img_name) }}"  class="img-responsive">
	                                @endif
	                        	</div>
	                            
	                            <h4 class="tittle" align="center" >{{ $value2->phuket_name }}</h4>
								<p  align="center" style="color: #868f98;">Posted by Chattha on {{ $obj_fn->format_date_en($value2->updated_at,7) }}</p>
								<div style="word-break: break-all; text-align: left;">
									<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value2->detail }}</p>
								</div>
                            </div>
                        </div><br>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- //blog-pop-->
@endsection
@section('script')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/jquery.flexisel.js') !!}
    <script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo1").flexisel({
				visibleItems: 4,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems:2
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			
		});
	</script>
@endsection
