<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
$myLatLong = '7.975597747315679, 98.33209991455078';

?>

@extends('frontend.layout.main-layout')

@section('title', 'Rentel')
@section('rentel', 'class="active"')
@section('contact-us')
    action="{{ URL::to('rentel/contact')}}"
@endsection
@section('css')
  {!! Html::style('css/sale/benner.css') !!}
    {!! Html::style('css/sale/jquery-ui1.css') !!}
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
  	{!! Html::style('css/sale/home.css') !!}
  	{!! Html::style('css/sale/map-location.css') !!}
  	{!! Html::style('css/sale/property-slider.css') !!}
  	{!! Html::style('css/sale/image-alider.css') !!}
@endsection

@section('content')
	<!-- banner -->
    <div id="home" class="w3ls-banner">
        <!-- banner-text -->
        <div class="slider">
            <div class="callbacks_container">
                <ul class="rslides callbacks callbacks1" id="slider4">
                    <li>
                        <div class="w3layouts-banner-top">
                            <div class="agileits-banner-info">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3layouts-banner-top w3layouts-banner-top1">
                            <div class="agileits-banner-info">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3layouts-banner-top w3layouts-banner-top2">
                            <div class="agileits-banner-info">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3layouts-banner-top w3layouts-banner-top3">
                            <div class="agileits-banner-info">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>

            <!--banner Slider starts Here-->
        </div>
    </div>

	<div class="container"> <!-- container-fluid -->
		<div class="row" style="padding-bottom: 4px; padding-top: 20px;">
			<div class="col-md-4 col-sm-6">
				<div class="w3_banner_bottom_pos">
					<form action="{{ URL::to('/sale') }}" method="get" style="margin-bottom: 0px;">
            			<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<h4 style="color: #fff;" align="center"><b>Find a property </b></h4>
						<div>
							<select class="form-control" name="property_type">
	                            <option value="">Filter By Type</option>
	                            <option value="3" @if(Input::get('property_type') == 3) selected @endif>Apartment/Condo</option>
	                            <option value="2" @if(Input::get('property_type') == 2) selected @endif>House/villa</option>         
	                            <option value="1" @if(Input::get('property_type') == 1) selected @endif>Land</option>
	                        </select>
						</div><br>
						<div class="agile_book_section_top">
							<input type="text" name="property_id" value="{{ Input::get('property_id') }}" class="form-control" style="border: 0;" placeholder="Property By Id" />
						</div>
						<div class="clearfix"></div>
						<div class="wthree_range_slider">
							<h4 style="color: #fff;" align="center"><b>Room range</b></h4>
                            <div id="slider-range2"></div>  
                            <input type="text" name="room" value="{{ $room }}" id="room" placeholder="0 - 0 ROOM" style="border: 0;" />
						</div>
						<div class="wthree_range_slider">
							<h4 style="color: #fff;" align="center"><b>Price rentel</b></h4>
                            <div id="slider-range"></div>   
                            <input type="text" name="amount" value="{{ Input::get('amount') }}" id="amount" placeholder="0 - 500000 THB" style="border: 0;" />
						</div>

						<div class="newmap">
							<div class="newmapLocations">
								<div class="loc" style="height: 0px; position: absolute; left: 140px; top: 0px">
									<label class="PHA"><!-- <input type="checkbox" class="" name="chbLocation" value="1" @if(Input::get('chbLocation') == 1) checked @endif> -->Phang Nga</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 230px; top: 85px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="3" @if(Input::get('chbLocation') == 3) checked @endif>Ao Po</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 170px; top: 95px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="4" @if(Input::get('chbLocation') == 4) checked @endif>Thalang</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 185px; top: 115px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="6" @if(Input::get('chbLocation') == 6) checked @endif>Cherng Talay</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 240px; top: 135px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="7" @if(Input::get('chbLocation') == 7) checked @endif>Cape Yamu</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 200px; top: 150px">
									<label class="PHA"><div><input type="checkbox" class="" name="chbLocation" value="8" @if(Input::get('chbLocation') == 8) checked @endif>Koh Kaew</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 160px; top: 165px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="9" @if(Input::get('chbLocation') == 9) checked @endif>Kathu</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 225px; top: 185px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="10" @if(Input::get('chbLocation') == 10) checked @endif>Phuket Town</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 240px; top: 205px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="11" @if(Input::get('chbLocation') == 11) checked @endif>Koh Siray</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 235px; top: 240px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="12" @if(Input::get('chbLocation') == 12) checked @endif>Panwa Cape</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 165px; top: 255px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="13" @if(Input::get('chbLocation') == 13) checked @endif>Chalong</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 150px; top: 275px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="14" @if(Input::get('chbLocation') == 14) checked @endif>Rawai</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 75px; top: 275px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="15" @if(Input::get('chbLocation') == 15) checked @endif>Nai Harn</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 90px; top: 250px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="16" @if(Input::get('chbLocation') == 16) checked @endif>Kata</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 85px; top: 215px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="17" @if(Input::get('chbLocation') == 17) checked @endif>Karon</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 80px; top: 185px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="18" @if(Input::get('chbLocation') == 18) checked @endif>Patong</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 55px; top: 165px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="19" @if(Input::get('chbLocation') == 19) checked @endif>Kamala</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 75px; top: 145px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="20" @if(Input::get('chbLocation') == 20) checked @endif>Surin</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 70px; top: 125px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="21" @if(Input::get('chbLocation') == 21) checked @endif>Bangtao</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 80px; top: 105px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="22" @if(Input::get('chbLocation') == 22) checked @endif>Layan</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 65px; top: 85px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="23" @if(Input::get('chbLocation') == 23) checked @endif>Nai Thon</label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 70px; top: 55px">
									<label class="PHA"><input type="checkbox" class="" name="chbLocation" value="24" @if(Input::get('chbLocation') == 24) checked @endif>Nai Yang</label>
								</div>
							</div>
						</div><br>
						<input type="submit" value="Find Property">
					</form>
        			<div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-6">
				<div class="row">
					@foreach ($data as $key => $value)
						<div class="col-md-4">
							<div class="w3ls_services_grid agileits_services_grid">
								<div class="agile_services_grid1_sub">
									<p style="top: 5px; right: 5px; background: #c7040c; color: #fff; font-weight: 600;">฿ {{ number_format($value->low_month) }} per month</p>

									@if($value->promote == 2)
										<p  style="bottom: 20px; left: : 5px; background: #c7040c; color: #fff;"><b>HOT</b></p >
									@elseif($value->promote == 3)
										<p  style="bottom: 20px; left: : 5px; background: #66FF00; color: #000;">PRICE REDUCED</p >
									{{-- @elseif($value->promote == 4) --}}
										{{-- <p  style="bottom: 20px; left: : 5px; background: #FFDEAD; color: #000;">INVESMENT</p > --}}
									@elseif($value->promote == 5)
										<p  style="bottom: 20px; left: : 5px; background: #FFFF00; color: #000;">BARGAIN PRICE</p >
									@endif

									@if ($value->img_name != '' && file_exists('uploads/property/100/' . $value->img_name))
		                                <a data-toggle="modal" data-target="#myModal{{$key}}"><img src="{{ URL::asset('uploads/property/' . $value->img_name) }}" style="width: 100%"></a>
		                            @endif
								</div>
								<div class="agileinfo_services_grid_pos">
									<i class="fa fa-star-o fa-2x" aria-hidden="true"></i>
								</div>
							</div>
							<div class="wthree_service_text" style="background: #f5f5f5;">
								<a data-toggle="modal" data-target="#myModal{{$key}}"><h3>{{ substr($value->title,0,40) }}</h3></a>
								<h4 class="w3_agileits_service"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> 
									@foreach($location as $key2 =>$a_location)
										@if($value->location_id == $a_location->location_id)
											{{ $a_location->location_name }}
										@endif
									@endforeach
								</h4>
								<ul style="padding-left: 0px;">
									<li>
										<div class="compatibility-software" data-color="#b3b3b3" data-hover-color="#333333">
											{{ $value->bedrooms}} <img src="{{ URL::asset('images/icon/bedroom-01.png') }}" style="width: 20px;">
		                                    <ul class="compatibility-formats-wrap">
		                                        <li class="compatibility-format" align="center">
		                                            <div style="color: #c7040c;">{{ $value->bedrooms}}&nbsp;bedrooms</div>
		                                        </li>
		                                    </ul>
		                                </div>
									</li>
									<li>
										<div class="compatibility-software" data-color="#b3b3b3" data-hover-color="#333333">
											{{ $value->bathrooms}} <img src="{{ URL::asset('images/icon/bathroom-01.png') }}" style="width: 20px;">
		                                    <ul class="compatibility-formats-wrap">
		                                        <li class="compatibility-format" align="center">
		                                            <div style="color: #c7040c;">{{ $value->bathrooms}}&nbsp;bathrooms</div>
		                                        </li>
		                                    </ul>
		                                </div>
									</li>
									<li>
										<div class="compatibility-software" data-color="#b3b3b3" data-hover-color="#333333">
											@if($value->property_id == 1)
												&nbsp;<img src="{{ URL::asset('images/icon/land-01.png') }}" style="width: 20px;">
											@elseif($value->property_id == 2)
												&nbsp;<img src="{{ URL::asset('images/icon/villa-01.png') }}" style="width: 20px;">
											@elseif($value->property_id == 3)
												&nbsp;<img src="{{ URL::asset('images/icon/condo-01.png') }}" style="width: 20px;">
											@elseif($value->property_id == 6)
												&nbsp;<img src="{{ URL::asset('images/icon/business-01.png') }}" style="width: 20px;">
											@endif
		                                    <ul class="compatibility-formats-wrap">
		                                        <li class="compatibility-format" align="center">
			                                        	@foreach($property_type as $type)
															@if($value->property_id == $type->property_id)
																<div style="color: #c7040c;">{{ $type->property_name }}</div>
															@endif
														@endforeach
		                                        </li>
		                                    </ul>
		                                </div>
									</li>
									<!-- <li><img src="{{ URL::asset('images/icon/bathroom-01.png') }}" style="width: 20px;"></li> -->
									@if(!empty($value->view_id))
										<?php $a_viwe = explode(",", $value->view_id); ?>
										@foreach($a_viwe as $value2)
											<li>
												<div class="compatibility-software" data-color="#b3b3b3" data-hover-color="#333333">
											
													@if($value2 == 1)
														&nbsp;<img src="{{ URL::asset('images/icon/sae-01.png') }}" style="width: 20px;">
													@endif
													@if($value2 == 2)
														&nbsp;<img src="{{ URL::asset('images/icon/courtyard-01.png') }}" style="width: 20px;">
													@endif
													@if($value2 == 3)
														&nbsp;<img src="{{ URL::asset('images/icon/pool-02.png') }}" style="width: 20px;">
													@endif
													@if($value2 == 4)
														&nbsp;<img src="{{ URL::asset('images/icon/city-01.png') }}" style="width: 20px;">
													@endif
													@if($value2 == 5)
														&nbsp;<img src="{{ URL::asset('images/icon/mountain-01.png') }}" style="width: 20px;">
													@endif
													@if($value2 == 6)
														&nbsp;<img src="{{ URL::asset('images/icon/courtyard-01.png') }}" style="width: 20px;">
													@endif
													<ul class="compatibility-formats-wrap">
				                                        <li class="compatibility-format" align="center">
				                                            <div style="color: #c7040c;">
					                                        	@foreach($views as $a_views)
																	@if($value2 == $a_views->view_id)
																		{{ $a_views->view_name }}
																	@endif
																@endforeach
				                                            </div>
				                                        </li>
				                                    </ul>
												
		                                		</div>
											</li>
										@endforeach
									@endif
								</ul>
								<br>
								<a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>

							</div>
							
							<br>
						</div>
					@endforeach
				</div>
				<div class="row">
					<br>
					{!! $data->appends(Input::except('page'))->render() !!}
				</div>
			</div>
		</div>
	</div>

	@foreach ($data as $key2 => $value2)
		<div class="modal fade bd-example-modal-lg" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-lg">
	        	<div class="modal-content">
	        		<div class="modal-header">
	                    <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                </div>
	            	<div class="container-fluid">
		        		<div class="row">
		        			<div class="col-md-12" align="center">
                				<h4 class="tittle">{{ $value2->title }}</h4>
                			</div>
		        		</div>
		        		
		        		<div class="row">
		        			<?php 
		        				$a_img = DB::table('rent_images')
		        							->where('rent_id', $value2->rent_id)
		        							->orderby('sorting','desc')
		        							->get();
		        			?>
		        			<!-- @if(!empty($a_img))
		        				@foreach ($a_img as $a_images)
		        					<div class="col-md-4 col-sm-4">
                						@if ($a_images->img_name != '' && file_exists('uploads/rent_images/' . $a_images->img_name))
				                            <img src="{{ URL::asset('uploads/rent_images/' . $a_images->img_name) }}" class="img-responsive" alt=" ">
				                        @endif
				                        <br>
                					</div>

		        				@endforeach
		        			@endif
		        			@if(empty($a_img))
		        				<div class="col-md-12">
		        					@if ($value2->img_name != '' && file_exists('uploads/property/' . $value2->img_name))
			                            <img src="{{ URL::asset('uploads/property/' . $value2->img_name) }}" class="img-responsive" alt=" ">
			                        @endif
			                        <br>
		        				</div>
		        			@endif -->
                			<div class="clo-md-12">
							    @if(!empty($a_img))
								    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:580px;overflow:hidden;visibility:hidden;">
								        <!-- Loading Screen -->
								        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
								            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{ URL::asset('images/spin.svg') }}" />
								        </div>
								        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:480px;overflow:hidden;">
					        				@foreach ($a_img as $a_images)
		                						@if ($a_images->img_name != '' && file_exists('uploads/rent_images/' . $a_images->img_name))
		                							<div>
										                <img data-u="image" src="{{ URL::asset('uploads/rent_images/' . $a_images->img_name) }}" />
										                <img data-u="thumb" src="{{ URL::asset('uploads/rent_images/100/' . $a_images->img_name) }}" />
										            </div>
						                        @endif
					        				@endforeach
								        </div>
								        <!-- Thumbnail Navigator -->
								        <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
								            <div data-u="slides">
								                <div data-u="prototype" class="p" style="width:190px;height:90px;">
								                    <div data-u="thumbnailtemplate" class="t"></div>
								                    <svg viewbox="0 0 16000 16000" class="cv">
								                        <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
								                        <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
								                        <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
								                    </svg>
								                </div>
								            </div>
								        </div>
								        <!-- Arrow Navigator -->
								        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
								            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
								                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
								                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
								                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
								            </svg>
								        </div>
								        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
								            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
								                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
								                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
								                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
								            </svg>
								        </div>
								    </div>
					        	@endif
					        	@if(empty($a_img))
			        				<div class="col-md-12">
			        					@if ($value2->img_name != '' && file_exists('uploads/property/' . $value2->img_name))
				                            <img src="{{ URL::asset('uploads/property/' . $value2->img_name) }}" class="img-responsive" alt=" ">
				                        @endif
				                        <br>
			        				</div>
			        			@endif

                			</div>
		        		</div><br>
		        		<!-- <div class="row" align="center">
                			<div class="col-md-1 col-md-offset-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/beach-01.png') }}" style="width: 30px;">
                				<p>Beach Front Villa</p>
                			</div>
		        			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/bedroom-01.png') }}" style="width: 30px;">
                				<p>{{ $value2->bedrooms }} Bedrooms</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/bathroom-01.png') }}" style="width: 30px;">
                				<p>{{ $value2->bathrooms }} Bathrooms</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/Breakfast-01.png') }}" style="width: 30px;">
                				<p>Continental Breakfast</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/chef-01.png') }}" style="width: 30px;">
                				<p>Thai Chef</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/staff-01.png') }}" style="width: 30px;">
                				<p>In-Villa Staff</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/pool-01.png') }}" style="width: 30px;">
                				<p>8 meters Pool</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/Airport-01.png') }}" style="width: 30px;">
                				<p>Airport Transfert</p>
                			</div>
                			<div class="col-md-1 col-sm-4 col-xs-6">
                				<img src="{{ URL::asset('images/icon/wifi-01.png') }}" style="width: 30px;">
                				<p>Wi-Fi in the villa</p>
                			</div>
		        		</div> -->
		        		<div class="row">
		        			<div class="col-md-12">
		        				<div class="table-responsive">
		        					<table class="table table-hover">
		        						<thead>
		        							<tr>
		                        				<th class="text-center col-md-6 col-sm-6"></th>
		                        				<th class="text-center col-md-2 col-sm-2"><div style="text-align: center;">Monthly</div></th>
		                        				<th class="text-center col-md-2 col-sm-2"><div style="text-align: center;">Weekly</div></th>
		                        				<th class="text-center col-md-2 col-sm-2"><div style="text-align: center;">Daily</div></th>
		                        			</tr>
		        						</thead>
		        						<tbody>
		        							<tr>
		                        				<th>
		                        					<b>Low Season</b>
		                        					<p style="color: #666;">{{ $value2->low_from }} - {{ $value2->low_to }}</p>
		                        					<p style="color: #666;">{{ $value2->low1_from }} - {{ $value2->low1_to }}</p>
		                        				</th>
		                        				<th><br>
		                        					<p style="text-align: center; background: #448ABB; color: #fff">฿ {{ number_format($value2->low_month) }}</p>
		                        					<p style="text-align: center; background: #448ABB; color: #fff">฿ {{ number_format($value2->low1_month) }}</p>
		                        				</th>
		                        				<th><br>
		                        					<p style="text-align: center; background: #73a7cc; color: #fff">฿ {{ number_format($value2->low_weekly) }}</p>
		                        					<p style="text-align: center; background: #73a7cc; color: #fff">฿ {{ number_format($value2->low1_weekly) }}</p>
		                        				</th>
		                        				<th><br>
		                        					<p style="text-align: center; background: #a2c5dd; color: #fff">฿ {{ number_format($value2->low_daily) }}</p>
		                        					<p style="text-align: center; background: #a2c5dd; color: #fff">฿ {{ number_format($value2->low1_daily) }}</p>
		                        				</th>
		                        			</tr>
		                        			<tr>
		                        				<th>
		                        					<b>High Season</b>
		                        					<p style="color: #666;">{{ $value2->high_from }} - {{ $value2->high_to }}</p>
		                        					<p style="color: #666;">{{ $value2->high1_from }} - {{ $value2->high1_to }}</p>
		                        				</th>
		                        				<th><br>
		                        					<p style="text-align: center; background: #ac6e1d; color: #fff">฿ {{ number_format($value2->high_month) }}</p>
		                        					<p style="text-align: center; background: #ac6e1d; color: #fff">฿ {{ number_format($value2->high1_month) }}</p>
		                        				</th>
		                        				<th><br>
		                        					<p style="text-align: center; background: #dc902d; color: #fff">฿ {{ number_format($value2->high_weekly) }}</p>
		                        					<p style="text-align: center; background: #dc902d; color: #fff">฿ {{ number_format($value2->high1_weekly) }}</p>
		                        				</th>
		                        				<th><br>
		                        					<p style="text-align: center; background: #e5ad64; color: #fff">฿ {{ number_format($value2->high_daily) }}</p>
		                        					<p style="text-align: center; background: #e5ad64; color: #fff">฿ {{ number_format($value2->high1_daily) }}</p>
		                        				</th>
		                        			</tr>
		                        			<tr>
		                        				<th><b>Peak Season</b><p style="color: #666;">{{ $value2->peak_from }} - {{ $value2->peak_to }}</p></th>
		                        				<th><br><p style="text-align: center; background: #9C292B; color: #fff">฿ {{ number_format($value2->peak_month) }}</p></th>
		                        				<th><br><p style="text-align: center; background: #cb3a3c; color: #fff">฿ {{ number_format($value2->peak_weekly) }}</p></th>
		                        				<th><br><p style="text-align: center; background: #d86c6e; color: #fff">฿ {{ number_format($value2->peak_daily) }}</p></th>
		                        			</tr>
		        						</tbody>
		                        	</table><br>
		        				</div>
		        				
		        			</div>
		        		</div>
		        		<div class="row">
                			<div class="col-md-6">
                				<table class="table table-striped table-hover">
                            		<tbody>
                            			<tr>
                            				<td class="col-md-6"-><b>Location</b></td>
                            				<td class="col-md-6"->
                            					@foreach($location as $key2 =>$a_location)
													@if($value2->location_id == $a_location->location_id)
														{{ $a_location->location_name }}
													@endif
												@endforeach
                            				</td>
                            			</tr>
                            			<tr>
                            				<td><b>Bedrooms</b></td>
                            				<td>{{ $value2->bedrooms}}&nbsp;<img src="{{ URL::asset('images/icon/bedroom-01.png') }}" style="width: 20px;"></td>
                            			</tr>
                            			<tr>
                            				<td><b>Bathrooms</b></td>
                            				<td>{{ $value2->bathrooms}}&nbsp;<img src="{{ URL::asset('images/icon/bathroom-01.png') }}" style="width: 20px;"></td>
                            			</tr>
                            			<tr>
                            				<td><b>View</b></td>
                            				<td>
                            					@if(!empty($value2->view_id))
													<?php $a_viwe = explode(",", $value2->view_id); ?>
													@foreach($a_viwe as $value3)
														@foreach($views as $a_views)
															@if($value3 == $a_views->view_id)
																{{ $a_views->view_name }}
															@endif
														@endforeach
														@if($value3 == 1)
															&nbsp;<img src="{{ URL::asset('images/icon/sae-01.png') }}" style="width: 20px;">
														@endif
														@if($value3 == 2)
															&nbsp;<img src="{{ URL::asset('images/icon/courtyard-01.png') }}" style="width: 20px;">
														@endif
														@if($value3 == 3)
															&nbsp;<img src="{{ URL::asset('images/icon/pool-02.png') }}" style="width: 20px;">
														@endif
														@if($value3 == 4)
															&nbsp;<img src="{{ URL::asset('images/icon/city-01.png') }}" style="width: 20px;">
														@endif
														@if($value3 == 5)
															&nbsp;<img src="{{ URL::asset('images/icon/mountain-01.png') }}" style="width: 20px;">
														@endif
														@if($value3 == 6)
															&nbsp;<img src="{{ URL::asset('images/icon/courtyard-01.png') }}" style="width: 20px;">
														@endif
														<br>
													@endforeach
												@endif
                            				</td>
                            			</tr>
                            			<tr>
                            				<td><b>Type</b></td>
                            				<td>
	                            				@if($value2->property_id == 1)
													land <img src="{{ URL::asset('images/icon/land-01.png') }}" style="width: 20px;">
												@elseif($value2->property_id == 2)
													villa <img src="{{ URL::asset('images/icon/villa-01.png') }}" style="width: 20px;">
												@elseif($value2->property_id == 3)
													condo <img src="{{ URL::asset('images/icon/condo-01.png') }}" style="width: 20px;">
												@elseif($value2->property_id == 6)
													business <img src="{{ URL::asset('images/icon/business-01.png') }}" style="width: 20px;">
												@endif
											</td>
                            			</tr>
                            			<!-- <tr>
                            				<td><b>Parking</b></td>
                            				<td>{{ $value2->location_id}}</td>
                            			</tr>
                            			<tr>
                            				<td><b>Swimming Pool</b></td>
                            				<td>{{ $value2->location_id}}</td>
                            			</tr> 
                            			<tr>
                            				<td><b>Building Area</b></td>
                            				<td>{{ $value2->location_id}}</td>
                            			</tr> -->
                            			<tr>
                            				<td><b>Land Area</b></td>
                            				<td>{{ $value2->land_size}} m²</td>
                            			</tr>
                            		</tbody>
                            	</table>
                			</div>
                			<div class="col-md-6">
                				<?php $description = explode("<br />",$value2->description);?>
                        		@foreach ($description as $a_description)
                    				<p>{{ $a_description }}</p>
                    			@endforeach
                			</div>
                		</div><br>
		        		<!-- <div class="row">
		        			<div class="col-md-12">
		        				<?php $description = explode("<br />",$value2->description);?>
                        		@foreach ($description as $a_description)
                    				<p>{{ $a_description }}</p>
                    			@endforeach
                			</div>
		        		</div> -->
		        		<div class="row">
		        			<div class="col-md-4 col-sm-4" align="">
		        				<div><h4><b>
                    				@foreach($property_type as $type)
                                        @if($value2->property_id == $type->property_id)
                                            {{ $type->property_name }}
                                        @endif
                                    @endforeach
                    			</b></h4></div>
                    			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; {{ $value2->bedrooms }} Bedrooms</div>
                    			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; {{ $value2->bathrooms }} Bathrooms</div>
                    			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; Floors : {{ $value2->floors }} </div>
                    			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; Indoor Area: {{ $value2->indoor_area }} m²</div>
                    			<?php $a_living = explode(",",$value2->living_id);?>
                    			@foreach ($a_living as $a_id)
                    				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
                    					@foreach($living as $n_living)
                                            @if($a_id == $n_living->living_id)
                                                {{ $n_living->living_name }}
                                            @endif
                                        @endforeach
                    				</div>
                    			@endforeach
                			</div>
                			<div class="col-md-4 col-sm-4" align="">
                				<h4><b>Features</b></h4>
                    			<?php $a_features = explode(",",$value2->features_id);?>
                    			@foreach ($a_features as $a_id)
                    				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
                    					@foreach($features as $a_name)
                                            @if($a_id == $a_name->features_id)
                                                {{ $a_name->features_name }}
                                            @endif
                                        @endforeach
                    				</div>
                    			@endforeach
                			</div>
                			<div class="col-md-4 col-sm-4" align="">
                    			<h4><b>Community</b></h4>
                    			<?php $a_community = explode(",",$value2->community_id);?>
                    			@foreach ($a_community as $a_com)
                    				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
                    					@foreach($community as $a_communites)
                                            @if($a_com == $a_communites->community_id)
                                                {{ $a_communites->community_name }}
                                            @endif
                                        @endforeach
                    				</div>
                    			@endforeach
                			</div>
		        		</div><br>
	                    <!-- ******************************* Map ***************************** -->
		        		<div class="row">
		        			<div class="col-md-12">
		        				<div class="form-group">
	                            	<?php $myLatLong = $value2->latitude.', '.$value2->longitude;?>
	                                <div id="dvMap" style="width: 100%; height: 500px"></div>
	                            </div>
	                            <div class="form-group">
	                                <input type="hidden" class="form-control" name="latitude" value="{{ $value2->latitude }}" id="latitude">
	                                <input type="hidden" class="form-control" name="longitude" value="{{ $value2->longitude }}" id="longitude">
		                        </div>
                			</div>
		        		</div>
		        		<br>
		        	</div>
		        	<div class="container-fluid">
		        		<div class="row">
		        			<div class="col-md-12">
		        				<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" align="center">
        							<h3><b>Check availability INQUIRY</b></h3>
		        				</div>
	                        	<form action="{{ url()->to($path.'/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-body">
                                        <div class="form-group">
                                        	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                            	<input type="text" class="form-control" name="name" value="" placeholder="name" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                            	<input type="email" class="form-control" name="email" value="" placeholder="Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                            	<input type="text" class="form-control" name="phone" value="" placeholder="Phone number (Please use international format)" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                            	<textarea name="msg" id="editor1" class="form-control " rows="5" placeholder="Request"></textarea>
                                            </div>
                                        </div> 
                                        <div class="form-actions">
                                        	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" align="center">
                                            	<input type='hidden' name="rent_id" id="rent_id" class="form-control" value="{{ $value2->rent_id }}">
                                            	<button type="submit" class="btn" style="background: #c7040c; color: #fff;"><h3><b>Send</b></h3></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                			</div>
		        		</div>
		        	</div><br>
		        </div>
	        </div>
	    </div>
    @endforeach
@endsection
@section('page-plugin')  
    {!! Html::script('js/sale/jssor.slider-26.3.0.min.js') !!}
    {!! Html::script('js/rentel/jquery-ui.js') !!}
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
@endsection
@section('script')
    {!! Html::script('js/frontend/fwslider.js') !!}
    {!! Html::script('js/sale/responsiveslides.min.js') !!}
    {!! Html::script('js/frontend/jquery.flexslider.js') !!}
    {!! Html::script('js/frontend/jquery.flexisel.js') !!}
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('API_KEY') }}"></script>
    {{ Html::script('js/backend/map.js') }}
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>

	<script>
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ $myLatLong }});
            var myOptions = {
                zoom: 15,
                center: myLatlng,
            };
            map = new google.maps.Map(document.getElementById('dvMap'), myOptions);
            // This event listener will call addMarker() when the map is clicked.
            google.maps.event.addListener(map, 'click', function(event) {
                addMarker(event.latLng);
                saveData(map, event);
            });
            // Adds a marker at the center of the map.
            addMarker(myLatlng);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {
            var jssor_1_SlideshowTransitions = [
              {$Duration:800,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $Align: 0,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 5,
                $SpacingX: 5,
                $SpacingY: 5,
                $Align: 390
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
	<script type="text/javascript">jssor_1_slider_init();</script>
    <script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo1").flexisel({
				visibleItems: 3,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems:2
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			
		});
	</script>
    
    <script type='text/javascript'>//<![CDATA[ 
		$(window).load(function(){
		 	$( "#slider-range" ).slider({
				range: true,
				min: 0,
				max: 500000,
				// values: [ 500000, 600000 ],
				slide: function( event, ui ) {  $( "#amount" ).val(  ui.values[ 0 ] + " - " + ui.values[ 1 ] + " THB ");
				}
		 	});
			// $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + " - " + $( "#slider-range" ).slider( "values", 1 ) + " THB" );

		});//]]>  
	</script>
	<script type='text/javascript'>//<![CDATA[ 
		$(window).load(function(){
		 	$( "#slider-range2" ).slider2({
				range: true,
				min: 0,
				max: 11,
				// values: [ 0, 1 ],
				slide: function( event, ui ) {  $( "#room" ).val(  ui.values[ 0 ] + " - " + ui.values[ 1 ] + " ROOM ");
				}
		 	});
			// $( "#room" ).val( $( "#slider-range2" ).slider2( "values", 0 ) + " - " + $( "#slider-range2" ).slider2( "values", 1 ) + " ROOM" );

		});//]]>  
	</script>
@endsection
