@extends('frontend.layout.main-layout')

@section('title', 'Vehicle')
@section('vehicle', 'class="active"')
@section('contact-us')
    action="{{ URL::to('vehicle/contact')}}"
@endsection
@section('css')
  {!! Html::style('css/frontend/vehicle.css') !!}
  {!! Html::style('css/frontend/flexslider.css') !!}
{{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
  <!-- {!! Html::style('css/frontend/yacht.css') !!} -->
@endsection

@section('content')
    <div class="featured_section_w3l">
        <div class="container">
            <div class="inner_tabs">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">cars</a></li>
                        <li role="presentation"><a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Motorbike</a></li>
                        <li role="presentation"><a href="#tree" role="tab" id="tree-tab" data-toggle="tab" aria-controls="tree">Yacht</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
                            <div class="section__content clearfix">
                                <div class="container">
                                    <div class="row">
                                            @foreach($data as $key => $field1)
                                                @if( $field1->type == 1)
                                        <!-- <div class="col-md-3"> -->
                                                    <div class="card effect__hover">
                                                        <div class="card__front">
                                                            <span class="card__text">
                                                                <div class="img-grid">
                                                                    @if ($field1->img_name != '' && file_exists('uploads/vehicle/' . $field1->img_name))
                                                                        <img src="{{ URL::asset('uploads/vehicle/' . $field1->img_name) }}" class="img-responsive">
                                                                    @endif
                                                                    <div class="car_description">
                                                                        <h4><a>{{ $field1->name }}</a></h4>
                                                                        <p align="left"><biv class="date">Low Season : {{ number_format($field1->low_price) }} ฿</biv> (per day)</p>
                                                                        <p align="left">{{ $field1->low_from }} - {{ $field1->low_to }}</p>
                                                                        <p align="left"><biv class="date">High Season : {{ number_format($field1->high_pice) }} ฿</biv> (per day)</p>
                                                                        <p align="left">{{ $field1->high_from }} - {{ $field1->high_to }} </p>
                                                                        <p align="left"><biv class="date">Engine : </biv>{{ $field1->engine }}</p>
                                                                        <p align="left"><biv class="date">Seat Capac : </biv>{{ $field1->seat_capac }}</p>
                                                                        <p align="left"><biv class="date">Auto/Manual : </biv>
                                                                            @if($field1->auto_manual == 1) 
                                                                                A/T
                                                                            @elseif($field1->auto_manual == 2)
                                                                                Manual
                                                                            @else
                                                                                Auto / Manual
                                                                            @endif
                                                                        </p><!-- <br><br><br><br><br><br><br><br><br><br> -->
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </div>
                                                        <div class="card__back">
                                                            <span class="card__text">
                                                                <div class="login-inner2">
                                                                    <h4>Select your Vehicle</h4>
                                                                    <div class="login-top sign-top">
                                                                        <form action="{{ url()->to($path.'/store') }}" method="POST" enctype="multipart/form-data">
                                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                            <input type="hidden" name="vehicle_id" value="{{ $field1->vehicle_id }}">
                                                                            <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                                            <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                                            <input type="text" name="phone" class="phone" placeholder="Tel" required=""/>
                                                                            <input type="text" name="location" class="location" placeholder="Location" required=""/>
                                                                            <!-- <div class="input-group input-large input-daterange">
                                                                                <input type="text" class="dt date date-time-picker date-set" name="date_from" placeholder="From Date" readonly>
                                                                                <span class="input-group-addon">to</span>
                                                                                <input type="text" class="dt date date-time-picker date-set" name="date_to" placeholder="To Date"  readonly>
                                                                            </div> -->
                                                                            <input type="text" class="dt date date-time-picker date-set" name="date_from" placeholder="From Date" readonly required="">
                                                                            <input type="text" class="dt date date-time-picker date-set" name="date_to" placeholder="To Date"  readonly required="">
                                                                            <input type="text" name="msg" class="comment" placeholder="Comment">
                                                                            <input type="submit" value="Book">   
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                        <!-- </div> -->
                                                @endif
                                            @endforeach
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
                            <div class="section__content clearfix">
                                @foreach($data as $key => $field2)
                                    @if( $field2->type == 2)
                                        <div class="card effect__hover">
                                            <div class="card__front">
                                                <span class="card__text">
                                                    <div class="img-grid">
                                                        @if ($field2->img_name != '' && file_exists('uploads/vehicle/' . $field2->img_name))
                                                            <img src="{{ URL::asset('uploads/vehicle/' . $field2->img_name) }}" class="img-responsive">
                                                        @endif
                                                        <div class="car_description">
                                                            <h4><a>{{ $field2->name }}</a></h4>
                                                            <p align="left"><biv class="date">Low Season : {{ number_format($field2->low_price) }} ฿</biv> (per day)</p>
                                                            <p align="left">{{ $field2->low_from }} - {{ $field2->low_to }}</p>
                                                            <p align="left"><biv class="date">High Season : {{ number_format($field2->high_pice) }} ฿</biv> (per day)</p>
                                                            <p align="left">{{ $field2->high_from }} - {{ $field2->high_to }} </p>
                                                            <p align="left"><biv class="date">Engine : </biv>{{ $field2->engine }}</p>
                                                            <p align="left"><biv class="date">Seat Capac : </biv>{{ $field2->seat_capac }}</p>
                                                            <p align="left"><biv class="date">Auto/Manual : </biv>
                                                                @if($field2->auto_manual == 1) 
                                                                    A/T
                                                                @elseif($field2->auto_manual == 2)
                                                                    Manual
                                                                @else
                                                                    Auto / Manual
                                                                @endif
                                                            </p><!-- <br><br><br><br><br><br><br><br><br><br> -->
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="card__back">
                                                <span class="card__text">
                                                    <div class="login-inner2">
                                                        <h4>Select your Vehicle</h4>
                                                        <div class="login-top sign-top">
                                                            <form action="{{ url()->to($path.'/store') }}" method="POST" enctype="multipart/form-data">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="hidden" name="vehicle_id" value="{{ $field2->vehicle_id }}">
                                                                <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                                <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                                <input type="text" name="phone" class="phone" placeholder="Tel" required=""/>
                                                                <input type="text" name="location" class="location" placeholder="Location" required=""/>
                                                                <input type="text" class="dt date date-time-picker date-set" name="date_from" placeholder="From Date" readonly>
                                                                <input type="text" class="dt date date-time-picker date-set" name="date_to" placeholder="To Date"  readonly>
                                                                <input type="text" name="msg" class="comment" placeholder="Comment"/>
                                                                <input type="submit" value="Book">   
                                                            </form>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>  
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tree" aria-labelledby="tree-tab">
                            <div class="section__content clearfix">
                                @foreach($data as $key => $field3)
                                    @if( $field3->type == 3)
                                        <div class="card effect__hover">
                                            <div class="card__front">
                                                <span class="card__text">
                                                    <div class="img-grid">
                                                        @if ($field3->img_name != '' && file_exists('uploads/vehicle/' . $field3->img_name))
                                                            <img src="{{ URL::asset('uploads/vehicle/' . $field3->img_name) }}" class="img-responsive">
                                                        @endif
                                                        <div class="car_description">
                                                            <h4><a>{{ $field3->name }}</a></h4>
                                                            <p align="left"><biv class="date">Low Season : {{ number_format($field3->low_price) }} ฿</biv> (per day)</p>
                                                            <p align="left">{{ $field3->low_from }} - {{ $field3->low_to }}</p>
                                                            <p align="left"><biv class="date">High Season : {{ number_format($field3->high_pice) }} ฿</biv> (per day)</p>
                                                            <p align="left">{{ $field3->high_from }} - {{ $field3->high_to }} </p>
                                                            <p align="left"><biv class="date">Passenger Capacity : </biv>{{ $field3->passenger }} (ft.)</p>
                                                            <p align="left"><biv class="date">Overnight Capacity : </biv>{{ $field3->overnight }}</p>
                                                            <p align="left"><biv class="date">Crew : </biv>{{ $field3->crew }}</p>
                                                            <p align="left"><biv class="date">Cabin Number : </biv>{{ $field3->cabin_no }}</p>
                                                            <p align="left"><biv class="date">Cruising Speed : </biv>{{ $field3->cruising_speed }} (Kts.)</p>
                                                            <!-- <br><br><br><br><br><br><br><br><br><br> -->
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="card__back">
                                                <span class="card__text">
                                                    <div class="login-inner2">
                                                        <h4>Select your Vehicle</h4>
                                                        <div class="login-top sign-top">
                                                            <form action="{{ url()->to($path.'/store') }}" method="POST" enctype="multipart/form-data">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="hidden" name="vehicle_id" value="{{ $field3->vehicle_id }}">
                                                                <input type="text" name="name" class="name active" placeholder="Name" required=""/>
                                                                <input type="text" name="email" class="email" placeholder="Email" required=""/>
                                                                <input type="text" name="phone" class="phone" placeholder="Tel" required=""/>
                                                                <input type="text" name="location" class="location" placeholder="Location" required=""/>
                                                                <input type="text" class="dt date date-time-picker date-set" name="date_from" placeholder="From Date" readonly>
                                                                <input type="text" class="dt date date-time-picker date-set" name="date_to" placeholder="To Date"  readonly>
                                                                <input type="text" name="msg" class="comment" placeholder="Comment"/>
                                                                <input type="submit" value="Book">   
                                                            </form>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!--//featured_section-->

    <br><br><br><br><br>

@endsection
@section('page-plugin')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}    
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
@endsection
@section('script')
    {{ Html::script('js/backend/validation.js') }}
    <script>
        $(".date-time-picker").datetimepicker({
            autoclose: true,
            isRTL: Metronic.isRTL(),
            format: "dd MM yyyy - hh:ii",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
        });
    </script>
@endsection
