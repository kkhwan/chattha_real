@extends('frontend.layout.main-layout')

@section('title', 'Yacht')
@section('yacht', 'class="active"')

@section('css')
  {!! Html::style('css/frontend/flexslider.css') !!}
  {!! Html::style('css/frontend/yacht.css') !!}
@endsection

@section('content')
    <div class="trending-ads">
        <div class="container">
            <!-- slider -->
            <div class="agile-trend-ads">
                <h2>YACHT HIRE</h2>
                <ul id="flexiselDemo3">
                    <li>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="" />
                                <span class="price">&#36; 450</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>There are many variations of passages</h5>
                                <span>1 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="" />
                                <span class="price">&#36; 399</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>Lorem Ipsum is simply dummy</h5>
                                <span>3 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="" />
                                <span class="price">&#36; 199</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>It is a long established fact that a reader</h5>
                                <span>8 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/hire/Toyota-Yaris-2016.jpg" alt="" />
                                <span class="price">&#36; 159</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>passage of Lorem Ipsum you need to be</h5>
                                <span>19 hour ago</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p5.jpg" alt="" />
                                <span class="price">&#36; 1599</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>There are many variations of passages</h5>
                                <span>1 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p6.jpg" alt="" />
                                <span class="price">&#36; 1099</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>passage of Lorem Ipsum you need to be</h5>
                                <span>1 day ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p7.jpg" alt="" />
                                <span class="price">&#36; 109</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>It is a long established fact that a reader</h5>
                                <span>9 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p8.jpg" alt="" />
                                <span class="price">&#36; 189</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>Lorem Ipsum is simply dummy</h5>
                                <span>3 hour ago</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p9.jpg" alt="" />
                                <span class="price">&#36; 2599</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>Lorem Ipsum is simply dummy</h5>
                                <span>3 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p10.jpg" alt="" />
                                <span class="price">&#36; 3999</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>It is a long established fact that a reader</h5>
                                <span>9 hour ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p11.jpg" alt="" />
                                <span class="price">&#36; 2699</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>passage of Lorem Ipsum you need to be</h5>
                                <span>1 day ago</span>
                            </div>
                        </div>
                        <div class="col-md-3 biseller-column">
                            <a href="#">
                                <img src="images/test/p12.jpg" alt="" />
                                <span class="price">&#36; 899</span>
                            </a> 
                            <div class="w3-ad-info">
                                <h5>There are many variations of passages</h5>
                                <span>1 hour ago</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>   
        </div>
        <!-- //slider -->               
    </div>
@endsection
@section('script')
    {!! Html::script('js/frontend/jquery-ui.min.js') !!}
    {!! Html::script('js/frontend/jquery.flexisel.js') !!}
    <script type="text/javascript">
         $(window).load(function() {
            $("#flexiselDemo3").flexisel({
                visibleItems:1,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 5000,            
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: { 
                    portrait: { 
                        changePoint:480,
                        visibleItems:1
                    }, 
                    landscape: { 
                        changePoint:640,
                        visibleItems:1
                    },
                    tablet: { 
                        changePoint:768,
                        visibleItems:1
                    }
                }
            });
             
        });
    </script>
@endsection
<!--  -->