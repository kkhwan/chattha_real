<?php 
$myLatLong = '7.975597747315679, 98.33209991455078'; 
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
?>
@extends('sale.layout.main-layout')

@section('title', 'Sales')
@section('sale', 'class="active"')

@section('css')
  {!! Html::style('css/sale/home.css') !!}
  {!! Html::style('css/sale/map-location.css') !!}
  {!! Html::style('css/sale/property-slider.css') !!}
@endsection

@section('content')

	<div class="container"> <!-- container-fluid -->
		<div class="row" style="padding-bottom: 4px; padding-top: 20px;">
			<div class="col-md-4 col-sm-6">
				<div class="w3_banner_bottom_pos">
					<form action="{{ URL::to('/sale') }}" method="get" style="margin-bottom: 0px;">
            			<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<h4 style="color: #fff;" align="center"><b>Find a property </b></h4>
						<div>
							<select class="form-control" name="property_type">
	                            <option value="">Filter By Type</option>
	                            <option value="3" @if(Input::get('property_type') == 3) selected @endif>Apartment/Condo</option>
	                            <option value="2" @if(Input::get('property_type') == 2) selected @endif>House/villa</option>         
	                            <option value="1" @if(Input::get('property_type') == 1) selected @endif>Land</option>
	                        </select>
						</div><br>
						<div class="agile_book_section_top">
							<input type="text" name="property_id" value="{{ Input::get('property_id') }}" class="form-control" style="border: 0;" placeholder="Property By Id" />
						</div>
						<div class="clearfix"></div>
						<div class="wthree_range_slider">
							<h4 style="color: #fff;"><b>Room range</b></h4>
                            <div id="slider-range2"></div>
                            <input type="text" name="room" value="{{ $room }}" id="room" placeholder="0 - 1 ROOM" style="border: 0;" />
							<div class="clearfix"> </div>
						</div>
						<div class="wthree_range_slider">
							<h4 style="color: #fff;"><b>Price sale</b></h4>
							<div id="slider-range"></div>
							<input type="text" name="amount" value="{{ Input::get('amount') }}" id="amount" placeholder="1000000 - 10000000 THB" style="border: 0;" />
							<div class="clearfix"> </div>
						</div>
						<div class="newmap">
							<div class="newmapLocations">
								<div class="loc" style="height: 0px; position: absolute; left: 140px; top: 0px">
									<label class="PHA"><div><input title="Phang Nga" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="1">Phang Nga</div></label>
								</div>
								<!-- <div class="loc" style="height: 0px; position: absolute; left: 135px; top: 4px">
									<label class="PHA"><div><input title="Mai Khao" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="2">Mai Khao</div></label>
								</div> -->
								<div class="loc" style="height: 0px; position: absolute; left: 230px; top: 85px">
									<label class="PHA"><div><input title="Ao Pro" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="3">Ao Pro</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 170px; top: 95px">
									<label class="PHA"><div><input title="Talang" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="4">Talang</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 185px; top: 115px">
									<label class="PHA"><div><input title="Cherng Talay" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="6">Cherng Talay</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 240px; top: 135px">
									<label class="PHA"><div><input title="Cape Yamu" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="7">Cape Yamu</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 200px; top: 150px">
									<label class="PHA"><div><input title="Koh Kaew" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="8">Koh Kaew</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 160px; top: 165px">
									<label class="PHA"><div><input title="Kathu" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="9">Kathu</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 225px; top: 185px">
									<label class="PHA"><div><input title="Phuket Town" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="10">Phuket Town</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 240px; top: 205px">
									<label class="PHA"><div><input title="Kho Siray Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="11">Kho Siray</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 235px; top: 240px">
									<label class="PHA"><div><input title="Panwa Cape" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="12">Panwa Cape</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 165px; top: 255px">
									<label class="PHA"><div><input title="Chalong Bay" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="13">Chalong</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 150px; top: 275px">
									<label class="PHA"><div><input title="Rawai Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="14">Rawai</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 75px; top: 275px">
									<label class="PHA"><div><input title="Nai Harn Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="15">Nai Harn</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 90px; top: 250px">
									<label class="PHA"><div><input title="Kata Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="16">Kata</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 85px; top: 215px">
									<label class="PHA"><div><input title="Karon Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="17">Karon</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 80px; top: 185px">
									<label class="PHA"><div><input title="Patong Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="18">Patong</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 55px; top: 165px">
									<label class="PHA"><div><input title="Kamala Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="19">Kamala</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 75px; top: 145px">
									<label class="PHA"><div><input title="Surin Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="20">Surin</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 70px; top: 125px">
									<label class="PHA"><div><input title="Bangtao Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="21">Bangtao</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 80px; top: 105px">
									<label class="PHA"><div><input title="Layan Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="22">Layan</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 65px; top: 85px">
									<label class="PHA"><div><input title="Nai Thon Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="23">Nai Thon</div></label>
								</div>
								<div class="loc" style="height: 0px; position: absolute; left: 70px; top: 55px">
									<label class="PHA"><div><input title="Nai Yang Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="24">Nai Yang</div></label>
								</div>
								<!-- <div class="loc" style="height: 0px; position: absolute; left: 70px; top: 50px">
									<label class="PHA"><div><input title="Mai Khao Beach" type="checkbox" class="map-checkbox area-PHA" name="location[]" value="25">Mai Khao</div></label>
								</div> -->
							</div>
						</div><br>
						<input type="submit" value="Find Property">
					</form>
					<div class="clearfix"> </div>
					
				</div><br>
			</div>
			<div class="col-md-8 col-sm-6">
				<div class="row">
					@foreach ($data as $key => $value)
						<div class="col-md-4">
							<div class="w3ls_services_grid agileits_services_grid">
								<div class="agile_services_grid1_sub">
									<p>฿ {{ $value->price }}</p>
									@if ($value->img_name != '' && file_exists('uploads/property/100/' . $value->img_name))
		                                <img src="{{ URL::asset('uploads/property/' . $value->img_name) }}" style="width: 100%">
		                            @endif
								</div>
								<div class="agileinfo_services_grid_pos">
									<i class="fa fa-star-o fa-2x" aria-hidden="true"></i>
								</div>
							</div>
							<div class="wthree_service_text" style="background: #f5f5f5;">
								<h3>{{ substr($value->title,0,15) }}...</h3>
								<h4 class="w3_agileits_service">{{ substr($value->description,0,50) }}...</h4>
								<a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>
							</div>
							<br>
						</div>
					@endforeach
				</div>
				<div class="row">
					<br>
					{!! $data->appends(Input::except('page'))->render() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
@section('from-content')
	@foreach ($data as $key2 => $value2)
    	<div class="modal ab fade" id="myModal{{ $key2 }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	        <div class="modal-dialog about" role="document">
	            <div class="modal-content about">
	                <div class="modal-header">
	                    <button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                </div>
	                <div class="modal-body">
	                    <div class="about">

	                        <div class="about-inner">
	                        	<!-- <div class="container">
	                        		<h4 class="tittle">{{ $value2->title }}</h4>
	                        		<div class="row">
	                        			<div class="col-md-4 col-sm-4 col-xs-4">
	                        				<img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive">
	                        			</div>
	                        			<div class="col-md-4 col-sm-4 col-xs-4">
	                        				<img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive">
	                        			</div>
	                        			<div class="col-md-4 col-sm-4 col-xs-4">
	                        				<img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive">
	                        			</div>
	                        		</div>
	                        		<div class="row">
	                        			<div class="col-md-4 col-sm-4 col-xs-4">
	                        				<img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive">
	                        			</div>
	                        			<div class="col-md-4 col-sm-4 col-xs-4">
	                        				<img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive">
	                        			</div>
	                        			<div class="col-md-4 col-sm-4 col-xs-4">
	                        				<img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive">
	                        			</div>
	                        		</div>
	                        		<div class="row">
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/beach-01.png') }}" class="img-responsive">
	                        				<div>Beach Front Villa</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/bedroom-01.png') }}" class="img-responsive">
	                        				<div>{{ $value2->bedrooms }} Bedrooms</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/bathroom-01.png') }}" class="img-responsive">
	                        				<div>Bathrooms</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/Breakfast-01.png') }}" class="img-responsive">
	                        				<div>Continental Breakfast</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/chef-01.png') }}" class="img-responsive">
	                        				<div>Thai Chef</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/staff-01.png') }}" class="img-responsive">
	                        				<div>In-Villa Staff</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/pool-01.png') }}" class="img-responsive">
	                        				<div>8 meters Pool</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/Airport-01.png') }}" class="img-responsive">
	                        				<div>Airport Transfert</div>
	                        			</div>
	                        			<div class="col-md-1 col-sm-1 col-xs-1">
	                        				<img src="{{ URL::asset('images/icon/wifi-01.png') }}" class="img-responsive">
	                        				<div>Wi-Fi in the villa</div>
	                        			</div>
	                        		</div>
	                        	</div> -->
	                            <h4 class="tittle">{{ $value2->title }}</h4>
	                        	<div style="color: #c7040c;" align="right" ><h5><b>Sale :</b> {{ $value2->price }} ฿ </h5></div>
	                            <table style="width: 100%">
                        			<tr>
                        				<th class="t-img3"><div ><img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive"></div></th>
                        				<th class="t-img3"><div><img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive"></div></th>
                        				<th class="t-img3"><div><img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive"></div></th>
                        			</tr>
                        			<tr>
                        				<th class="t-img3"><div ><img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive"></div></th>
                        				<th class="t-img3"><div><img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive"></div></th>
                        				<th class="t-img3"><div><img src="{{ URL::asset('images/test.jpg') }}" class="img-responsive"></div></th>
                        			</tr>
	                        	</table><br>
	                        	<table style="width: 100%;">
	                        		<thead>
	                        			<tr>
	                        				<th class="t-img">
	                        					<div align="center">
	                        						<img src="{{ URL::asset('images/icon/beach-01.png') }}" class="img-responsive">
	                        						{{-- <div align="center"> --}}
						                        		{{-- @if ($value2->img_name != '' && file_exists('uploads/property/' . $value2->img_name)) --}}
						                                    {{-- <img src="{{ URL::asset('uploads/property/' . $value2->img_name) }}"  class="img-responsive"> --}}
						                                {{-- @endif --}}
						                        	{{-- </div> --}}
	                        					</div>
	                        				</th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/bedroom-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/bathroom-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/Breakfast-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/chef-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/staff-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/pool-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/Airport-01.png') }}" class="img-responsive"></div></th>
	                        				<th class="t-img"><div align="center"><img src="{{ URL::asset('images/icon/wifi-01.png') }}" class="img-responsive"></div></th>
	                        			</tr>
	                        		</thead>
	                        		<tbody>
	                        			<tr>
	                        				<td><div class="t-text" align="center">Beach Front Villa</div></td>
	                        				<td><div class="t-text" align="center">{{ $value2->bedrooms }} Bedrooms</div></td>
	                        				<td><div class="t-text" align="center">{{ $value2->bathrooms }} Bathrooms</div></td>
	                        				<td><div class="t-text" align="center">Continental Breakfast</div></td>
	                        				<td><div class="t-text" align="center">Thai Chef</div></td>
	                        				<td><div class="t-text" align="center">In-Villa Staff</div></td>
	                        				<td><div class="t-text" align="center">8 meters Pool</div></td>
	                        				<td><div class="t-text" align="center">Airport Transfert</div></td>
	                        				<td><div class="t-text" align="center">Wi-Fi in the villa</div></td>
	                        			</tr>
	                        		</tbody>
	                        	</table><br>
	                        	<div style="word-break: break-all; text-align: left;">
	                        		<?php $description = explode("<br />",$value2->description);?>
	                        		@foreach ($description as $a_description)
	                    				<p>{{ $a_description }}</p>
	                    			@endforeach
								</div>
	                        	
	                        	<table style="width: 100%">
                        			<tr>
                        				<th class="t-img2">
		                        			<div><h4><b>Building Info</b></h4></div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
		                        				@foreach($buildingState as $state)
	                                                @if($value2->building_state_id == $state->building_state_id)
	                                                    {{ $state->state_name }}
	                                                @endif
	                                            @endforeach
		                        			</div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
		                        				@foreach($buildingOwner as $owner)
	                                                @if($value2->building_owner_id == $owner->building_owner_id)
	                                                    {{ $owner->owner_name }}
	                                                @endif
	                                            @endforeach
		                        			</div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; Completed in  {{ $value2->year }}</div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; Indoor Area: {{ $value2->indoor_area }} m²</div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; {{ $value2->bedrooms }} Bedrooms</div>
		                        			@if($value2->rental_program == 1)
		                        				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; Rental program</div>
		                        			@endif
		                        			<?php $a_living = explode(",",$value2->living_id);?>
		                        			@foreach ($a_living as $a_id)
		                        				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
		                        					@foreach($living as $n_living)
		                                                @if($a_id == $n_living->living_id)
		                                                    {{ $n_living->living_name }}
		                                                @endif
		                                            @endforeach
		                        				</div>
		                        			@endforeach
                        				</th>
                        				<th class="t-img2">
		                        			<div><h4><b>Land info</b></h4></div>
		                        			<?php $a_land_detail = explode(",",$value2->land_detail_id);?>
		                        			@foreach ($a_land_detail as $a_detail)
		                        				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
		                        					@foreach($landDetail as $land_detail)
		                                                @if($a_detail == $land_detail->land_detail_id)
		                                                    {{ $land_detail->detail_name }}
		                                                @endif
		                                            @endforeach
		                        				</div>
		                        			@endforeach 
		                        			<div><h4><b>Building Storey</b></h4></div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; {{ $value2->apm_storeys}} storey</div>
		                        			<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; On the {{ $value2->apm_floor}} floor</div>
		                        			@if($value2->apm_access == 1)
		                        				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; Elevator access</div>
		                        			@endif
		                        			<div><h4><b>Estate</b></h4></div>
		                        			<?php $a_community = explode(",",$value2->community_id);?>
		                        			@foreach ($a_community as $a_com)
		                        				<div class="text-p"> <i class="fa fa-check" aria-hidden="true"></i>&nbsp; 
		                        					@foreach($community as $a_communites)
		                                                @if($a_com == $a_communites->community_id)
		                                                    {{ $a_communites->community_name }}
		                                                @endif
		                                            @endforeach
		                        				</div>
		                        			@endforeach
                        				</th>
                        			</tr>
	                        	</table><br>
	                        	<!-- ******************************* Map ***************************** -->
		                        <div class="map">
		                        	<div class="form-group">
		                            	<?php $myLatLong = $value2->latitude.', '.$value2->longitude;?>
		                                <div id="dvMap" style="width: 100%; height: 500px"></div>
		                            </div>
		                            <div class="form-group">
		                                <input type="hidden" class="form-control" name="latitude" value="{{ $value2->latitude }}" id="latitude">
		                                <input type="hidden" class="form-control" name="longitude" value="{{ $value2->longitude }}" id="longitude">
			                        </div>
		                        </div><br>
		                        <!-- ******************************* Map ***************************** -->
	                        	<div class="from-check">
	                        		<h3><b>Check availability INQUIRY</b></h3>
		                        	<form action="" method="POST" enctype="multipart/form-data" class="form-horizontal">
	                                    <div class="about-inner">
	                                        <div class="form-body">
	                                            <div class="form-group">
	                                                <input type="text" class="form-control" name="name" value="" placeholder="name" required>
	                                            </div>
	                                            <div class="form-group">
	                                                <input type="email" class="form-control" name="price" value="" placeholder="Email" required>
	                                            </div>
	                                            <div class="form-group">
	                                                <input type="text" class="form-control" name="phone" value="" placeholder="Phone number (Please use international format)" required>
	                                            </div>
	                                            <div class="form-group">
	                                                <textarea name="msg" id="editor1" class="form-control " rows="5" placeholder="Request"></textarea>
	                                            </div> 
	                                            <div class="form-actions">
	                                                <input type='hidden' name="activity_id" id="activity_id" class="form-control" value="{{ $value2->activity_id }}">
	                                                <button type="submit" class="btn" style="background: #c7040c; color: #fff;"><h3><b>Send</b></h3></button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </form>
	                        	</div>

	                        </div>

	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    @endforeach
@endsection
@section('script')
    {!! Html::script('js/sale/jquery-ui.js') !!}
    {!! Html::script('js/frontend/fwslider.js') !!}
    {!! Html::script('js/frontend/jquery.flexslider.js') !!}
    {!! Html::script('js/frontend/jquery.flexisel.js') !!}
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('API_KEY') }}"></script>
    {{ Html::script('js/backend/map.js') }}
	<script>
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ $myLatLong }});
            var myOptions = {
                zoom: 20,
                center: myLatlng,
            };
            map = new google.maps.Map(document.getElementById('dvMap'), myOptions);
            // This event listener will call addMarker() when the map is clicked.
            google.maps.event.addListener(map, 'click', function(event) {
                addMarker(event.latLng);
                saveData(map, event);
            });
            // Adds a marker at the center of the map.
            addMarker(myLatlng);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo1").flexisel({
				visibleItems: 3,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems:2
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			
		});
	</script>
    <script type='text/javascript'>//<![CDATA[ 
		$(window).load(function(){
		 	$( "#slider-range" ).slider({
				range: true,
				min: 1000000,
				max: 99000000,
				values: [ 1000000, 10000000 ],
				slide: function( event, ui ) {  $( "#amount" ).val(  ui.values[ 0 ] + " - " + ui.values[ 1 ] + " THB ");
				}
		 	});
			// $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + " - " + $( "#slider-range" ).slider( "values", 1 ) + " THB" );

		});//]]>  
	</script>
	<script type='text/javascript'>//<![CDATA[ 
		$(window).load(function(){
		 	$( "#slider-range2" ).slider2({
				range: true,
				min: 0,
				max: 11,
				values: [ 0, 1 ],
				slide: function( event, ui ) {  $( "#room" ).val(  ui.values[ 0 ] + " - " + ui.values[ 1 ] + " ROOM ");
				}
		 	});
			// $( "#room" ).val( $( "#slider-range2" ).slider2( "values", 0 ) + " - " + $( "#slider-range2" ).slider2( "values", 1 ) + " ROOM" );

		});//]]>  
	</script>
@endsection
