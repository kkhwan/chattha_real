<div class="agileits_work_grids">
					<ul id="flexiselDemo1">
						@foreach ($data as $key => $value)
							<li style="border-left: 1px solid rgba(51, 51, 51, 0.58); border-top: 1px solid rgba(51, 51, 51, 0.58)">
								<div class="w3ls_services_grid agileits_services_grid">
									<div class="agile_services_grid1_sub">
										<p>฿ {{ $value->price }}</p>
										@if ($value->img_name != '' && file_exists('uploads/property/100/' . $value->img_name))
	                                        <img src="{{ URL::asset('uploads/property/' . $value->img_name) }}" style="width: 100%">
	                                    @endif
									</div>
									<div class="agileinfo_services_grid_pos">
										<i class="fa fa-star-o fa-2x" aria-hidden="true"></i>
									</div>
								</div>
								<div class="wthree_service_text" style="background: #f5f5f5;">
									<h3>{{ substr($value->title,0,20) }}...</h3>
									<h4 class="w3_agileits_service">{{ substr($value->description,0,50) }}...</h4>
									<a data-toggle="modal" data-target="#myModal{{$key}}" class="link">Read More</a>
									<!-- <ul>
										<li><i class="fa fa-star" aria-hidden="true"></i></li>
										<li><i class="fa fa-star" aria-hidden="true"></i></li>
										<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
										<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
										<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
										<li>(543)</li>
									</ul> -->
								</div>
							</li>
						@endforeach
					</ul>
				</div>