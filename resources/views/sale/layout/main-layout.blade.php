
<html>
<head>
    <meta charset="utf-8">
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}

    <link rel="icon" href="{{ url()->asset('favicon.ico') }}" type="image/x-icon" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="lang" content="{{ $lang }}">
    <title>CHATTHA - @yield('title')</title>
    <!-- Responsive Window -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Custom fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') !!}
    {{ Html::style('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}
    {{ Html::style('assets/global/plugins/uniform/css/uniform.default.min.css') }}

    <!-- CSS Files -->
    <!-- {!! Html::style('assets/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!} -->
    {!! Html::style('css/frontend/main.css') !!}
    {!! Html::style('css/sale/benner.css') !!}
    {!! Html::style('css/sale/jquery-ui1.css') !!}
    {!! Html::style('css/sale/from-skill.css') !!}
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {!! Html::style('css/frontend/contact-box.css') !!}
    @yield('css')
</head>

<body>
    @include('frontend.include.header')

    <div id="ui-main" class="wrapper">
        <!-- banner -->
        <div id="home" class="w3ls-banner">
            <!-- banner-text -->
            <div class="slider">
                <div class="callbacks_container">
                    <ul class="rslides callbacks callbacks1" id="slider4">
                        <li>
                            <div class="w3layouts-banner-top">
                                <div class="agileits-banner-info">
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="w3layouts-banner-top w3layouts-banner-top1">
                                <div class="agileits-banner-info">
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="w3layouts-banner-top w3layouts-banner-top2">
                                <div class="agileits-banner-info">
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="w3layouts-banner-top w3layouts-banner-top3">
                                <div class="agileits-banner-info">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>

                <!--banner Slider starts Here-->
            </div>
        </div>
        <!-- //banner -->

        @yield('content')

        @yield('from-content')
        
        <a href="#" class="contact-box islive">
            <i class="fa fa-envelope-o fa-1x" aria-hidden="true"></i>
        </a>
        <div class="contactformwrapper" style="display:none;">
            <div>
                <h4 align="center" style="color: #C41F3E;"><b>Contact Us</b></h4>
                <p align="center" style="color: #868686;">Use the form below to contact us!</p>
                <form action="{{ URL::to('/sale/contact_us')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="form-group">
                            <div style="margin: 0px 10px 0px 10px;">
                                <input type="text" class="form-control" name="name" value="" placeholder="Your Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="margin: 0px 10px 0px 10px;">
                                <input type="text" class="form-control" name="email" value="" placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="margin: 0px 10px 0px 10px;">
                                <input type="text" class="form-control" name="mobile" value="" placeholder="Your Phone" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="margin: 0px 10px 0px 10px;">
                                <textarea type="text" class="form-control" name="msg" value="" placeholder="Type your Message" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="margin: 0px 10px 0px 10px; ">
                                <input type="submit" class="form-control btn" style="background-color: #C41F3E; color: #fff;" value="SEND">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('frontend.include.footer')

    {{ Html::script('assets/global/plugins/jquery.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-migrate.min.js') }}
    <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    {{ Html::script('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
    {{ Html::script('assets/global/plugins/jquery.blockui.min.js') }}
    {{ Html::script('assets/global/plugins/jquery.cokie.min.js') }}
    {{ Html::script('assets/global/plugins/uniform/jquery.uniform.min.js') }}
    @yield('page-plugin')
    {{ Html::script('assets/global/plugins/bootbox/bootbox.min.js') }}
    <!-- END PAGE LEVEL PLUGINS -->

    {{ Html::script('assets/global/scripts/metronic.js') }}
    
    <script>
      // Get the modal
      var modal = document.getElementById('id01');

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
    </script>

    {!! Html::script('assets/global/scripts/jquery.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    
    @yield('page-plugin')
    {!! Html::script('js/sale/responsiveslides.min.js') !!}
    
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>

    @yield('script')
    <script>
        $(document).ready(function(){
            $(".islive").click(function(){
                $(".contactformwrapper").toggle();
            });
        });
    </script>

    <!-- {!! Html::script('js/frontend/main.js') !!} -->
    {!! Html::script('js/frontend/jquery.mobilemenu.js') !!}
    
</body>
</html>
